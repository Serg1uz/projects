Description: >
  This is an example of a long running ECS service that serves a JSON API of products.

Parameters:
  VPC:
    Description: The VPC that the ECS cluster is deployed to
    Type: AWS::EC2::VPC::Id

  ECSCluster:
    Description: Please provide the ECS Cluster ID that this service should run on
    Type: String

  DesiredCount:
    Description: How many instances of this task should we run across our cluster?
    Type: Number
    Default: 2

  Listener:
    Description: The Application Load Balancer listener to register with
    Type: String

  DBEndpoint:
    Description: The DB Endpoint
    Type: String

  RedisEndpoint:
    Description: The Redis Endpoint
    Type: String

  ServiceRole:
    Description: An IAM Role that grants the service access to register/unregister with the Application Load Balancer (ALB).
    Type: String

  AppUrl:
    Description: AppUrl
    Type: String

  ECRBackendRepositoryURL:
    Description: The ECR repository for the backend container
    Type: String

  FlowerUsername:
    Description: Username for flower monitoring service authentication
    Type: String

  FlowerPassword:
    Description: Password for flower monitoring service authentication
    Type: String

Resources:

  FlowerService:
    Type: AWS::ECS::Service
    DependsOn: FlowerListenerRule
    Properties:
      Cluster: !Ref ECSCluster
      Role: !Ref ServiceRole
      DesiredCount: !Ref DesiredCount
      TaskDefinition: !Ref FlowerTaskDefinition
      LoadBalancers:
        - ContainerName: "flower"
          ContainerPort: 5555
          TargetGroupArn: !Ref FlowerTargetGroup

  FlowerTaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: flower
      ContainerDefinitions:
        - Name: flower
          Essential: true
          Image: mher/flower
          Memory: 128
          Command:
            - 'flower'
            - !Sub "--broker=${RedisEndpoint}"
            - !Sub "--basic_auth=${FlowerUsername}:${FlowerPassword}"
          PortMappings:
            - ContainerPort: 5555
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref FlowerCloudWatchLogsGroup
              awslogs-region: !Ref AWS::Region

  FlowerCloudWatchLogsGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Ref AWS::StackName
      RetentionInDays: 365

  FlowerTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      VpcId: !Ref VPC
      Port: 80
      Protocol: HTTP
      Matcher:
        HttpCode: 200-401

  FlowerListenerRule:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      ListenerArn: !Ref Listener
      Priority: 1
      Conditions:
        - Field: host-header
          Values:
            - !Sub "flower.${AppUrl}"
      Actions:
        - TargetGroupArn: !Ref FlowerTargetGroup
          Type: forward
