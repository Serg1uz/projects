stages:
  - test
  - build
  - release
  - deploy
  - cloudformation

variables:
  REPOSITORY_URL: ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${AppUrl}/backend
  ECRBackendRepositoryName: ${AppUrl}/backend
  GitSHA: $CI_COMMIT_SHORT_SHA
  AWSAccessKeyId: $AWS_ACCESS_KEY_ID
  AWSSecretAccessKey: $AWS_SECRET_ACCESS_KEY

Pytest:
  image: python:3.7
  stage: test
  services:
    - postgres:latest
    - redis:latest
  variables:
    DATABASE_URL: "postgresql://postgres:postgres@postgres:5432/postgres"
    DJANGO_SETTINGS_MODULE: "backend.settings.gitlab-ci"
    SECRET_KEY: "secret"
    STACK_NAME: 'my-stack'
    DEBUG: ""
  before_script:
    - cd backend
    - pip install -r requirements/test.txt
    - pip install -r requirements/base.txt
  script:
    - flake8
    - pytest --cov
  after_script:
    - echo "Pytest tests complete"
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'

Jest:
  image: node:14
  stage: test
  before_script:
    - cd frontend
    - npm install --progress=false
  script:
    - npm run lint
    - npm run test:unit
  after_script:
    - echo "Jest tests complete"
  coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'

.Cypress:
  image: cypress/base:10
  stage: test
  variables:
    VUE_APP_BASE_URL: http://localhost:8080
  before_script:
    - cd frontend
    - npm install --progress=false
  script:
    - apt install httping
    - npm run serve &
    - while ! httping -qc1 http://localhost:8080/login ; do sleep 1 ; done
    - $(npm bin)/cypress run
  after_script:
    - echo "Cypress tests complete"
  artifacts:
    paths:
      - tests/e2e/videos/
      - tests/e2e/screenshots/
    expire_in: 7 days

Build frontend:
  image: node:14
  stage: build
  only:
    - master
  variables:
    VUE_APP_CI_COMMIT_SHORT_SHA: "$CI_COMMIT_SHORT_SHA"
    VUE_APP_CI_JOB_URL: "$CI_JOB_URL"
    VUE_APP_API_BASE_URL: "https://api.${AppUrl}"
    VUE_APP_BASE_URL: "https://${AppUrl}"
    VUE_APP_WS_PROTOCOL: wss://
    VUE_APP_BASE_HOST: api.${AppUrl}
    VUE_APP_SITE_DOMAIN: ${AppUrl}
  artifacts:
    paths:
      - quasar/dist/pwa
  before_script:
    - npm install -g @quasar/cli
    - cd quasar
    - npm install --progress=false
  script:
    - quasar build -m pwa
  after_script:
    - echo "Build Complete"

Build backend:
  stage: build
  image: docker:stable
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  only:
    - master
  before_script:
    - cd backend
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $CI_REGISTRY_IMAGE:latest -f scripts/prod/Dockerfile .
    - docker push $CI_REGISTRY_IMAGE:latest
  after_script:
    - echo "Build backend complete"

Release backend:
  stage: release
  image: docker:stable
  only:
    - master
  services:
    - docker:dind
  before_script:
    - cd backend
    - apk add --no-cache curl jq python py-pip
    - pip install awscli
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest -t $REPOSITORY_URL:${GitSHA} -f scripts/prod/Dockerfile .
    - $(aws ecr get-login --no-include-email --region us-east-1)
    - docker push $REPOSITORY_URL:${GitSHA}
  after_script:
    - echo "Release backend complete"

Deploy frontend:
  image: python:3.7
  stage: deploy
  only:
    - master
  dependencies:
    - "Build frontend"
  before_script:
    - cd quasar
    - pip install awscli
    - aws iam get-user
  script:
    - aws s3 sync --delete --exclude index.html --exclude service-worker.js ./dist/pwa/ s3://${AppUrl}/
    - aws s3 cp --cache-control 'max-age=0' ./dist/pwa/index.html s3://${AppUrl}/
    - aws s3 cp --cache-control 'max-age=0' ./dist/pwa/service-worker.js s3://${AppUrl}/
  after_script:
    - echo "Deploy frontend complete"

.Create stack:
  image: python:3.7
  stage: cloudformation
  only:
    - master
  variables:
    EnvironmentName: staging
  before_script:
    - pip install awscli
    - ./cloudformation/tests/validate-templates.sh
    - ./cloudformation/scripts/generate_secrets.py
    - aws s3 sync cloudformation/ s3://${AppUrl}-cloudformation/ --delete
  script:
    - |
      aws cloudformation create-stack \
        --stack-name ${StackName} \
        --template-url https://s3.amazonaws.com/${AppUrl}-cloudformation/master.yaml \
        --capabilities=CAPABILITY_NAMED_IAM \
        --parameters file://./parameters.json
  after_script:
    - echo "Create stack complete"

Update stack:
  image: python:3.7
  stage: cloudformation
  only:
    - master
  variables:
    EnvironmentName: staging
  before_script:
    - pip install awscli
    - ./cloudformation/tests/validate-templates.sh
    - ./cloudformation/scripts/generate_secrets.py
    - aws s3 sync cloudformation/ s3://${AppUrl}-cloudformation/
  script:
    - |
      aws cloudformation update-stack \
        --stack-name ${StackName} \
        --template-url https://s3.amazonaws.com/${AppUrl}-cloudformation/master.yaml \
        --capabilities=CAPABILITY_NAMED_IAM \
        --parameters file://./parameters.json
  after_script:
    - echo "Update stack complete"
