<?php

require_once __DIR__ . '/bootstrap.php';

$mpdf = new \Mpdf\Mpdf([
	'mode' => 'c',
	'margin_left' => 32,
	'margin_right' => 25,
	'margin_top' => 47,
	'margin_bottom' => 47,
	'margin_header' => 10,
	'margin_footer' => 10
]);

$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins

$header = '
<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>
<td width="33%">Left header p <span style="font-size:14pt;">{PAGENO}</span></td>
<td width="33%" align="center"><img src="assets/sunset.jpg" width="126px" /></td>
<td width="33%" style="text-align: right;"><span style="font-weight: bold;">Right header</span></td>
</tr></table>';

$headerEven = '
<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>
<td width="33%"><span style="font-weight: bold;">Outer header</span></td>
<td width="33%" align="center"><img src="assets/sunset.jpg" width="126px" /></td>
<td width="33%" style="text-align: right;">Inner header p <span style="font-size:14pt;">{PAGENO}</span></td>
</tr></table>';

$footer = '<div align="center">See <a href="http://mpdf.github.io">documentation manual</a></div>  {PAGENO}/ {nb}';

$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLHeader($headerEven, 'E');

$mpdf->SetHTMLFooter($footer);
$mpdf->SetHTMLFooter($footer, 'E');


$html = '
<h1>mPDF</h1>

<h2>Headers & Footers Method 2</h2>

<h3>Odd / Right page</h3>

<p>Nulla felis erat, imperdiet eu, ullamcorper non, nonummy quis, elit. Suspendisse potenti. Ut a eros at ligula
vehicula pretium. Maecenas feugiat pede vel risus. Nulla et lectus. Fusce eleifend neque sit amet erat. Integer
consectetuer nulla non orci. Morbi feugiat pulvinar dolor. Cras odio. Donec mattis, nisi id euismod auctor,
neque metus pellentesque risus, at eleifend lacus sapien et risus. Phasellus metus. Phasellus feugiat, lectus ac
aliquam molestie, leo lacus tincidunt turpis, vel aliquam quam odio et sapien. Mauris ante pede, auctor ac, suscipit
quis, malesuada sed, nulla. Integer sit amet odio sit amet lectus luctus euismod. Donec et nulla. Sed quis orci.</p>
<pagebreak />

<h2>Tables</h2>
<h3>CSS Styles</h3>
<p>The CSS properties for tables and cells is increased over that in html2fpdf. It includes recognition of THEAD, TFOOT and TH.<br />See below for other facilities such as autosizing, and rotation.</p>
<table border="1">
<tbody><tr><td>Row 1</td><td>This is data</td><td>This is data</td></tr>

<tr><td>Row 2</td>

<td style="background-gradient: linear #c7cdde #f0f2ff 0 1 0 0.5;">
<p>This is data p</p>
This is data out of p
<p style="font-weight:bold; font-size:20pt; background-color:#FFBBFF;">This is bold data p</p>
<b>This is bold data out of p</b><br />
This is normal data after br
<h3>H3 in a table</h3>
<div>This is data div</div>
This is data out of div
<div style="font-weight:bold;">This is data div (bold)</div>
This is data out of div
</td>


<td><p>More data</p><p style="font-size:12pt;">This is large text</p></td></tr>
<tr><td><p>Row 3</p></td><td><p>This is long data</p></td><td>This is data</td></tr>
<tr><td><p>Row 4 &lt;td&gt; cell</p></td><td>This is data</td><td><p>This is data</p></td></tr>
<tr><td>Row 5</td><td>Also data</td><td>Also data</td></tr>
<tr><td>Row 6</td><td>Also data</td><td>Also data</td></tr>
<tr><td>Row 7</td><td>Also data</td><td>Also data</td></tr>
<tr><td>Row 8</td><td>Also data</td><td>Also data</td></tr>
</tbody></table>

<p>This table has padding-left and -right set to 5mm i.e. padding within the cells. Also border colour and style, font family and size are set by <acronym>CSS</acronym>.</p>
<table align="right" style="border: 1px solid #880000; font-family: Mono; font-size: 7pt; " class="widecells">
<tbody><tr><td>Row 1</td><td>This is data</td><td>This is data</td></tr>
<tr><td>Row 2</td><td><p>This is data p</p></td><td><p>More data</p></td></tr>
<tr><td><p>Row 3</p></td><td><p>This is long data</p></td><td>This is data</td></tr>
<tr><td><p>Row 4 &lt;td&gt; cell</p></td><td>This is data</td><td><p>This is data</p></td></tr>
<tr><td>Row 5</td><td>Also data</td><td>Also data</td></tr>
<tr><td>Row 6</td><td>Also data</td><td>Also data</td></tr>
<tr><td>Row 7</td><td>Also data</td><td>Also data</td></tr>
<tr><td>Row 8</td><td>Also data</td><td>Also data</td></tr>
</tbody></table>

<p>This table has padding-top and -bottom set to 3mm i.e. padding within the cells. Also background-, border colour and style, font family and size are set by in-line <acronym>CSS</acronym>.</p>
<table style="border: 1px solid #880000; background-color: #BBCCDD; font-family: Mono; font-size: 7pt; " class="tallcells">
<tbody><tr><td>Row 1</td><td>This is data</td><td>This is data</td></tr>
<tr><td>Row 2</td><td><p>This is data p</p></td><td><p>More data</p></td></tr>
<tr><td><p>Row 3</p></td><td><p>This is long data</p></td><td>This is data</td></tr>
</tbody></table>


<h3 style="margin-top: 20pt; margin-collapse:collapse;">Table Styles</h3>
<p>The style sheet used for these examples shows some of the table styles I use on my website. The property \'topntail\' defined by a border-type definition e.g. "1px solid #880000" puts a border at the top and bottom of the table, and also below a header row (thead) if defined. Note also that &lt;thead&gt; will automatically turn on the header-repeat i.e. reproduce the header row at the top of each page.</p>
<p>bpmTopic Class</p>
<table class="bpmTopic"><thead></thead><tbody>
<tr>
<td>Row 1</td>
<td>This is data</td>
<td>This is data</td>
</tr>
<tr>
<td>Row 2</td>
<td>
<p>This is data p</p>
</td>
<td>
<p>More data</p>
</td>
</tr>
<tr>
<td>
<p>Row 3</p>
</td>
<td>
<p>This is long data</p>
</td>
<td>This is data</td>
</tr>
<tr>
<td>
<p>Row 4 &lt;td&gt; cell</p>
</td>
<td>This is data</td>
<td>
<p>This is data</p>
</td>
</tr>
<tr>
<td>Row 5</td>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr>
<td>Row 6</td>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr>
<td>Row 7</td>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr>
<td>Row 8</td>
<td>Also data</td>
<td>Also data</td>
</tr>
</tbody></table>

<p>&nbsp;</p>

<p>bpmTopic<b>C</b> Class (centered) Odd and Even rows</p>
<table class="bpmTopicC"><thead>
<tr class="headerrow"><th>Col/Row Header</th>
<td>
<p>Second column header p</p>
</td>
<td>Third column header</td>
</tr>
</thead><tbody>
<tr class="oddrow"><th>Row header 1</th>
<td>This is data</td>
<td>This is data</td>
</tr>
<tr class="evenrow"><th>Row header 2</th>
<td>
<p>This is data p</p>
</td>
<td>
<p>This is data</p>
</td>
</tr>
<tr class="oddrow"><th>
<p>Row header 3</p>
</th>
<td>
<p>This is long data</p>
</td>
<td>This is data</td>
</tr>
<tr class="evenrow"><th>
<p>Row header 4</p>
<p>&lt;th&gt; cell acting as header</p>
</th>
<td>This is data</td>
<td>
<p>This is data</p>
</td>
</tr>
<tr class="oddrow"><th>Row header 5</th>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr class="evenrow"><th>Row header 6</th>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr class="oddrow"><th>Row header 7</th>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr class="evenrow"><th>Row header 8</th>
<td>Also data</td>
<td>Also data</td>
</tr>
</tbody></table>

<p>&nbsp;</p>

<p>bpmTopnTail Class </p>
<table class="bpmTopnTail"><thead></thead><tbody>
<tr>
<td>Row 1</td>
<td>This is data</td>
<td>This is data</td>
</tr>
<tr>
<td>Row 2</td>
<td>
<p>This is data p</p>
</td>
<td>
<p>This is data</p>
</td>
</tr>
<tr>
<td>
<p>Row 3</p>
</td>
<td>
<p>This is long data</p>
</td>
<td>This is data</td>
</tr>
<tr>
<td>
<p>Row 4 &lt;td&gt; cell</p>
</td>
<td>This is data</td>
<td>
<p>This is data</p>
</td>
</tr>
<tr>
<td>Row 5</td>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr>
<td>Row 6</td>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr>
<td>Row 7</td>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr>
<td>Row 8</td>
<td>Also data</td>
<td>Also data</td>
</tr>
</tbody></table>
<p>&nbsp;</p>
<p>bpmTopnTail<b>C</b> Class (centered) Odd and Even rows</p>
<table class="bpmTopnTailC"><thead>
<tr class="headerrow"><th>Col/Row Header</th>
<td>
<p>Second column header p</p>
</td>
<td>Third column header</td>
</tr>
</thead><tbody>
<tr class="oddrow"><th>Row header 1</th>
<td>This is data</td>
<td>This is data</td>
</tr>
<tr class="evenrow"><th>Row header 2</th>
<td>
<p>This is data p</p>
</td>
<td>
<p>This is data</p>
</td>
</tr>
<tr class="oddrow"><th>
<p>Row header 3</p>
</th>
<td>
<p>This is long data</p>
</td>
<td>This is data</td>
</tr>
<tr class="evenrow"><th>
<p>Row header 4</p>
<p>&lt;th&gt; cell acting as header</p>
</th>
<td>This is data</td>
<td>
<p>This is data</p>
</td>
</tr>
<tr class="oddrow"><th>Row header 5</th>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr class="evenrow"><th>Row header 6</th>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr class="oddrow"><th>Row header 7</th>
<td>Also data</td>
<td>Also data</td>
</tr>
<tr class="evenrow"><th>Row header 8</th>
<td>Also data</td>
<td>Also data</td>
</tr>
</tbody></table>

<p>&nbsp;</p>

<p>TopnTail Class</p>
<table class="bpmTopnTail"><thead>
<tr class="headerrow"><th>Col and Row Header</th>
<td>
<p>Second</p>
<p>column</p>
</td>
<td class="pmhTopRight">Top right align</td>
</tr>
</thead><tbody>
<tr class="oddrow"><th>
<p>Row header 1 p</p>
</th>
<td>This is data</td>
<td>This is data</td>
</tr>
<tr class="evenrow"><th>Row header 2</th>
<td class="pmhBottomRight"><b><i>Bottom right align</i></b></td>
<td>
<p>This is data. Can use</p>
<p><b>bold</b> <i>italic </i><sub>sub</sub> or <sup>sup</sup> text</p>
</td>
</tr>
<tr class="oddrow"><th class="pmhBottomRight">
<p>Bottom right align</p>
</th>
<td class="pmhMiddleCenter" style="border: #000000 1px solid">
<p>This is data. This cell</p>
<p>uses Cell Styles to set</p>
<p>the borders.</p>
<p>All borders are collapsible</p>
<p>in mPDF.</p>
</td>
<td>This is data</td>
</tr>
<tr class="evenrow"><th>Row header 4</th>
<td>
<p>This is data p</p>
</td>
<td>More data</td>
</tr>
<tr class="oddrow"><th>Row header 5</th>
<td colspan="2" class="pmhTopCenter">Also data merged and centered</td>
</tr>
</tbody></table>

<p>&nbsp;</p>

<h4>Lists in a Table</h4>
<table class="bpmTopnTail"><thead>
<tr class="headerrow"><th>Col and Row Header</th>
<td>
<p>Second</p>
<p>column</p>
</td>
<td class="pmhTopRight">Top right align</td>
</tr>
</thead><tbody>
<tr class="oddrow"><th>
<p>Row header 1 p</p>
</th>
<td>This is data</td>
<td>This is data</td>
</tr>
<tr class="evenrow"><th>Row header 2</th>
<td>
<ol>
<li>Item 1</li>
<li>Item 2
<ol type="a">
<li>Subitem of ordered list</li>
<li>Subitem 2
<ol type="i">
<li>Level 3 subitem</li>
<li>Level 3 subitem</li>
</ol>
</li>
</ol>
</li>
<li>Item 3</li>
<li>Another Item</li>
<li>Subitem
<ol>
<li>Level 3 subitem</li>
</ol>
</li>
<li>Another Item</li>
</ol>
</td>
<td>
Unordered list:
<ul>
<li>Item 1</li>
<li>Item 2
<ul>
<li>Subitem of unordered list</li>
<li>Subitem 2
<ul>
<li>Level 3 subitem</li>
<li>Level 3 subitem</li>
<li>Level 3 subitem</li>
</ul>
</li>
</ul>
</li>
<li>Item 3</li>
</ul>
</td>
</tr>
</tbody></table>
<p>&nbsp;</p>


<h4>Automatic Column Width</h4>
<table class="bpmTopnTail"><tbody>
<tr>
<td>Causes</td>
<td>Nulla felis erat, imperdiet eu, ullamcorper non, nonummy quis, elit. <br />
Ut a eros at ligula vehicula pretium; maecenas feugiat pede vel risus.<br />
Suspendisse potenti</td>
</tr>
<tr>
<td>Mechanisms</td>
<td>Ut magna ipsum, tempus in, condimentum at, rutrum et, nisl. Vestibulum interdum luctus sapien. Quisque viverra. Etiam id libero at magna pellentesque aliquet. Nulla sit amet ipsum id enim tempus dictum. Maecenas consectetuer eros quis massa. Mauris semper velit vehicula purus. Duis lacus. Aenean pretium consectetuer mauris. Ut purus sem, consequat ut, fermentum sit amet, ornare sit amet, ipsum. Donec non nunc. Maecenas fringilla. Curabitur libero. In dui massa, malesuada sit amet, hendrerit vitae, viverra nec, tortor. Donec varius. Ut ut dolor et tellus adipiscing adipiscing.</td>
</tr>
</tbody></table>


<h4>ColSpan & Rowspan</h4>
<table class="bpmTopnTail"><tbody>
<tr>
<td rowspan="2">Causes</td>
<td colspan="2">Nulla felis erat, imperdiet eu, ullamcorper non, nonummy quis, elit. <br />
Ut a eros at ligula vehicula pretium; maecenas feugiat pede vel risus.<br />
Suspendisse potenti</td>
</tr>
<tr>
<td>Fusce eleifend neque sit amet erat.<br />
Donec mattis, nisi id euismod auctor, neque metus pellentesque risus, at eleifend lacus sapien et risus.</td>
<td>Mauris ante pede, auctor ac, suscipit quis, malesuada sed, nulla.<br />
Phasellus feugiat, lectus ac aliquam molestie, leo lacus tincidunt turpis, vel aliquam quam odio et sapien.</td>
</tr>
</tbody></table>

<h3>Even / Left page</h3>

<p>Nulla felis erat, imperdiet eu, ullamcorper non, nonummy quis, elit. Suspendisse potenti. Ut a eros at ligula
vehicula pretium. Maecenas feugiat pede vel risus. Nulla et lectus. Fusce eleifend neque sit amet erat. Integer
consectetuer nulla non orci. Morbi feugiat pulvinar dolor. Cras odio. Donec mattis, nisi id euismod auctor, neque
metus pellentesque risus, at eleifend lacus sapien et risus. Phasellus metus. Phasellus feugiat, lectus ac aliquam
molestie, leo lacus tincidunt turpis, vel aliquam quam odio et sapien. Mauris ante pede, auctor ac, suscipit quis,
malesuada sed, nulla. Integer sit amet odio sit amet lectus luctus euismod. Donec et nulla. Sed quis orci.</p>';


$mpdf->WriteHTML($html);

$footer2 = '<div align="center">TEST</div> {PAGENO}/ {nb}';
$mpdf->SetHTMLFooter($footer2);
$mpdf->SetHTMLFooter($footer2, 'E');


$mpdf->Output('example16.pdf', 'F');
