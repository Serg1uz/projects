var pay_types = ['CC', 'ACH', 'PAYPAL', 'EBAY', 'CHECK'];
var over5000 = 0;
var under1000 = 0;
var seniors = ['aleksmas@gmail.com', 'kaliberov@gmail.com', 'kostiantynkovlev@gmail.com', 'den.pyrlia@gmail.com', 'sergeimakarenko2010@gmail.com', 'olga1kushnikova@gmail.com', 'maryna.shk@gmail.com', 'Nagaev.Dima.UTTC@gmail.com', 'andrey.vovchenko.uttc@gmail.com', 'olegmaslyanyi@gmail.com', 'amalyuk.uttc@gmail.com', 'ttitarenko.uttc@gmail.com'];
var CEO = ['aleksmas@gmail.com', 'kaliberov@gmail.com'];
var $id;
var $pay_type;
var flag_CC, flag_ACH, flag_PAYPAL, flag_EBAY, flag_CHECK;
var min_profit_count = 0;
var handling = 0;
var minOrderFee = 0;
var handlingTariff = 0;
var dropShipmentTariff = 0;

function deformat(value, type) {

    if (value != null) {
        if (typeof value !== 'string') {
            value = value.toString();
        }
        var res = value.replace(/[$,]/gi, '');

        switch (type) {
            case 'int':
                return parseInt(res);
                break;
            default:
                return parseFloat(res);
                break;
        }
    }
}

function priceFormat(_number) {
    //////console.log('Number: ' + _number);
    var decimal = 2;
    var separator = ',';
    var decpoint = '.';
    var format_string = '$#';

    var r = parseFloat(_number)

    var exp10 = Math.pow(10, decimal);// приводим к правильному множителю
    r = Math.round(r * exp10) / exp10;// округляем до необходимого числа знаков после запятой

    rr = Number(r).toFixed(2).toString().split('.');

    b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1" + separator);

    r = (rr[1] ? b + decpoint + rr[1] : b);
    return format_string.replace('#', r);
}

$( document ).ajaxStart(function() {
    $("#wait").show();
}).ajaxStop(function() {
    $("#wait").hide();
});


//#######################################
$(document).ready(function () {
    var brands = [];
    var partnum = [];
    var this_brand;

    var e_email = $("input[name='data[e_email]']").val();
    /**
     * Если есть формула, то вывести окно
     */
    $('input[name*="_uttc_net_price"]').hover(function () {
            var hover = $(this).attr('hover');

            if (hover !== null && hover != '') {

                if (hover.length > 1) {
                    var parent = $(this).parent();
                    parent.append("<div id='base_hover'>"+hover+"</div>");

                    var base_hover = $('#base_hover');
                    base_hover.addClass('base_hover');
                    //////console.log('atata');
                }
            }

        },
        function () {
            setTimeout(function () {
                $('#base_hover').remove();
            }, 0);
        }
    );

    window.uttc_net_price_by_brand = {};
    window.suggested_price_by_brand = {};
    $("[data-brandname]").each(function(){
        brandNameRow = $(this).attr('data-brandname');
        brandNameRow = brandNameRow.replace(/\s|\.|\,|\(|\)|&|\"|\'|\`/g,'-');
        input = $(this).find("td.payment_suggestedPrice input.sug-price");
        payType = $(input).attr('pay-type');
        row_uttc_net_price = $(this).find("td.payment_uttcNetPrice input").val();
        row_suggested_net_price = $(this).find("td.payment_suggestedPrice input.sug-price").val();
        row_qty = $(this).find("td.payment_qty input").val();

        if (!uttc_net_price_by_brand.hasOwnProperty(payType))
            uttc_net_price_by_brand[payType] = {};
        if (!uttc_net_price_by_brand[payType].hasOwnProperty(brandNameRow))
            uttc_net_price_by_brand[payType][brandNameRow] = 0;

        if (!suggested_price_by_brand.hasOwnProperty(payType))
            suggested_price_by_brand[payType] = {};
        if (!suggested_price_by_brand[payType].hasOwnProperty(brandNameRow))
            suggested_price_by_brand[payType][brandNameRow] = 0;
        console.log(row_suggested_net_price);
        console.log(' ^^^^^^');
        uttc_net_price_by_brand[payType][brandNameRow] += row_uttc_net_price * row_qty;
        suggested_price_by_brand[payType][brandNameRow] += row_suggested_net_price * row_qty;
        console.log('pay-type ' + payType);
        console.log('row_suggested_net_price ' + row_suggested_net_price);
        console.log('row_qty ' + row_qty);
        console.log(suggested_price_by_brand);
        console.log('suggested_price_by_brand');
    });
    console.log(suggested_price_by_brand);
    console.log(' testfffffffff');

    /**
     * Если есть кратность у товара, то вывести окно
     */
    $('input[name*="_qty"]').hover(function(){
            var manuf_qty = $(this).attr('manuf-box-qty');
            var multiple = $(this).val();
            if(multiple % manuf_qty)
            {

                var parent = $(this).parent();
                parent.append("<div id='base_hover'>Кратность упаковки<br/> <strong>" + manuf_qty + "</strong></div>");

                var base_hover = $('#base_hover');
                base_hover.addClass('base_hover');
                //////console.log('atata');
            }
        },
        function(){
            setTimeout(function(){$('#base_hover').remove();}, 0);
        }
    );

    var differences = function(diff, pType)
    {
        var suggestedPriceColumn = $('input[name*="_suggested_price"][pay-type="' + pType + '"]');
        $.each(suggestedPriceColumn, function(){
            var fieldId = $(this).parent().parent().attr('id');
            var suggestedValue = parseFloat(deformat($(this).val()));
            var finalSaleValue = parseFloat(deformat($('input[name="' + fieldId + '_final_sale_price"][pay-type="' + pType + '"]').val()));
            if(finalSaleValue !== suggestedValue)
            {
                //console.log(pType + ': suggested: ' + suggestedValue + ' final: ' + finalSaleValue);
                diff++;
            }
        });

        return diff;
    }


    /**
     * Если есть Base товар, то вывести окно
     */
    $('input[name*="_uttc_sell_price"]').hover(function () {
            var base = $(this).attr('data-base');
            if (base !== null && base != '') {

                var parent = $(this).parent();
                parent.append("<div id='base_hover'>Базовый товар<br/>PN# <strong>" + base + "</strong></div>");
                var base_hover = $('#base_hover');
                base_hover.addClass('base_hover');
                //  ////console.log('atata');
            }
        },
        function () {
            setTimeout(function () {
                $('#base_hover').remove();
            }, 0);
        }
    );

    $.handling_fee_recalc = function()
    {
        handling_fee_select = $('select[id*="_handling_fee"]');
        $.each(handling_fee_select, function(){
            obj = $(this);
            data_id = obj.attr('data-id');

            if($('input[name="'+data_id+'__checkbox"]').is(':checked')){
                handling += parseFloat(obj.val());
               // ////console.log('Handling Fee: ' + handling);
            }
        });

        if(($('input[name*="__checkbox"]').length > 0)) {
            //alert("Sorry it is test 1 "+handling);
            $.ajax({
                type: 'POST',
                url: 'index.php?page=domestic&action=update_service',
                data: {
                    'service-action': 'handling_fee',
                    'token': $("input[name='data[token]']").val(),
                    'handling_fee': handling
                }
            });
        }
    }

    if(($('select[id*="_handling_fee"]').length > 0))
    {
        //alert("Sorry it is test 2 "+handling);
        $('input[type="checkbox"][name*="__checkbox"]').change(function(){
            handling = 0;
            $.handling_fee_recalc();
        });
    }else{
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'service-action': 'handling_fee',
                'token': $("input[name='data[token]']").val(),
                'handling_fee': 0
            }
        });
    }

    /**
     * Функция подсчета налоговой ставки
     */
    $.tax = function () {

        var tax = $('#1tax_sum1');
        var tax_hidden = $('#tax_sum');
        var tax_sum = $('input[name*="_tax_sum"]');
        var data_id = tax_sum.attr('data-id');
        var tax_percent_info = $('#tax_percent_info');
        var tax_percent = $('#tax_percent');
        var sum_net = 0;
        var sum_sugg = 0;
        var sum_info = 0;
        var sum_hidden = 0;

        var $shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
        var $count = 0;
        // console.log(tax_sum);
        // alert('dsf');
        $.each(tax_sum, function () {
            console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')

            $item = $(this);
            $data_id = $item.data('id');
            $data_clientTax = $item.data('clienttax');
            $data_usTax = $item.data('ustax');
            $attr = $('[name="' + $data_id + '_selected_brand_for_tax"]').attr('data-flag');

            // налог на нас
            sum_info = parseFloat($item.val());
            sum_hidden = parseFloat($data_usTax) * tax_sum_js;

        });

        // set tax
        tax_percent_info.val(sum_info == 0 ? '' : tax_rounding(taxJarPercent * 100));
        tax.val(sum_info == 0 ? '' : rounding(taxJarValue));
        tax_percent.val(sum_hidden == 0 ? '' : tax_rounding(tax_percent_js * 100));
        tax_hidden.val(rounding(tax_sum_js));

    }

    $.tax();

    $('button.table-by-brand').click(function(){
        var brand = $(this).attr('data-brand');
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=getTableByBrand',
            data: {
                'brand' : brand
            },
            success: function(response)
            {
                var json = eval('(' + response + ')');
                console.log(json);
                $('.show-table-by-brand').html(json.response);
            }


        })
    });

    $('.table-by-customer-email').click(function(){
        var customerEmail = $(this).attr('data-email');
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=get_domestic_data_for_table',
            data: {
                'email' : customerEmail
            },
            success: function(response)
            {
                var json = eval('(' + response + ')');
                console.log(json);
                $('.show-table-by-customer-email').html(json.response);
            }


        })
    });

    /**
     * Установка снятие фришиппинга у конкурента
     */
    $('button[id*="_freesh_checkbox"]').click(function(){
        var itm = $(this);
        var itm_id = itm.attr('db-row-id');

        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=setFreeShipping',
            data: {
                'itm_id' : itm_id
            },
            success: function(response)
            {
                var json = eval('(' + response + ')');
                ////console.log(json);

                if(json.response == 'success')
                {
                    var min_sp_inp = $('input[name="'+itm_id+'_min_sell_price"]');
                    var min_sp_inp_val = min_sp_inp.val();
                    var free_hiden_inp = $('input[name="'+itm_id+'_sell_price_freeshipping"]');

                    if(free_hiden_inp.val() == 0 || free_hiden_inp.length == 0)
                    {
                        free_hiden_inp.val(1);
                        min_sp_inp.css('background-color', 'orange');
                    }
                    else
                    {
                        free_hiden_inp.val(0);
                        min_sp_inp.css('background-color', 'white');
                    }

                }

            }

        })
    });

    /**
     * Сохрание профитов
     * TODO:
     */

    $.save_profits = function()
    {
        var approximateProfitInputs =  $('input[id*="_approximate_profit"]');
        var approximateProfitInputsString = '';

        $.each(approximateProfitInputs, function(){
            var  approximateProfitInputValue = $(this);

            approximateProfitInputsString = approximateProfitInputsString + parseFloat(deformat(approximateProfitInputValue.val())) + ",";
        });
        approximateProfitInputsString = approximateProfitInputsString.substr(0, approximateProfitInputsString.length - 1);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'save_approximate_profit': approximateProfitInputsString,
                'token': token,
                'service-action': 'save_approximate_profit'
            }
        });

        var totalSuggestedPriceFinalInputs =  $('input[id*="_total_suggested_price_final"]');
        var totalSuggestedPriceFinalString = '';
        var totalProfitInt = 0;

        $.each(totalSuggestedPriceFinalInputs, function(){
            var  totalSuggestedPriceFinalInputValue = $(this);

            totalSuggestedPriceFinalString = totalSuggestedPriceFinalString + parseFloat(deformat(totalSuggestedPriceFinalInputValue.val())) + ",";

        });
        totalSuggestedPriceFinalString = totalSuggestedPriceFinalString.substr(0, totalSuggestedPriceFinalString.length - 1);
        if (flag_CC == 1) {
            totalProfitInt = parseFloat(deformat($('input[id="CC_total_suggested_price_final"]').val()));
        }

        if (flag_ACH == 1 && flag_CC == 0) {
            totalProfitInt = parseFloat(deformat($('input[id="ACH_total_suggested_price_final"]').val()));
        }

        if (flag_PAYPAL == 1 && flag_CC == 0 && flag_ACH == 0) {
            totalProfitInt = parseFloat(deformat($('input[id="PAYPAL_total_suggested_price_final"]').val()));
        }

        if (flag_EBAY == 1 && flag_CC == 0 && flag_ACH == 0 && flag_PAYPAL == 0) {
            totalProfitInt = parseFloat(deformat($('input[id="EBAY_total_suggested_price_final"]').val()));
        }

        if (flag_CHECK == 1 && flag_CC == 0 && flag_ACH == 0 && flag_PAYPAL == 0 && flag_EBAY == 0) {
            totalProfitInt = parseFloat(deformat($('input[id="CHECK_total_suggested_price_final"]').val()));
        }

        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'save_total_suggested_price_final': totalSuggestedPriceFinalString,
                'totalProfitInt': totalProfitInt,
                'token': token,
                'service-action': 'save_total_suggested_price_final'
            }
        });

        var appTotalFinalSalePriceInputs =  $('input[id*="_app_total_final_sale_price"]');
        var appTotalFinalSalePriceString = '';

        $.each(appTotalFinalSalePriceInputs, function(){
            var  appTotalFinalSalePriceInputValue = $(this);

            appTotalFinalSalePriceString = appTotalFinalSalePriceString + parseFloat(deformat(appTotalFinalSalePriceInputValue.val())) + ",";
        });
        appTotalFinalSalePriceString = appTotalFinalSalePriceString.substr(0, appTotalFinalSalePriceString.length - 1);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'save_app_total_final_sale_price': appTotalFinalSalePriceString,
                'token': token,
                'service-action': 'save_app_total_final_sale_price'
            }
        });

        var totalUttcNetPriceInputs =  $('input[id*="_total_uttc_net_price"]');
        var totalUttcNetPriceString = '';

        $.each(totalUttcNetPriceInputs, function(){
            var  totalUttcNetPriceInputValue = $(this);

            totalUttcNetPriceString = totalUttcNetPriceString + parseFloat(deformat(totalUttcNetPriceInputValue.val())) + ",";
        });
        totalUttcNetPriceString = totalUttcNetPriceString.substr(0, totalUttcNetPriceString.length - 1);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'save_totalUttcNetPrice': totalUttcNetPriceString,
                'token': token,
                'service-action': 'save_totalUttcNetPrice'
            },
            success: function(){
                //  ////console.log('totalUttcNetPriceString: ' + totalUttcNetPriceString);
            }
        });

        var totalUttcSellPriceInputs =  $('input[id*="_total_uttc_sell_price"]');
        var totalUttcSellPriceString = '';

        $.each(totalUttcSellPriceInputs, function(){
            var  totalUttcSellPriceInputValue = $(this);

            totalUttcSellPriceString = totalUttcSellPriceString + parseFloat(deformat(totalUttcSellPriceInputValue.val())) + ",";
        });
        totalUttcSellPriceString = totalUttcSellPriceString.substr(0, totalUttcSellPriceString.length - 1);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'save_totalUttcSellPrice': totalUttcSellPriceString,
                'token': token,
                'service-action': 'save_totalUttcSellPrice'
            },
            success: function(){
                //////console.log('totalUttcSellPriceString: ' + totalUttcSellPriceString);
            }
        });

        var totalSuggestedPriceInputs =  $('input[id*="_total_suggested_price"]:not(input[id*="_total_suggested_price_final"])');
        var totalSuggestedPriceString = '';

        $.each(totalSuggestedPriceInputs, function(){
            var  totalSuggestedPriceInputValue = $(this);

            totalSuggestedPriceString = totalSuggestedPriceString + parseFloat(deformat(totalSuggestedPriceInputValue.val())) + ",";
        });
        totalSuggestedPriceString = totalSuggestedPriceString.substr(0, totalSuggestedPriceString.length - 1);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'save_totalSuggestedPrice': totalSuggestedPriceString,
                'token': token,
                'service-action': 'save_totalSuggestedPrice'
            },
            success: function(){
                //////console.log('totalSuggestedPriceString: ' + totalSuggestedPriceString);
            }
        });

        var totalApproxProfitInputs =  $('input[id*="_total_approx_profit"]');
        var totalApproxProfitString = '';

        $.each(totalApproxProfitInputs, function(){
            var  totalApproxProfitInputValue = $(this);

            totalApproxProfitString = totalApproxProfitString + parseFloat(deformat(totalApproxProfitInputValue.val())) + ",";
        });
        totalApproxProfitString = totalApproxProfitString.substr(0, totalApproxProfitString.length - 1);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'save_totalApproxProfit': totalApproxProfitString,
                'token': token,
                'service-action': 'save_totalApproxProfit'
            },
            success: function(){
                //////console.log('totalApproxProfitString: ' + totalApproxProfitString);
            }
        });

        var totalFinalSalePriceInputs =  $('input[id*="_total_final_sale_price"]:not(input[id*="_app_total_final_sale_price"])');
        var totalFinalSalePriceString = '';

        $.each(totalFinalSalePriceInputs, function(){
            var  totalFinalSalePriceInputValue = $(this);

            totalFinalSalePriceString = totalFinalSalePriceString + parseFloat(deformat(totalFinalSalePriceInputValue.val())) + ",";
        });
        totalFinalSalePriceString = totalFinalSalePriceString.substr(0, totalFinalSalePriceString.length - 1);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'save_totalFinalSalePrice': totalFinalSalePriceString,
                'token': token,
                'service-action': 'save_totalFinalSalePrice'
            },
            success: function(){
                //////console.log('totalFinalSalePriceString: ' + totalFinalSalePriceString);
            }
        });
    }

    /**
     *  Связка таблиц
     */
    if($('input[name*="conn_tbl"]').length > 0)
    {
        btn = $('.tbl_po_zakazu_kl');
        btn.append('<br/><input type="submit" form="conn" value="Связать таблицы"/>');
        btn.click(function(){
            var conn_tbls = [];
            var conn_each = $('input[name*="conn_tbl"]');
            $.each(conn_each, function(){
                var itm = $(this);
                if(itm.is(':checked')){
                    conn_tbls.push(itm.val());
                }
            });
            ////console.log(conn_tbls);

        });

    }



    //Подсветка если мин профит
    $.minprofit = function () {
        function min_alert(md_sell_price, min_profit_count) {
            md_sell_price.removeClass('min');
            return ++min_profit_count;

        }

        var elem = $('input[name*="_qty"]');

        $.each(elem, function () {
            var obj = $(this);
            var parent = obj.parent().parent();
            var id = parent.attr('id');

            var md_sell_price = $('input[name="' + id + '_uttc_sell_price"]');
            var bb_sell_price = $('input[name="' + id + '_bb_sell_price"]');
            var lower_sale_price = $('input[name="' + id + '_lower_sale_price"]');
            var on_site = $('input[name="' + id + '_on_site"]');
            var must_be = $('input[name="' + id + '_must_be"]');

            if ((on_site.val() != 'no' && on_site.val() != null) && deformat(md_sell_price.val()) !== 0) {
                if ((deformat(md_sell_price.val()) == deformat(bb_sell_price.val()) && deformat(md_sell_price.val()) != deformat(must_be.val())) || (deformat(md_sell_price.val()) > deformat(must_be.val()) && deformat(bb_sell_price.val()) > deformat(must_be.val()) && Math.abs(deformat(md_sell_price.val()) - deformat(bb_sell_price.val())) < deformat(md_sell_price.val()) * 0.001)) {
                    // md_sell_price.addClass('min')
                }else
                {
                    min_profit_count = min_alert(md_sell_price, min_profit_count);
                }

            } else {
                min_profit_count = min_alert(md_sell_price, min_profit_count);
            }

            //////console.log("################################################");
            // ////console.log("count :" + min_profit_count);


        });

    }
    $.minprofit();

    $.tax_refresh = function () {
        var $field = $('select[name*="_selected_brand_for_tax"]');
        $token = $("input[name='data[token]']").val();
        $.each($field, function () {
            $item = $(this);
            var $id = $item.attr('data-id');
            $brand = $item.attr('data-brand');
            var $val = $item.val();

            //alert('val: ' + $val);
            //alert('brand: ' + $brand);
            //alert('$id: ' + $id);

            var tax_array = [];

            $.ajax({
                type: "POST",
                url: 'index.php?page=domestic&action=tax_calculate',
                async: true,
                data: {
                    'token': $token,
                    'brand': $brand
                },
                success: function ($data) {

                    data = $.parseJSON($data);


                    $.each(data, function (k, v) {

                        if ($val == '2') {
                            //alert('1: ' + data[k]['pay_type']);
                            tax_array[data[k]['pay_type']] = data[k]['SUM(suggested_price_data)'];
                        }
                        else {
                            //alert('2: ' + data[k]['pay_type']);
                            tax_array[data[k]['pay_type']] = data[k]['SUM(uttc_net_price_data)'];
                        }

                    });
                    ////console.log(tax_array);
                    var $pt;
                    var $tax_hidden_field = $('input[name="' + $id + '_tax_sum"]');
                    if ($('input[type="checkbox"]#CC').is(":checked")) {
                        $pt = 'CC';
                    } else if ($('input[type="checkbox"]#ACH').is(":checked")) {
                        $pt = 'ACH';
                    } else if ($('input[type="checkbox"]#PAYPAL').is(":checked")) {
                        $pt = 'PAYPAL';
                    } else if ($('input[type="checkbox"]#EBAY').is(":checked")) {
                        $pt = 'EBAY';
                    } else if ($('input[type="checkbox"]#CHECK').is(":checked")) {
                        $pt = 'CHECK';
                    }

                    //alert('price: ' + tax_array[$pt]);
                    $tax_hidden_field.val(tax_array[$pt]);

                    // alert($tax_hidden_field.val());
                    $.tax();

                }
            });
        });
    }
    function print_r(array, return_val) {	// Prints human-readable information about a variable
        //
        // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // + namespaced by: Michael White (http://crestidg.com)

        var output = "", pad_char = " ", pad_val = 4;

        var formatArray = function (obj, cur_depth, pad_val, pad_char) {
            if (cur_depth > 0)
                cur_depth++;

            var base_pad = repeat_char(pad_val * cur_depth, pad_char);
            var thick_pad = repeat_char(pad_val * (cur_depth + 1), pad_char);
            var str = "";

            if (obj instanceof Array) {
                str += "Array\n" + base_pad + "(\n";
                for (var key in obj) {
                    if (obj[key] instanceof Array) {
                        str += thick_pad + "[" + key + "] => " + formatArray(obj[key], cur_depth + 1, pad_val, pad_char);
                    } else {
                        str += thick_pad + "[" + key + "] => " + obj[key] + "\n";
                    }
                }
                str += base_pad + ")\n";
            } else {
                str = obj.toString(); // They didn't pass in an array.... why? -- Do the best we can to output this object.
            }
            ;

            return str;
        };

        var repeat_char = function (len, char) {
            var str = "";
            for (var i = 0; i < len; i++) {
                str += char;
            }
            ;
            return str;
        };

        output = formatArray(array, 0, pad_val, pad_char);

        if (return_val !== true) {
            document.write("<pre>" + output + "</pre>");
            return true;
        } else {
            return output;
        }
    }

    $('select[name*="_selected_brand_for_tax"]').change(function () {
        $token = $("input[name='data[token]']").val();
        $item = $(this);
        var $id = $item.attr('data-id');
        $brand = $item.attr('data-brand');
        $val = $item.val();
        $item.attr('data-flag', $val);
        $data_flag = $item.attr('data-flag');
        //alert($data_flag);


        var $shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
        var tax_array = [];
        $.ajax({
            type: "POST",
            url: 'index.php?page=domestic&action=update_tax_pos',
            data: {
                'token': $token,
                'field_id': $id,
                'tax_pos': $val
            }
        });
        $.ajax({
            type: "POST",
            url: 'index.php?page=domestic&action=tax_calculate',
            async: true,
            data: {
                'token': $token,
                'brand': $brand
            },
            success: function ($data) {

                data = $.parseJSON($data);


                $.each(data, function (k, v) {
                    if ($val == 2) {
                        // alert('1: ' + parseFloat($shipp_to_freeport_customer.val()));
                        tax_array[data[k]['pay_type']] = data[k]['SUM(suggested_price)'];
                    }
                    else {
                        // alert('2: ' + data[k]['pay_type']);
                        tax_array[data[k]['pay_type']] = data[k]['SUM(uttc_net_price)'];
                    }
                });

                var $pt;
                var $tax_hidden_field = $('input[name="' + $id + '_tax_sum"]');
                if ($('input[type="checkbox"]#CC').is(":checked")) {
                    $pt = 'CC';
                } else if ($('input[type="checkbox"]#ACH').is(":checked")) {
                    $pt = 'ACH';
                } else if ($('input[type="checkbox"]#PAYPAL').is(":checked")) {
                    $pt = 'PAYPAL';
                } else if ($('input[type="checkbox"]#EBAY').is(":checked")) {
                    $pt = 'EBAY';
                } else if ($('input[type="checkbox"]#CHECK').is(":checked")) {
                    $pt = 'CHECK';
                }


                $tax_hidden_field.val(tax_array[$pt]);

                // alert($tax_hidden_field.val());
                $.tax();


            }
        });


    });


//Сбрасываем денежный формат значения на обычный при ввводе.
    $('#action_table input').click(function () {
        if ($(this).attr("pay-type") == "CC" || $(this).attr("pay-type") == "ACH" || $(this).attr("pay-type") == "PAYPAL" || $(this).attr("pay-type") == "EBAY") {
            if ($(this).attr("type") != 'button') {
                if ($(this).attr('name').indexOf('msp_link') == -1) {
                    $(this).val(deformat($(this).val()));
                }
            }
            if ($(this).val() == 'NaN') {
                $(this).val('');
            }
        }
    });

    function prform() {
        //Приводим поля к денежному формату
        var net_price_each = $('input[name*="uttc_net_price"]');
        $.each(net_price_each, function () {
            var elem = $(this);
            var id = $(this).attr('name').split('_');

            if (isNaN(elem.val())) {
                elem.val(deformat(elem.val()));
            }

            ($('[name="'+id[0]+'_brand"]').val().toLowerCase() == "ideal") ? elem.val(elem.val()) : elem.val(priceFormat(elem.val())) ;

        });

        var sell_price_each = $('input[name*="uttc_sell_price"]');
        $.each(sell_price_each, function () {
            var elem = $(this);
            if (isNaN(elem.val())) {
                elem.val(deformat(elem.val()));
            }
            elem.val(priceFormat(elem.val()));
        });

        var must_be_each = $('input[name*="must_be"]');
        $.each(must_be_each, function () {
            var elem = $(this);
            if (isNaN(elem.val())) {
                elem.val(rounding(deformat(elem.val())));
            }
            elem.val(priceFormat(elem.val()));
        });

        var min_sell_price_each = $('input[name*="min_sell_price"]');
        $.each(min_sell_price_each, function () {
            var elem = $(this);
            if (isNaN(elem.val())) {
                elem.val(deformat(elem.val()));
            }
            elem.val(priceFormat(elem.val()));

            if (deformat(elem.val()) == 0) {
                elem.val('N/A');
            }
        });

        var suggested_price_each = $('input[name*="suggested_price"]');
        $.each(suggested_price_each, function () {
            var elem = $(this);
            if (isNaN(elem.val())) {
                elem.val(deformat(elem.val()));
            }
            elem.val(priceFormat(elem.val()));
        });


        var final_sale_price_each = $('input[name*="final_sale_price"]');
        $.each(final_sale_price_each, function () {
            var elem = $(this);
            if (isNaN(elem.val())) {
                elem.val(deformat(elem.val()));
            }
            elem.val(priceFormat(elem.val()));
        });
    }

    prform();


    $('select[name="br_link"]').change(function () {
        var link = $('select[name="br_link"]').val();


        window.open(
            link,
            '_blank'
        );


    });
    $("div#tbb").hide();
    $("span.showhide_tbb").click(function () {

        var pos = $("span.showhide_tbb").attr('data');
        if (pos == 0) {
            $("span.showhide_tbb").attr('data', 1);
            $("div#tbb").hide("slow");
        }
        else {
            $("span.showhide_tbb").attr('data', 0);
            $("div#tbb").show("slow");
        }
    });



    /* function empty(selector, empty_value)
     {
     if($(selector).val() == empty_value || $(selector).val() == 'null')
     {
     $(selector).css('background', 'red');
     }

     }*/

    //ShowHide InfoBlock

    $("span.showhide").click(function () {

        var pos = $("span.showhide").attr('data');
        if (pos == 0) {
            $("span.showhide").attr('data', 1);
            $("div#info_block").hide("slow");
        }
        else {
            $("span.showhide").attr('data', 0);
            $("div#info_block").show("slow");
        }
    });

    $("span.showhide2").click(function () {
        var pos = $("span.showhide").attr('data');
        if (pos == 0) {
            $("span.showhide").attr('data', 1);
            $("div#info_block").hide("slow");
        }
        else {
            $("span.showhide").attr('data', 0);
            $("div#info_block").show("slow");
        }
    });

    var clipboard, getContent, hideTooltip, setTooltip;
    getContent = $('#customer_email').parent();
    getContent.append('<i class="copy"></i>');
    $('a i.copy').on('click', function(e) {
        e.preventDefault();
    });
    setTooltip = function(btn, message) {
        $(btn).tooltip('hide').attr('data-original-title', message).tooltip('show');
    };
    hideTooltip = function(btn) {
        setTimeout((function() {
            $(btn).tooltip('hide');
        }), 1000);
    };
    $('.copy').tooltip({
        trigger: 'click',
        placement: 'top'
    });
    clipboard = new ClipboardJS('.copy', {
        text: function(trigger) {
            return $(trigger).parent().find('#customer_email').val();
        }
    });
    clipboard.on('success', function(e) {
        setTooltip(e.trigger, 'Copied!');
        hideTooltip(e.trigger);
    });
    clipboard.on('error', function(e) {
        setTooltip(e.trigger, 'Failed!');
        hideTooltip(e.trigger);
    });

    //function copy customer email to buffer
    // $(document).on("click", 'input#customer_email', function (e) {
    //     var buffer = $('input#customer_email');
    //     //window.prompt("Для сохранения нажиите Ctrl+C или Enter", buffer.val());
    //     buffer.focus();
    //     buffer.select();
    //
    //     var clip = null;
    //
    //     clip = new ZeroClipboard.Client();
    //     clip.setHandCursor(true);
    //     clip.addEventListener('load', function (client) {
    //         alert('load');
    //     });
    //     clip.addEventListener('mouseOver', function (client) {
    //         alert('mouseOver');
    //         clip.setText(gebid('customer_email').value);
    //     });
    //     clip.addEventListener('complete', function (client, text) {
    //         alert('complete');
    //     });
    //     clip.glue('copy_btn');
    // });

    // empty('select#state', '0||');
    /* var page_type = $('input#page-status').val();
     if(page_type != 'new' && page_type != 'edit')
     { */
    /*   $('input').attr('readonly', false);
     $('textarea').attr('readonly',  false);*/
    /* }*/
    calcs(pay_types);
    if (table_status == 'Approved') {
        $('#action_table input').attr('readonly', true);
        $('#action_table textarea').attr('readonly', true);
        $('#action_table select').attr('disabled', true);
        $('#action_table button').attr('disabled', true);
        $('input[name*="__checkbox"]').attr('disabled', true);
        $('select[id*="_handling_fee"]').attr('disabled', true);
    }

    if (table_status == 'Waiting for Aprv' && access_lvl < 3) {
        $('#action_table input').attr('readonly', true);
        $('#action_table textarea').attr('readonly', true);
        $('#action_table select').attr('disabled', true);
        $('#action_table button').attr('disabled', true);
        $('input[name*="__checkbox"]').attr('disabled', true);
        $('select[id*="_handling_fee"]').attr('disabled', true);
    }

    if (table_status == 'New') {
        $('#action_table input').attr('readonly', false);
        $('#action_table textarea').attr('readonly', false);
        $('#action_table select').attr('disabled', false);
        $('#action_table button').attr('disabled', false);
        $('input[name*="__checkbox"]').attr('disabled', false);
        $('select[id*="_handling_fee"]').attr('disabled', false);
    }

    if (table_status == 'Revision') {
        $('#action_table input').attr('readonly', false);
        $('#action_table button').attr('disabled', false);
        $('#action_table textarea').attr('readonly', false);
        $('#action_table select').attr('disabled', false);
        $('#zip').attr('disabled', false);
        $('input[name*="__checkbox"]').attr('disabled', false);
        $('select[id*="_handling_fee"]').attr('disabled', false);
    }

    if (table_status == 'Manual Closed') {
        $('#action_table input').attr('readonly', true);
        $('#action_table textarea').attr('readonly', true);
        $('#action_table select').attr('disabled', true);
        $('#action_table button').attr('disabled', true);
        $('#zip').attr('disabled', true);
        $('input[name*="__checkbox"]').attr('disabled', false);
        $('select[id*="_handling_fee"]').attr('disabled', false);
        $('#table_close').hide();
        $('.nav-head-error').html('<h3 style="text-align: center; border: 3px solid red;"><span style="text-decoration: underline;">Manual Closed</span><br/>Причина: ' + reason + '</h3>');

    }

    // ////console.log($('input[name*="__checkbox"]').length);
    if(($('input[name*="__checkbox"]').length > 0)){
        ////console.log('HF loaded');
        var handling_fee_select = $('select[id*="_handling_fee"]');
        var handling_fee_checkbox = $('input[name*="__checkbox"]');
        var handling_fee = $('#handling_fee');
        // handling_fee_select.attr('disabled', true);
        var each_elems = $('input[type="checkbox"][name*="__checkbox"]');
        $.each(each_elems, function(){
            item = $(this);
            id = item.attr('data-id');
            //    data_pos = item.attr('data-pos')
            if(item.attr('data-pos') == 1)
            {
                item.attr('checked', true);
            }
            if(item.is(':checked')){
                if(table_status == 'Approved')
                {
                    $('select[id="'+id+'_handling_fee"]').attr('disabled', true);
                }else {
                    $('select[id="' + id + '_handling_fee"]').attr('disabled', false);
                }
                handling_fee.val($('select[id="'+id+'_handling_fee"]').val());
            }
            if(item.is(':not(:checked)')){
                $('select[id="'+id+'_handling_fee"]').attr('disabled', true);
                handling_fee.val(0);
            }
        });

        $('select[id*="_handling_fee"], input[name*="__checkbox"]').change(function(){
            e = $(this);
            data_id = e.attr('data-id');
            select = $('select[id="'+data_id+'_handling_fee"]');
            check = $('input[name="' + data_id + '__checkbox"]');

            if(check.is(':checked')){
                select.attr('disabled', false);
                handling_fee.val(select.val());
            }
            if(check.is(':not(:checked)')){
                select.attr('disabled', true);
                handling_fee .val(0);
            }
            // ////console.log('HF click');
            var handling_fee_pos = '';
            var handling_fee_amount = '';
            $.each(each_elems, function(){
                item = $(this);
                var id = item.attr('data-id');
                if(item.is(':checked'))
                {
                    item.attr('data-pos', 1);
                    handling_fee.val($('select[id="'+id+'_handling_fee"]').val());
                }else if(item.is(':not(:checked)'))
                {
                    item.attr('data-pos', 0);
                    handling_fee.val(0);
                }
                sel_fee = $('select[id="' + id + '_handling_fee"]').val();

                handling_fee_pos += item.attr('data-pos')+',';
                handling_fee_amount += sel_fee+',';


            });
            handling_fee_pos = handling_fee_pos.substring(0, handling_fee_pos.length - 1);
            handling_fee_amount = handling_fee_amount.substring(0, handling_fee_amount.length - 1);
            //////console.log('handling_fee_pos: ' + handling_fee_pos);
            //////console.log('handling_fee_amount: ' + handling_fee_amount);

            $.ajax({
                type: 'POST',
                url: 'index.php?page=ajax&action=handling_fee',
                data: {
                    handling_fee: handling_fee_pos,
                    handling_fee_amount: handling_fee_amount,
                    token: token
                }
            });

        });

        //////console.log("minOrderFee_Obj:"  + $('select[id*="_minOrderFee"]').length);
        if(($('input[id*="_minOrderFee"]').length > 0))
        {
            mof = $('input[id*="_minOrderFee"]');
            $.each(mof, function(){
                minOrderFee += parseFloat($(this).val());
            });
        }
        // ////console.log('minOrderFee: ' + minOrderFee);
        if(($('input[id*="_handlingTariff"]').length > 0))
        {
            ht = $('input[id*="_handlingTariff"]');
            $.each(ht, function(){
                handlingTariff += parseFloat($(this).val());
            });
        }
        // ////console.log('handlingTariff: ' + handlingTariff);
        if(($('input[id*="_dropShipmentTariff"]').length > 0))
        {
            dst = $('input[id*="_dropShipmentTariff"]');
            $.each(dst, function(){
                dropShipmentTariff += parseFloat($(this).val());
            });

        }
        sum = parseFloat(minOrderFee) + parseFloat(handlingTariff) + parseFloat(dropShipmentTariff);

        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'service-action': 'manuf_handling_fee',
                'token': $("input[name='data[token]']").val(),
                'manuf_handling_fee': sum
            }
        });

        //  ////console.log('dropShipmentTariff: ' + dropShipmentTariff);
        //  ////console.log('sum: ' + sum);
        $.handling_fee_recalc();
        // ////console.log('handling fee: ' + handling);
        // ////console.log('minOrderFee: ' + minOrderFee);
        // ////console.log('handlingTarrif: '+ handlingTariff);
        // ////console.log('dropShipmentTariff: ' + dropShipmentTariff);



    } else {
        if(($('input[id*="_dropShipmentTariff"]').length > 0))
        {
            dst = $('input[id*="_dropShipmentTariff"]');
            $.each(dst, function(){
                dropShipmentTariff += parseFloat($(this).val());
            });
        }
        if(($('input[id*="_minOrderFee"]').length > 0))
        {
            mof = $('input[id*="_minOrderFee"]');
            $.each(mof, function(){
                minOrderFee += parseFloat($(this).val());
            });
        }
        if(($('input[id*="_handlingTariff"]').length > 0))
        {
            ht = $('input[id*="_handlingTariff"]');
            $.each(ht, function(){
                handlingTariff += parseFloat($(this).val());
            });
        }
    }

    //ФЛаги
    function chk_remove_b() {
        flag_CC = $('input#CC_flag').attr('CC-flag');
        flag_ACH = $('input#ACH_flag').attr('ACH-flag');
        flag_PAYPAL = $('input#PAYPAL_flag').attr('PAYPAL-flag');
        flag_EBAY = $('input#EBAY_flag').attr('EBAY-flag');
        flag_CHECK = $('input#CHECK_flag').attr('CHECK-flag');

        if (flag_CC == 1) {
            $('table.CC').show();
            $('div#rhCC').show();
        }
        else {
            $('table.CC').hide();
            $('div#rhCC').hide();

        }

        if (flag_ACH == 1) {
            $('table.ACH').show();
            $('div#rhACH').show();
        }
        else {
            $('table.ACH').hide();
            $('div#rhACH').hide();
        }

        if (flag_PAYPAL == 1) {
            $('table.PAYPAL').show();
            $('div#rhPAYPAL').show();
        }
        else {
            $('table.PAYPAL').hide();
            $('div#rhPAYPAL').hide();
        }

        if (flag_EBAY == 1) {
            $('table.EBAY').show();
            $('div#rhEBAY').show();
        }
        else {
            $('table.EBAY').hide();
            $('div#rhEBAY').hide();
        }

        if (flag_CHECK == 1) {
            $('table.CHECK').show();
            $('div#rhCHECK').show();
        }
        else {
            $('table.CHECK').hide();
            $('div#rhCHECK').hide();
        }

        if (flag_CC == 1) {
            $('a.rbhACH').hide();
            $('a.rbhPAYPAL').hide();
            $('a.rbhEBAY').hide();
            $('a.rbhCHECK').hide();

            $('._CC').removeClass('disable');
            $('._ACH').addClass('disable');
            $('._PAYPAL').addClass('disable');
            $('._EBAY').addClass('disable');
            $('._CHECK').addClass('disable');
        }

        if (flag_ACH == 1 && flag_CC == 0) {
            $('a.rbhACH').show();
            $('a.rbhEBAY').hide();

            $('._CC').addClass('disable');
            $('._PAYPAL').addClass('disable');
            $('._EBAY').addClass('disable');
            $('._CHECK').addClass('disable');
            $('._ACH').removeClass('disable');

        }

        if (flag_PAYPAL == 1 && flag_CC == 0 && flag_ACH == 0) {
            $('a.rbhPAYPAL').show();
            $('a.rbhACH').hide();
            $('a.rbhEBAY').hide();

            $('._CC').addClass('disable');
            $('._ACH').addClass('disable');
            $('._EBAY').addClass('disable');
            $('._CHECK').addClass('disable');
            $('._PAYPAL').removeClass('disable');


        }

        if (flag_EBAY == 1 && flag_CC == 0 && flag_ACH == 0 && flag_PAYPAL == 0) {
            $('a.rbhEBAY').show();

            $('._CC').addClass('disable');
            $('._ACH').addClass('disable');
            $('._PAYPAL').addClass('disable');
            $('._CHECK').addClass('disable');
            $('._EBAY').removeClass('disable');
        }

        if (flag_CHECK == 1 && flag_CC == 0 && flag_ACH == 0 && flag_PAYPAL == 0 && flag_EBAY == 0) {
            $('a.rbhCHECK').show();

            $('._CC').addClass('disable');
            $('._ACH').addClass('disable');
            $('._PAYPAL').addClass('disable');
            $('._EBAY').addClass('disable');
            $('._CHECK').removeClass('disable');
        }


        flag_CC = $('input#CC_flag').attr('CC-flag');
        flag_ACH = $('input#ACH_flag').attr('ACH-flag');
        flag_PAYPAL = $('input#PAYPAL_flag').attr('PAYPAL-flag');
        flag_EBAY = $('input#EBAY_flag').attr('EBAY-flag');
        flag_CHECK = $('input#CHECK_flag').attr('CHECK-flag');

        $.ajax({
            type: 'POST',
            url: 'index.php?page=tables&action=set_flags',
            data: {
                'token': $("input[name='data[token]']").val(),
                'flags': flag_CC + ',' + flag_ACH + ',' + flag_PAYPAL + ',' + flag_EBAY + ',' + flag_CHECK
            }
        });

        calcs(pay_types);

    }

    $('input[type="checkbox"]').click(function () {
        var meth = $(this).attr('id');
        if ($('input[type="checkbox"]#CC').is(":checked")) {
            $('input#CC_flag').attr('CC-flag', 1);

        }
        else if ($('input[type="checkbox"]#CC').is(":not(:checked)")) {
            $('input#CC_flag').attr('CC-flag', 0);

        }

        if ($('input[type="checkbox"]#ACH').is(":checked")) {
            $('input#ACH_flag').attr('ACH-flag', 1);

        }
        else if ($('input[type="checkbox"]#ACH').is(":not(:checked)")) {
            $('input#ACH_flag').attr('ACH-flag', 0);

        }

        if ($('input[type="checkbox"]#PAYPAL').is(":checked")) {
            $('input#PAYPAL_flag').attr('PAYPAL-flag', 1);

        }
        else if ($('input[type="checkbox"]#PAYPAL').is(":not(:checked)")) {
            $('input#PAYPAL_flag').attr('PAYPAL-flag', 0);

        }

        if ($('input[type="checkbox"]#EBAY').is(":checked")) {
            $('input#EBAY_flag').attr('EBAY-flag', 1);

        }
        else if ($('input[type="checkbox"]#EBAY').is(":not(:checked)")) {
            $('input#EBAY_flag').attr('EBAY-flag', 0);
        }

        if ($('input[type="checkbox"]#CHECK').is(":checked")) {
            $('input#CHECK_flag').attr('CHECK-flag', 1);
        }
        else if ($('input[type="checkbox"]#CHECK').is(":not(:checked)")) {
            $('input#CHECK_flag').attr('CHECK-flag', 0);
        }


        chk_remove_b();
    });
    chk_remove_b();

    $('#action_table textarea').change(function () {
        calcs(pay_types);
    });

    $('#action_table input').change(function () {

        if ($(this).attr('type') == 'checkbox' && $(this).data('check')) {
            return;
        }
        //#################################################################################################################
        pn = $(this).attr('pn');
        obj = null;
        if($(this).attr('pn')){
            obj = $(this);
        }
        rewrite(pn,obj);


        function rewrite(pn, obj) {


            elems = $('input[pn="' + pn + '"]');
            $.each(elems, function () {
                attr = $(this).attr('name');
                var pay_type = $(this).attr('pay-type');
                if (pay_type) {
                    qty = $('input[pay-type="'+pay_type+'"][pn="' + pn + '"][name*="_qty"]').val();
                    // qty = obj.val();
                    uttc_net_price = $('input[pay-type="'+pay_type+'"][pn="' + pn + '"][name*="_uttc_net_price"]').val();
                    uttc_sell_price = $('input[pay-type="'+pay_type+'"][pn="' + pn + '"][name*="_uttc_sell_price"]').val();
                    min_sell_price = $('input[pay-type="'+pay_type+'"][pn="' + pn + '"][name*="_min_sell_price"]').val();

                    if(!uttc_net_price) uttc_net_price = $('input[pay-type="CC"][pn="' + pn + '"][name*="_uttc_net_price"]').val();
                    if(!uttc_sell_price) uttc_sell_price = $('input[pay-type="CC"][pn="' + pn + '"][name*="_uttc_sell_price"]').val();
                    if(!min_sell_price) min_sell_price = $('input[pay-type="CC"][pn="' + pn + '"][name*="_min_sell_price"]').val();

                    $('input[pay-type="ACH"][pn="' + pn + '"][name*="_qty"]').val(qty);
                    $('input[pay-type="PAYPAL"][pn="' + pn + '"][name*="_qty"]').val(qty);
                    $('input[pay-type="EBAY"][pn="' + pn + '"][name*="_qty"]').val(qty);
                    $('input[pay-type="CC"][pn="' + pn + '"][name*="_qty"]').val(qty);
                    $('input[pay-type="ACH"][pn="' + pn + '"][name*="_uttc_net_price"]').val(uttc_net_price);
                    $('input[pay-type="PAYPAL"][pn="' + pn + '"][name*="_uttc_net_price"]').val(uttc_net_price);
                    $('input[pay-type="ACH"][pn="' + pn + '"][name*="_uttc_sell_price"]').val(uttc_sell_price);
                    $('input[pay-type="PAYPAL"][pn="' + pn + '"][name*="_uttc_sell_price"]').val(uttc_sell_price);
                    $('input[pay-type="ACH"][pn="' + pn + '"][name*="_min_sell_price"]').val(min_sell_price);
                    $('input[pay-type="PAYPAL"][pn="' + pn + '"][name*="_min_sell_price"]').val(min_sell_price);


                }

                // if (flag_CC == 0 && flag_ACH == 1) {
                //     qty = $('input[pay-type="ACH"][pn="' + pn + '"][name*="_qty"]').val();
                //     uttc_net_price = $('input[pay-type="ACH"][pn="' + pn + '"][name*="_uttc_net_price"]').val();
                //     uttc_sell_price = $('input[pay-type="ACH"][pn="' + pn + '"][name*="_uttc_sell_price"]').val();
                //     min_sell_price = $('input[pay-type="ACH"][pn="' + pn + '"][name*="_min_sell_price"]').val();
                //
                //
                //     $('input[pay-type="PAYPAL"][pn="' + pn + '"][name*="_qty"]').val(qty);
                //     $('input[pay-type="PAYPAL"][pn="' + pn + '"][name*="_uttc_net_price"]').val(uttc_net_price);
                //     $('input[pay-type="PAYPAL"][pn="' + pn + '"][name*="_uttc_sell_price"]').val(uttc_sell_price);
                //     $('input[pay-type="PAYPAL"][pn="' + pn + '"][name*="_min_sell_price"]').val(min_sell_price);
                //
                // }
            });


        }

        var parent = $(this).parent().parent();
        $id = parent.attr('id');

        $pay_type = $(this).attr('pay-type');
        calcs(pay_types);
        recalc($pay_type, $id);
        //#################################################################################################################

        /*function cc_calc() {
         var row = $('input.partnum');
         $.each(row, function () {
         qty_CC = $('input[pay-type="CC"][name*="_qty"]').val();
         uttc_net_price_CC = $('input[pay-type="CC"][name*="_uttc_net_price"]').val();
         uttc_sell_price_CC = $('input[pay-type="CC"][name*="_uttc_sell_price"]').val();
         min_sell_price_CC = $('input[pay-type="CC"][name*="_min_sell_price"]').val();
         msp_link_CC = $('input[pay-type="CC"][name*="_msp_link"]').val();
         status_CC = $('input[pay-type="CC"][name*="_status"]').val();

         par = $(this).parent().parent();
         id = par.attr('id');


         $('input[pay-type="ACH"][name="' + id + '_qty"]').val(qty_CC);
         $('input[pay-type="ACH"][name="' + id + '_uttc_net_price"]').val(uttc_net_price_CC);
         $('input[pay-type="ACH"][name="' + id + '_uttc_sell_price"]').val(uttc_sell_price_CC);
         $('input[pay-type="ACH"][name="' + id + '_min_sell_price"]').val(min_sell_price_CC);
         $('input[pay-type="ACH"][name="' + id + '_msp_link"]').val(msp_link_CC);
         $('input[pay-type="ACH"][name="' + id + '_status"]').val(status_CC);

         $('input[pay-type="PAYPAL"][name="' + id + '_qty"]').val(qty_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_uttc_net_price"]').val(uttc_net_price_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_uttc_sell_price"]').val(uttc_sell_price_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_min_sell_price"]').val(min_sell_price_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_msp_link"]').val(msp_link_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_status"]').val(status_CC);


         });
         }

         function ach_calc() {
         var row = $('input.partnum');
         $.each(row, function () {
         qty_CC = $('input[pay-type="ACH"][name*="_qty"]').val();
         uttc_net_price_CC = $('input[pay-type="ACH"][name*="_uttc_net_price"]').val();
         uttc_sell_price_CC = $('input[pay-type="ACH"][name*="_uttc_sell_price"]').val();
         min_sell_price_CC = $('input[pay-type="ACH"][name*="_min_sell_price"]').val();
         msp_link_CC = $('input[pay-type="ACH"][name*="_msp_link"]').val();
         status_CC = $('input[pay-type="ACH"][name*="_status"]').val();

         par = $(this).parent().parent();
         id = par.attr('id');

         $('input[pay-type="PAYPAL"][name="' + id + '_qty"]').val(qty_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_uttc_net_price"]').val(uttc_net_price_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_uttc_sell_price"]').val(uttc_sell_price_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_min_sell_price"]').val(min_sell_price_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_msp_link"]').val(msp_link_CC);
         $('input[pay-type="PAYPAL"][name="' + id + '_status"]').val(status_CC);


         });
         }

         var pm = $(this).attr('pay-type');
         if (typeof pm !== 'undefined') {
         if (pm == 'CC') {
         cc_calc();
         }

         if (pm == 'ACH') {
         if ($('input[type="checkbox"][id="CC"]').is(":checked")) {
         cc_calc();
         }
         else {
         ach_calc();
         }
         }

         if (pm == 'PAYPAL') {
         if ($('input[type="checkbox"][id="CC"]').is(":checked")) {
         cc_calc();
         } else if ($('input[type="checkbox"][id="ACH"]').is(":checked")) {
         ach_calc();
         }
         }


         }

         function auto_rewrite(pm) {

         var row = $('input.partnum');
         $.each(row, function () {
         qty_CC = $('input[pay-type="CC"][name*="_qty"]').val();
         uttc_net_price_CC = $('input[pay-type="CC"][name*="_uttc_net_price"]').val();
         uttc_sell_price_CC = $('input[pay-type="CC"][name*="_uttc_sell_price"]').val();
         min_sell_price_CC = $('input[pay-type="CC"][name*="_min_sell_price"]').val();
         msp_link_CC = $('input[pay-type="CC"][name*="_msp_link"]').val();
         status_CC = $('input[pay-type="CC"][name*="_status"]').val();

         par = $(this).parent().parent();
         id = par.attr('id');

         if (pay_method != 'EBAY') {
         $('input[pay-type="' + pay_method + '"][name="' + id + '_qty"]').val(qty_CC);
         $('input[pay-type="' + pay_method + '"][name="' + id + '_uttc_net_price"]').val(uttc_net_price_CC);
         $('input[pay-type="' + pay_method + '"][name="' + id + '_uttc_sell_price"]').val(uttc_sell_price_CC);
         $('input[pay-type="' + pay_method + '"][name="' + id + '_min_sell_price"]').val(min_sell_price_CC);
         $('input[pay-type="' + pay_method + '"][name="' + id + '_msp_link"]').val(msp_link_CC);
         $('input[pay-type="' + pay_method + '"][name="' + id + '_status"]').val(status_CC);
         }


         });

         }*/

        location.reload(true);
    });
//#######################################
    $.widget("ui.autocomplete", $.ui.autocomplete, {
        options: {
            maxItems: 100000
        },
        _renderMenu: function (ul, items) {
            var that = this,
                count = 0;
            $.each(items, function (index, item) {
                if (count < that.options.maxItems) {
                    that._renderItemData(ul, item);
                }
                count++;
            });
        }
    });
//#######################################
    //Получаем список брендов
    $.ajax({
        type: "POST",
        url: 'index.php?page=domestic&action=get_brand_list',
        async: false,
        success: function (data) {

            data = $.parseJSON(data);

            for (var i = 0; i < data.length; i++) {

                brands[i] = data[i].Tables_in_primebuy_price;
            }


        }
    });
//#######################################
//AutoComplete для брендов
//#######################################
    $(document).on("click", "input.brandtag", function () {
        $(".brandtag").autocomplete({
            source: brands,
            select: function(event, ui) {
                $('.nav-head-form .qty').data('brand_input', ui.item.value)
                $('.nav-head-form .qty').attr('data-brand_input', ui.item.value)
            },
        });

    });
//Выбор бренда
//#######################################
    $(document).on("blur", 'input.brandtag', function (e) {

        this_brand = $(this).val();
        //$(this).attr("readonly", true);

        $.ajax({
            type: "POST",
            url: "index.php?page=domestic&action=get_partnum_by_brand&brand=" + encodeURIComponent(this_brand),
            success: function (data) {

                data = $.parseJSON(data);
                // ////console.log(data);
                for (var i = 0; i < data.length; i++) {
                    partnum[i] = data[i].part_numb;
                }
                $(".partnum").autocomplete({
                    source: partnum,
                    maxItems: 5
                });
            }
        });
    });

    $(document).on("blur", 'input.partnum', function (e) {
        self = $(this)
        partnum_input = self.val();
        brand_input = self.next('.qty').data('brand_input')
        field_input = 'manuf_box_qty';
        console.log('self')
        console.log(self)
        console.log(self.next('.qty').data('brand_input'))
        $.ajax({
            type: "POST",
            // url: "index.php?page=export&action=get_partnum_by_brand&brand=" + encodeURIComponent(this_brand),
            url: "index.php?page=export&action=get_manuf_box_qty&brand=" + encodeURIComponent(brand_input) + "&partnum=" + encodeURIComponent(partnum_input) + "&field=" + encodeURIComponent(field_input),
            success: function (data) {

                data = $.parseJSON(data);

                self.next('input.qty').attr('data-manuf_qty',data['box_qty'])
                    .attr('step',data['box_qty'])
                    .attr('min',data['box_qty'])

                console.log(data)

            }
        });
    });




//Freeport выпадающий список
//#######################################
    $('select#freeport').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'freeport',
                'token': $("input[name='data[token]']").val(),
                'freeport': val
            },
            success: function () {

            }
        });
    });
//#######################################
    $('input#shipp_to_freeport').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'shipp_to_freeport',
                'token': $("input[name='data[token]']").val(),
                'shipp_to_freeport': val
            },
            success: function () {

            }

        });
    });
//#######################################
    $('input#shipp_to_freeport_customer').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'shipp_to_freeport_customer',
                'token': $("input[name='data[token]']").val(),
                'shipp_to_freeport_customer': val
            },
            success: function () {
                window.location.reload();
            }

        });
    });

    $('input[name*="_qty"]').change(function(){
        window.location.reload();
    });

    //$('input#source').change(function () {
    $('select#source').change(function () {
        console.log('qwe: ' + $("input[name='data[token]']").val());

        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'source',
                'token': $("input[name='data[token]']").val(),
                'source': val
            },
            success: function () {

            }
        });
    });

    /**
     * Обновление поля Store from
     */
    $('select#store_from').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'store_from',
                'token': $("input[name='data[token]']").val(),
                'store_from': val
            },
            success: function () {
                location.reload();
            }
        });
    });

    $('select#state').change(function () {
        // empty('select#state', '0||');
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'state',
                'token': $("input[name='data[token]']").val(),
                'state': val
            },
            success: function (response, result) {
                if (result == 'success') {
                    location.reload();
                }

            }
        });
    });

    $('select#country-select').change(function () {
        // empty('select#state', '0||');
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'country',
                'token': $("input[name='data[token]']").val(),
                'country': val
            },
            success: function (response, result) {
                if (result == 'success') {
                    location.reload();
                }

            }
        });
    });

    $('#cs_msg').change(function () {
        val = $(this).val();

        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'cs_msg',
                'token': $("input[name='data[token]']").val(),
                'cs_msg': val
            }

        });
    });

    $('#zip').change(function () {
        val = $(this).val();

        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'zip',
                'token': $("input[name='data[token]']").val(),
                'zip': val
            },
            success: function (response, result) {
                if (result == 'success') {
                    location.reload();
                }

            }
        });
    });

    $('textarea#customer').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'customer',
                'token': $("input[name='data[token]']").val(),
                'customer': val
            },
            success: function () {

            }
        });
    });
    $('textarea#approval_comment').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'approval_comment',
                'token': $("input[name='data[token]']").val(),
                'approval_comment': val
            },
            success: function () {

            }
        });
    });
//// need add+
    $('input#rfq').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'rfq',
                'token': $("input[name='data[token]']").val(),
                'rfq': val
            },
            success: function () {
                console.log("RFQ update SUCCESS");
            },
            error: function (error) {
                console.log("RFQ NOT update, was error "+ JSON.stringify(error));
            }
        });
    });

    $('input#link_to_customer').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'link_to_customer',
                'token': $("input[name='data[token]']").val(),
                'link_to_customer': val
            },
            success: function () {

            }
        });
    });

    $('input#made_by').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'made_by',
                'token': $("input[name='data[token]']").val(),
                'made_by': val
            },
            success: function () {

            }
        });
    });


    $('select#freeshipping_on_site').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'freeshipping_on_site',
                'token': $("input[name='data[token]']").val(),
                'freeshipping_on_site': val
            },
            success: function () {

            }
        });
    });

//Freeshipping выпадающий список
//#######################################
    $('select#freeshipp_to_customer').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'freeshipp_to_customer',
                'token': $("input[name='data[token]']").val(),
                'freeshipp_to_customer': val
            },
            success: function () {
                window.location.reload();
            }
        });
    });
//#######################################
    $('select#freeshipping_mfg').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'freeshipping_mfg',
                'token': $("input[name='data[token]']").val(),
                'freeshipping_mfg': val
            },
            success: function () {

            }
        });
    });

    $('input#tax_percent').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'tax_percent',
                'token': $("input[name='data[token]']").val(),
                'tax_percent': val
            },
            success: function () {

            }
        });
    });


    $('input#tax_sum').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'tax_sum',
                'token': $("input[name='data[token]']").val(),
                'tax_sum': val
            },
            success: function () {

            }
        });
    });


    $('textarea#other_notes').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'other_notes',
                'token': $("input[name='data[token]']").val(),
                'other_notes': val
            },
            success: function () {

            }
        });
    });


    $('textarea#important_notes').change(function () {
        val = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'important_notes',
                'token': $("input[name='data[token]']").val(),
                'important_notes': val
            },
            success: function () {

            }
        });
    });
    $('input[name*="_final_sale_price"]').change(function () {
        if (table_status == 'Waiting for Aprv') {
            var fsp = $('input[name*="_final_sale_price"]');

            $.each(fsp, function () {
                var id = $(this).parent().parent().attr('id');
                var final_sale_price = $('input[name="' + id + '_final_sale_price"]').val();
                var suggested_price = $('input[name="' + id + '_suggested_price"]').val();
                if (deformat(final_sale_price) != deformat(suggested_price)) {
                    fsp_editable = 1;
                    $.ajax({
                        type: 'POST',
                        url: 'index.php?page=tables&action=edit_table_check',
                        data: {
                            'token': $("input[name='data[token]']").val()
                        }
                    });
                    return;
                }
            });

        }
        // ////console.log(fsp_editable);
    });



    $('input#approve').click(function () {
        var conf = true;
        var c = 0;
        if (fsp_editable == 1) {
            var fsp = $('input[name*="_final_sale_price"]');

            $.each(fsp, function () {
                var id = $(this).parent().parent().attr('id');
                var final_sale_price = $('input[name="' + id + '_final_sale_price"]').val();
                var suggested_price = $('input[name="' + id + '_suggested_price"]').val();
                if (deformat(final_sale_price) != deformat(suggested_price)) {
                    if (c == 0) {
                        conf = confirm("Final Sale Price отличается от Suggested Price");
                        c++;
                    }
                }
            });
        }

        if($.isOver5000())
        {
            //////console.log($.inArray(user_email, seniors));
            //////console.log(seniors);
            if($.inArray(user_email, seniors) != -1){
                var over = confirm("Вы подтверждаете оплату CC на сумму больше $5000 ?");
                if(over)
                {
                    $.ajax({
                        type: 'POST',
                        url: 'index.php?page=tables&action=add_comment',
                        data: {
                            'comment': e_name + ' подтвердил оплату CC на сумму более $5000',
                            'token': token,
                            'over5000': 1

                        }
                    });
                }
                else
                {
                    return;
                }
            }
        }

        if($.isUnder1000())
        {
            //////console.log($.inArray(user_email, seniors));
            //////console.log(seniors);
            if($.inArray(user_email, seniors) != -1){
                var over = confirm("Вы подтверждаете оплату ACH на сумму меньше $1000 ?");
                if(over)
                {
                    $.ajax({
                        type: 'POST',
                        url: 'index.php?page=tables&action=add_comment',
                        data: {
                            'comment': e_name + ' подтвердил оплату ACH на сумму менее $1000',
                            'token': token,
                            'over5000': 1

                        }
                    });
                }
                else
                {
                    return;
                }
            }
        }
        if (!conf) {
            return;
        }

        var diff = 0;

        if ($('input[type="checkbox"]#CC').is(":checked")) {
            diff = differences(diff, 'CC');
        }

        if ($('input[type="checkbox"]#ACH').is(":checked")) {
            diff = differences(diff, 'ACH');
        }

        if ($('input[type="checkbox"]#PAYPAL').is(":checked")) {
            diff = differences(diff, 'PAYPAL');
        }

        if ($('input[type="checkbox"]#EBAY').is(":checked")) {
            diff = differences(diff, 'EBAY');
        }



        if(diff > 0)
        {
            if(!confirm("Final Sale Price отличается от Suggested Price. Данные введены правильно?"))
            {
                return;
            }
        }

        /**
         * Добавление к комменту дзаметок
         */
        var inputs_array = [$('#freeshipp_to_customer'), $('#freeshipping_on_site'), $('#freeshipping_mfg'), $('#freeport')];
        if($('input[name*="__checkbox"]').length > 0){
            $.each($('input[name*="__checkbox"]'), function(){
                var id = $(this).attr('data-id');
                inputs_array.push($('input[name="'+id+'__checkbox"]'));
            });
        }

        var notes = "";
        var fields_txt = "<table style=\"width: 19em;\">";
        $.each(inputs_array, function(){
            item = $(this);
            title = item.attr('data-title');

            value = item.val();
            if(item.is(':checked'))
            {
                var id = item.attr('data-id');
                value = $('select#'+id+'_handling_fee').val();
                fields_txt += "<tr><td style=\"font-size:12px; border: 1px solid black;\">" +title + "</td><td style=\"font-weight: bold; border: 1px solid black;\">" + value + "</td></tr>";
            }else
            if(value === 'Yes'){ fields_txt += "<tr><td style=\"font-size:12px; border: 1px solid black;\">" +title + "</td><td style=\"font-weight: bold; border: 1px solid black;\">" + value + "</td></tr>"; }
        });
        fields_txt += "</table>";
        var res_text = notes + fields_txt;

        //    ////console.log(txt);
        textarea = $('#approval_comment');
        //  ////console.log('ready');

        $.ajax({
            type: 'POST',
            url: 'index.php?page=tables&action=addNoteToApprovalComment',
            async: false,
            data: {
                textarea: textarea.val(),
                fields_txt: fields_txt,
                res_text: res_text,
                token: $("input[name='data[token]']").val()
            }
        });
        var tax_percent_info = $('#tax_percent_info');
        var tax_percent = $('#tax_percent');
        var tax_value;
        if(tax_percent.val() != 0)
        {
            tax_value = tax_percent.val();
        }else if (tax_percent_info.val() != 0)
        {
            tax_value = tax_percent_info.val();
        }else{
            tax_value = 0;
        }
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'tax_percent',
                'token': $("input[name='data[token]']").val(),
                'tax_percent': tax_value/100
            }

        });

        $.ajax({
            type: 'POST',
            url: 'index.php?page=tables&action=approve',
            async: false,
            data: {
                'url': current_url,
                'customer_email': customer_email,
                'customer_subject': customer_subject,
                'customer_id': customer_id,
                'token': $("input[name='data[token]']").val()
            }
        });

        window.location.href = 'index.php?page=domestic&token=' + $("input[name='data[token]']").val();
    });

    $('input#customer_email').change(function () {
        var value = $(this).val();

        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'customer_email',
                'token': $("input[name='data[token]']").val(),
                'customer_email': value
            },
            success: function () {

            }
        });
    });

    $('input#revision').click(function () {
        $.ajax({
            type: 'POST',
            url: 'index.php?page=tables&action=revision',
            async: false,
            data: {
                'url': current_url,
                'customer_email': customer_email,
                'customer_subject': customer_subject,
                'customer_id': customer_id,
                'token': $("input[name='data[token]']").val()
            }
        });

        window.location.href = 'index.php?page=domestic&token=' + $("input[name='data[token]']").val();
    });

    $('input#send_to_revision').click(function () {
        $.ajax({
            type: 'POST',
            url: 'index.php?page=tables&action=send_to_revision',
            async: false,
            data: {
                'url': current_url,
                'customer_email': customer_email,
                'customer_subject': customer_subject,
                'customer_id': customer_id,
                'token': $("input[name='data[token]']").val()
            }
        });

        window.location.href = 'index.php?page=domestic&token=' + $("input[name='data[token]']").val();
    });

    $('input#_check_all_support').click(function () {
        at = $(this).attr('data-check');
        //////console.log('attr: '+at);
        var items = $('input[name*="support_item_"][data-check="' + at + '"]');
        var cb = $('input[data-check="' + at + '"]#_check_all_support').is(':checked');
        $.each(items, function () {
            if (cb) {

                $(this).prop('checked', true);

            } else {


                $(this).prop('checked', false);


            }
        });

    });

    $('input#send_to_support').click(function () {
        var items = $('input[name*="support_item_"]');
        $token = $('span.custom-id').text();
        $token = $token.replace(' ', '');
        var item_array = [];
        $.each(items, function () {
            var _this = $(this);

            if (_this.is(':checked') && !_this.hasClass('disable')) {
                item_array.push(_this.val());
            }
        });
        if(item_array.length <= 0)
        {
            alert("Нужно выбрать товары для отправки в саппорт.");
            return;
        }
        ////console.log($token);
        ////console.log(item_array);
        $("#support").bPopup({
            content:'ajax',
            contentContainer: '.custom_content',
            loadUrl: 'index.php?page=domestic&action=send_to_support&token=' + $token + '&items=' + item_array
        });
    });

    $('input#send_to_approve').click(function () {

        //проверка на пустые поля
        var line = $('input[name*="_brand"][type="text"]');
        var check = 0;

        $.each(line, function () {
            var parent_element = $(this).parent().parent();
            var id = parent_element.attr('id');
            var paytype = parent_element.attr('pay-type');
            if ($('input[type="checkbox"]#' + paytype).is(":not(:checked)")) {
                return;
            }
            var description = $('textarea[name="' + id + '_description"]');
            var qty = $('input[name="' + id + '_qty"]');
            var net_price = $('input[name="' + id + '_uttc_net_price"]');
            var sell_price = $('input[name="' + id + '_uttc_sell_price"]');
            var min_sell_price = $('input[name="' + id + '_min_sell_price"]');
            var suggested_price = $('input[name="' + id + '_suggested_price"]');
            var msp_link = $('input[name="' + id + '_msp_link"]');


            //  ////console.log('value: ' + field.val());

            if (!description.val()) {
                description.parent().addClass('empty_field');
                description.addClass('empty_input');
                check++;
            } else {
                description.parent().removeClass('empty_field');
                description.removeClass('empty_input');
            }

            if (!qty.val() || qty.val() == 0) {
                qty.parent().addClass('empty_field');
                qty.addClass('empty_input');
                check++;
            } else {
                qty.parent().removeClass('empty_field');
                qty.removeClass('empty_input');
            }


            if (!net_price.val() || deformat(net_price.val()) == 0) {
                net_price.parent().addClass('empty_field');
                net_price.addClass('empty_input');
                check++;
            } else {
                net_price.parent().removeClass('empty_field');
                net_price.removeClass('empty_input');
            }

            /* if(!sell_price.val() || deformat(sell_price.val()) == 0)
             {
             sell_price.parent().addClass('empty_field').css("background-color", "");
             sell_price.addClass('empty_input');
             check++;
             }else { sell_price.parent().removeClass('empty_field').css("background-color", "#ffff00;"); sell_price.removeClass('empty_input'); }
             */
            if (!min_sell_price.val()) {
                min_sell_price.parent().addClass('empty_field');
                min_sell_price.addClass('empty_input');
                check++;
            } else {
                min_sell_price.parent().removeClass('empty_field');
                min_sell_price.removeClass('empty_input');
            }

            if (!suggested_price.val() || deformat(suggested_price.val()) == 0) {
                suggested_price.parent().addClass('empty_field').css("background-color", "");
                suggested_price.addClass('empty_input');

                check++;
            } else {
                suggested_price.parent().removeClass('empty_field').css("background-color", "#FFCC99");
                suggested_price.removeClass('empty_input');
            }

            if (!msp_link.val()) {
                msp_link.parent().addClass('empty_field');
                msp_link.addClass('empty_input');
                check++;
            } else {
                msp_link.parent().removeClass('empty_field');
                msp_link.removeClass('empty_input');
            }
        });

        var source = $('#source');
        var storeFrom = $('#store_from');
        var state = $('#state');
        var customer = $('#customer');
        var link_to_customer = $('#link_to_customer');
        var freeport = $('#freeport');
        var zip = $('#zip');
        var cs_msg = $('#cs_msg');
        var freeshipp_to_customer = $('#freeshipp_to_customer');
        var freeshipping_on_site = $('#freeshipping_on_site');
        var freeshipping_mfg = $('#freeshipping_mfg');

        /* Customer RFQ #: VALIDATION  //////////// add+*/
        var rfq = $('#rfq').val();
        var rfqObj = $('#rfq');
        if(rfq != null){
            console.log("START VALIDATE  "+ JSON.stringify(rfq));
            reg = new RegExp("^([a-zA-Z0-9-_/#&$]{3,})$");
            regValid = reg.exec(rfq);
            console.log("Regularka  VALID "+regValid);
            if( (regValid == null  && rfq != "n/a")  || rfq == "" ){
                rfqObj.parent().addClass('empty_field');
                rfqObj.addClass('empty_input');
                check++;
                console.log("NOT VALID "+rfq);
            }else{
                rfqObj.parent().removeClass("empty_field");
                //rfqObj.parent().toggleClass("empty_field");
                rfqObj.removeClass("empty_input");
                //rfqObj.toggleClass("empty_input");
                console.log("VALID "+rfq);
            }
        }else{
            console.log("CONTINUE "+ JSON.stringify(rfq));
        }
        /*END  Customer RFQ #: VALIDATION */


        if (!source.val().trim()) {
            source.parent().addClass('empty_field');
            source.addClass('empty_input');
            check++;
        } else {
            source.parent().removeClass('empty_field');
            source.removeClass('empty_input');
        }

        if (!storeFrom.val().trim()) {
            storeFrom.parent().addClass('empty_field');
            storeFrom.addClass('empty_input');
            check++;
        } else {
            storeFrom.parent().removeClass('empty_field');
            storeFrom.removeClass('empty_input');
        }

        if (!cs_msg.val().trim()) {
            cs_msg.parent().addClass('empty_field');
            check++;
        } else {
            cs_msg.parent().removeClass('empty_field');
        }

        if (state.val() == 'null') {
            state.addClass('empty_field');
            check++;
        } else {
            state.removeClass('empty_field');
        }

        /*if (!zip.val()) {
         zip.parent().addClass('empty_field');
         zip.addClass('empty_input');
         check++;
         } else {
         zip.parent().removeClass('empty_field');
         zip.removeClass('empty_input');
         }*/

        if (!customer.val().trim()) {
            customer.parent().addClass('empty_field');
            customer.addClass('empty_input');
            check++;
        } else {
            customer.parent().removeClass('empty_field');
            customer.removeClass('empty_input');
        }

        if (!link_to_customer.val().trim()) {
            link_to_customer.parent().addClass('empty_field');
            link_to_customer.addClass('empty_input');
            check++;
        } else {
            link_to_customer.parent().removeClass('empty_field');
            link_to_customer.removeClass('empty_input');
        }

        if (freeshipp_to_customer.val() == 'def') {
            freeshipp_to_customer.addClass('empty_field');
            check++;
        } else {
            freeshipp_to_customer.removeClass('empty_field');
        }

        if (freeshipping_mfg.val() == 'def') {
            freeshipping_mfg.addClass('empty_field');
            check++;
        } else {
            freeshipping_mfg.removeClass('empty_field');
        }

        if (freeshipping_on_site.val() == 'def') {
            freeshipping_on_site.addClass('empty_field');
            check++;
        } else {
            freeshipping_on_site.removeClass('empty_field');
        }

        if (freeport.val() == 'def') {
            freeport.addClass('empty_field');
            check++;
        } else {
            var shipp_to_freeport = $('#shipp_to_freeport');
            if (freeport.val() == 'Yes') {

                if ((!shipp_to_freeport.val() || shipp_to_freeport.val() == 0) && freeshipping_mfg.val() != 'Yes') {
                    shipp_to_freeport.parent().addClass('empty_field');
                    shipp_to_freeport.addClass('empty_input');
                    check++;
                } else {
                    shipp_to_freeport.parent().removeClass('empty_field');
                    shipp_to_freeport.removeClass('empty_input');
                }
            } else {
                shipp_to_freeport.parent().removeClass('empty_field');
                shipp_to_freeport.removeClass('empty_input');
            }
            freeport.removeClass('empty_field');

        }

        if(typeof error_msg !== 'undefined')
        {
            alert(error_msg);
            return;
        }
        if (check > 0) {
            $('#error').text('Остались незаполненные поля!');
            return;
        }


        if ($('input[type="checkbox"]#CC').is(":checked")) {
            var qtyFields = $('input[name*="_qty"][pay-type="CC"]');


            if(parseFloat(deformat($('#CC_total_suggested_price_final').val())) <= 0)
            {
                alert("Профит не может быть меньше нуля!");
                return;
            }

        } else if ($('input[type="checkbox"]#ACH').is(":checked")) {
            var qtyFields = $('input[name*="_qty"][pay-type="ACH"]');
            if(parseFloat(deformat($('#ACH_total_suggested_price_final').val())) <= 0)
            {
                alert("Профит не может быть меньше нуля!");
                return;
            }
        } else if ($('input[type="checkbox"]#PAYPAL').is(":checked")) {
            var qtyFields = $('input[name*="_qty"][pay-type="PAYPAL"]');
            if(parseFloat(deformat($('#PAYPAL_total_suggested_price_final').val())) <= 0)
            {
                alert("Профит не может быть меньше нуля!");
                return;
            }
        } else if ($('input[type="checkbox"]#EBAY').is(":checked")) {
            var qtyFields = $('input[name*="_qty"][pay-type="EBAY"]');
            if(parseFloat(deformat($('#EBAY_total_suggested_price_final').val())) <= 0)
            {
                alert("Профит не может быть меньше нуля!");
                return;
            }
        }
        var dixon_multiplicity = new Array;
        var multiplicity = qtyFields.length;
        ////console.log('Кол-во полей multiplicity: ' + multiplicity);
        $.each(qtyFields, function( ){
            var field = $(this);
            var brand = $(this).parent().parent().attr('brand');
            var part_number = $(this).parent().parent().attr('pn');
            var manuf_qty = field.attr('manuf-box-qty');
            var qty = parseInt(field.val());
            var manufArray = ['Milwaukee instruments'];
            if(brand == 'Dixon valve')
            {
                var buy_in_package = field.attr('buy-in-package');
                var packageArrayYes = ['Yes', 'Y'];
                var packageArrayCall = ['call'];
                ////console.log("package: " + $.inArray(buy_in_package, packageArrayYes));
                if($.inArray(buy_in_package, packageArrayYes) > -1)
                {
                    if( ((qty/manuf_qty) % 1.0) != 0) {
                        alert(brand + " " + part_number + " кратность упаковки " + manuf_qty + " шт.");
                        multiplicity++;
                        return;
                    }
                }
                if($.inArray(buy_in_package, packageArrayCall) > -1)
                {
                    dixon_multiplicity.push(part_number);


                }

            }
            if(brand == 'Milwaukee instruments')
            {
                if( ((qty/manuf_qty) % 1.0) != 0)
                {
                    alert(part_number + " кратность упаковки " + manuf_qty + " шт.");
                    multiplicity++;
                    return;
                }
            }
            if(brand == 'Cherne')
            {
                if( ((qty/manuf_qty) % 1.0) !== 0) {
                    alert(brand + " " + part_number + " кратность упаковки " + manuf_qty + " шт.");
                    multiplicity++;
                    return;
                }



            }


            if(brand == 'Aven')
            {
                if( ((qty/manuf_qty) % 1.0) !== 0) {
                    alert(brand + " " + part_number + " кратность упаковки " + manuf_qty + " шт.");
                    multiplicity++;
                    return;
                }


            }

            if(brand == 'Brady people id')
            {
                if(qty < manuf_qty)
                {
                    alert(brand + " " + part_number + " минимальный заказ " + manuf_qty + " шт.");
                    multiplicity++;
                    return;
                }
            }

            if(brand == 'Hf scientific')
            {
                var only_export = field.attr('only-export');
                if(only_export == 1)
                {
                    alert(brand + " " + part_number + " can not be sold in the USA, only export.");
                    multiplicity++;
                    return;
                }

            }

            if(brand == 'Comark')
            {
                if( ((qty/manuf_qty) % 1.0) != 0)
                {
                    alert(brand + " " + part_number + " кратность упаковки " + manuf_qty + " шт.");
                    multiplicity++;
                    return;
                }
            }

            if(brand == 'Fieldpiece')
            {
                if( ((qty/manuf_qty) % 1.0) != 0)
                {
                    alert(brand + " " + part_number + " кратность упаковки " + manuf_qty + " шт.");
                    multiplicity++;
                    return;
                }
            }


            if(brand == 'General tools')
            {
                if(qty < manuf_qty)
                {
                    alert(brand + " " + part_number + " минимальный заказ " + manuf_qty + " шт.");
                    multiplicity++;
                    return;
                }
            }



            if(qty >= manuf_qty)
            {
                multiplicity--;
            }
        });

        ////console.log('Кратность: ' + multiplicity);
        if(dixon_multiplicity.length > 0)
        {
            var dixonConfirmText = "";
            $.each(dixon_multiplicity, function(k, v){
                dixonConfirmText += v + ", ";
            });
            dixonConfirmText += " у этих партномеров скорее всего есть понятие кратности упаковки. Уточните у завода.";
            if(!confirm(dixonConfirmText))
            {
                return;
            }
            multiplicity--;

        }
        if(multiplicity > 0)
        {
            // HB1-680
            if ($.inArray(user_id, ["2","9","80","81","88"]) !== -1) {
                if (!confirm("На некоторые товары есть кратность упаковки, Вы действительно хотите отправить таблицу на апрув?")) {
                    return;
                }
            } else {
                alert('Проверьте товары на минимальный заказ и кратность упаковки.');
                return;
            }
        }

        if($.inArray(user_email, seniors) == -1 && $.inArray(user_email, CEO) == -1){
            if ($('input[type="checkbox"]#ACH').is(":checked")) {
                if(parseFloat(deformat($('#ACH_total_suggested_price').val())) <= 1000)
                {
                    alert("Нельзя выбрать оплату ACH/WT, если сумма менее $1,000.00. Обратитесь к старшему смены.");
                    return;
                }
            }
        }

        if($.inArray(user_email, seniors) == -1 && $.inArray(user_email, CEO) == -1){
            if ($('input[type="checkbox"]#CC').is(":checked")) {
                ////console.log('SUM: ' + parseFloat(deformat($('#CC_total_suggested_price').val())));
                if(parseFloat(deformat($('#CC_total_suggested_price').val())) >= 5000)
                {
                    alert("Нельзя выбрать оплату CC, если сумма более $5,000.00. Обратитесь к старшему смены.");
                    return;
                }
            }
        }

        /*//проверка на выбран ли емейл на кого отправлять таблицу.
         if ($('input[type="checkbox"]#app-email_1').is(":not(:checked)") && $('input[type="checkbox"]#app-email_2').is(":not(:checked)") && $('input[type="checkbox"]#app-email_3').is(":not(:checked)") && $('input[type="checkbox"]#app-email_4').is(":not(:checked)") && dev_email != user_email) {
         show_msg("#error", 'Кому отправляем ?', 'red');

         } else {

         var email = [];
         if ($('input[type="checkbox"]#app-email_1').is(":checked")) {
         email.push($('input[type="checkbox"]#app-email_1').val());

         }

         if ($('input[type="checkbox"]#app-email_2').is(":checked")) {
         email.push($('input[type="checkbox"]#app-email_2').val());
         }

         if ($('input[type="checkbox"]#app-email_3').is(":checked")) {
         email.push($('input[type="checkbox"]#app-email_3').val());
         }

         if ($('input[type="checkbox"]#app-email_4').is(":checked")) {
         email.push($('input[type="checkbox"]#app-email_4').val());
         }*/



        var diff = 0;

        if ($('input[type="checkbox"]#CC').is(":checked")) {
            diff = differences(diff, 'CC');
        }

        if ($('input[type="checkbox"]#ACH').is(":checked")) {
            diff = differences(diff, 'ACH');
        }

        if ($('input[type="checkbox"]#PAYPAL').is(":checked")) {
            diff = differences(diff, 'PAYPAL');
        }

        if ($('input[type="checkbox"]#EBAY').is(":checked")) {
            diff = differences(diff, 'EBAY');
        }



        if(diff > 0)
        {
            if(!confirm("Колонки Suggested Price и Final Sale Price имеют различия. Значения в Final Sale Price будут заменены на значения из Suggested Price"))
            {
                return;
            }
        }

        need_send_to_support = false;
        if (!support_comment && min_profit_count > 0) {

            $.ajax({
                type: 'POST',
                url: 'index.php?page=tables&action=getSupportComment',
                async: false,
                data: {
                    'customer_id': customer_id
                },
                success: function (response) {
                    if (response) {
                        ////console.log('Response: ' + response);
                        need_send_to_support = true;
                    }

                }

            });


            if (!need_send_to_support) {
                need_send_to_support = confirm("Действительно ли Вы хотите отправить таблицу на Approval без отправки письма в IT департмент?");
                if (!need_send_to_support) {
                    return;
                }
            }
        }

        if (on_hold == 1) {
            alert("Вы не можете отправить таблицу на Approve, так как она находится в статусе HOLD");
            return;
        }



        //TODO reset when send
        $token = $('input[name="data[token]"]').val();
        /* $.ajax({
         type: 'POST',
         async: false,
         url: 'index.php?page=domestic&action=reset_to_approve&token=' + $token,
         success: function(){
         calcs(pay_types);
         }
         });*/
        elems = $('input[name*="_final_sale_price"]');
        $.each(elems, function () {
            elem = $(this);
            id = elem.parent().parent().attr('id');
            var final_sp_flag = $('input[name="' + id + '_fsp_flag"]');
            var fsp = $('input[name="' + id + '_final_sale_price"]');

            suggested = $('input[name="' + id + '_suggested_price"]');
            final_sp_flag.val(1);
            $.ajax({
                async: false,
                type: 'POST',
                url: 'index.php?page=domestic&action=reset_flag&token=' + $token + '&flag=multiplier_flag&id=' + id
            });
            $.ajax({
                async: false,
                type: 'POST',
                url: 'index.php?page=domestic&action=flag&field=final_sale_price&id=' + id
            });

            fsp.val(priceFormat(rounding(deformat(suggested.val()))));


        });
        calcs(pay_types);

        $.ajax({
            type: 'POST',
            url: 'index.php?page=tables&action=send_to_approve',
            async: false,
            data: {
                'token': $("input[name='data[token]']").val(),
                'customer_email': customer_email,
                'customer_subject': customer_subject,
                'current_url': current_url,
                'customer_id': customer_id
            }

        });

        window.location.href = 'index.php?page=domestic&token=' + $("input[name='data[token]']").val();

    });

    $('input#table_delete').click(function () {
        if (confirm("Удалить таблицу?")) {
            $.ajax({
                type: 'POST',
                url: 'index.php?page=tables&action=hide_table',
                async: false,
                data: {
                    'token': token
                }
            });

            window.location.href = "index.php?page=tables&action=status_approved";
        }

    });

    $('input#table_close').click(function () {
        var msg = prompt("Вы собираетесь закрыть таблицу, введите причину:");
        if (msg != null && msg != '') {
            $.ajax({
                type: 'POST',
                url: 'index.php?page=tables&action=manual_close',
                async: false,
                data: {
                    'token': token,
                    'msg': msg
                }
            });
            window.location.href = "index.php?page=tables&action=status_approved";
        }
    });

    $('input#on_hold').click(function () {
        var msg = prompt("Вы собираетесь поставить таблицу в статус HOLD, введите причину:");
        if (msg != null && msg != '') {
            $.ajax({
                type: 'POST',
                url: 'index.php?page=tables&action=hold_table',
                async: false,
                data: {
                    'token': token,
                    'msg': msg
                }
            });
            location.reload();
        }
    });

    $('input#exit_hold_table').click(function () {

        $.ajax({
            type: 'POST',
            url: 'index.php?page=tables&action=exit_hold_table',
            async: false,
            data: {
                'token': token
            }
        });
        location.reload();

    });


    $('input#copy_table').click(function () {
        if (confirm("Копируем таблицу?")) {
            $.ajax({
                type: 'POST',
                url: 'index.php?page=domestic&action=copy_tbl',
                async: false,
                data: {
                    'token': token
                },
                success: function (data) {
                    var obj = document.getElementsByClassName('price_error');
                    if(data == 'ERROR'){
                        alert("Не удалось скопировать таблицу! ЦЕНА NET PRICE ИЗМЕНИЛАСЬ.");
                        obj[0].style.display = 'block';
                    }else if(data == 'WARNING'){
                        alert("Таблица скопирована, но не удалось проверить цену! ОБЯЗАТЕЛЬНО ПРОВЕРЬТЕ NET PRICE");
                        obj[0].style.display = 'block';
                    }else{
                        document.location.href = data;
                    }

                }
            });
        }
    });

//#######################################
    $("select").change(function () {
        var type;
        var parent = $(this).parent().parent();
        $id = parent.attr('id');

        $pay_type = $(this).attr('pay-type');
        calcs(pay_types);
        recalc($pay_type, $id);
    });
//#######################################
    $('input#refresh').click(function () {

        $.tax_refresh();


        calcs(pay_types);
    });
    $.tax_refresh();
    $('input#reset').click(function () {
        $token = $('input[name="data[token]"]').val();
        var multiplier = $('input[name*="_multiplier"]');
        var suggested = $('input[name*="_suggested_price"]');
        var final_price = $('input[name*="_final_sale_price"]');


        var all_final_multiplier = $('input[name="all_final_multiplier"]');
        all_final_multiplier.val(null);

        var all_cs_input = $('input[name="all_cs_input"]');
        all_cs_input.val(null);

        var all_cs_select = $('select[name="all_cs_select"]');
        all_cs_select.val(null);


        $.each(multiplier, function () {
            $(this).css('background', 'white');
        });

        $.each(suggested, function () {
            $(this).css('background', 'white');
        });

        $.each(final_price, function () {
            $(this).css('background', 'white');
        });


        var elems = $('input[name*="_multiplier_flag"]');

        $.each(elems, function () {
            $(this).val(0);
        });

        var elems = $('input[name*="_suggested_flag"]');

        $.each(elems, function () {
            $(this).val(0);
        });

        var elems = $('input[name*="_fsp_flag"]');

        $.each(elems, function () {
            $(this).val(0);
        });

        var elems = $('select[name*="_cs_select"]');
        $.each(elems, function () {
            $(this).val('null');
        });

        var elems = $('input[name*="_cs_input"]');
        $.each(elems, function () {
            $(this).val(0);
        });

        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=reset&token=' + $token
        });

        calcs(pay_types);
    });

    /**
     * оповещение если оффер >=5000.
     */
    $.over5000offer = function()
    {
        var ccOffer = $('#CC_total_suggested_price');
        var ccCheckbox = $('input[type="checkbox"]#CC');
        $.message = function()
        {

            if(table_status == 'New' || table_status == 'Revision'){
                var error = '#' + $('#error').attr('id');
                if(parseFloat(deformat(ccOffer.val())) >= 5000 && ccCheckbox.is(':checked'))
                {

                    $('#error').text('Сумма заказа: ' + ccOffer.val() + ' Оплата CC только с разрешения старшего смены.');
                    over5000 = 1;
                }else{
                    //console.log('empty');
                    $('div#error').empty();
                    over5000 = 0;
                }
            }
        }
        $.message();
        $('input, select').change(function(){
            $.message();
        });

    }
    $.under1000offer = function()
    {
        var achOffer = $('#ACH_total_suggested_price');
        var achCheckbox = $('input[type="checkbox"]#ACH');
        var ccCheckbox = $('input[type="checkbox"]#ACH');
        $.message = function()
        {
            if(table_status == 'New' || table_status == 'Revision'){
                var error = '#' + $('#error').attr('id');

                if(deformat(achOffer.val()) <= 1000 && achCheckbox.is(':checked'))
                {
                    $(error).text('Сумма заказа: ' + achOffer.val() + ' Оплата ACH/WT только с разрешения старшего смены.');
                    under1000 = 1;
                }else{
                    if(!ccCheckbox.is(':checked')){

                        $('div#error').empty();
                        under1000 = 0;
                    }
                }
            }
        }
        $.message();
        $('input, select').change(function(){
            $.message();
        });

    }
    $.isOver5000 = function()
    {
        var ccOffer = $('#CC_total_suggested_price');
        var ccCheckbox = $('input[type="checkbox"]#CC');

        if(deformat(ccOffer.val()) >= 5000 && ccCheckbox.is(':checked'))
        {
            return true;
        }else{
            return false;
        }

    }
    $.over5000offer();

    $.isUnder1000 = function()
    {
        var achOffer = $('#ACH_total_suggested_price');
        var achCheckbox = $('input[type="checkbox"]#ACH');

        if(deformat(achOffer.val()) <= 1000 && achCheckbox.is(':checked'))
        {
            return true;
        }else{
            return false;
        }

    }
    $.under1000offer();

    $('#useTaxInProfit').change(function(){
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'useTaxInProfit',
                'token': $("input[name='data[token]']").val(),
                'useTaxInProfit': $(this).val()
            }

        });
    });

    $('#setClientTax').change(function(){
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            async: false,
            data: {
                'service-action': 'setClientTax',
                'token': $("input[name='data[token]']").val(),
                'setClientTax': $(this).val()
            }

        });
        location.reload(true);
    });

    $("#isExport").click(function () {
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=update_service',
            data: {
                'service-action': 'isExport',
                'token': $("input[name='data[token]']").val(),
                'isExport': $('input[type="checkbox"]#isExport').is(':checked') ? 1 : 0
            },

            success: function (response, result) {
                if (result == 'success') {
                    location.reload();
                }

            }
        });
    });
});
//#######################################
$(document).on("dblclick", 'input[name*="_multiplier"]', function (e) {
    if (table_status != 'Waiting for Aprv' || table_status != 'Approved') {
        id = $(this).parent().parent().attr('id');
        // $(this).css('background', '#A92677');
        $(this).focus();
        $(this).select();
        var elems = $('input[name="' + id + '_multiplier_flag"]');
        $token = $('input[name="data[token]"]').val();
        $('input[name="' + id + '_final_sale_price"]').css('background', 'white');
        $('input[name="' + id + '_fsp_flag"]').val(0);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=reset_flag&token=' + $token + '&flag=final_sale_price_flag&id=' + id
        });
        $.each(elems, function () {
            $(this).val(1);
        });
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=flag&field=multiplier&id=' + id
        });
    }


});
$(document).on("dblclick", 'input[name*="_final_sale_price"]', function (e) {


    if (table_status != 'Waiting for Aprv' || table_status != 'Approved') {
        id = $(this).parent().parent().attr('id');
        //$(this).css('background', '#A92677');
        $(this).focus();
        $(this).select();
        var elems = $('input[name="' + id + '_fsp_flag"]');

        $token = $('input[name="data[token]"]').val();
        $('input[name="' + id + '_multiplier"]').css('background', 'white');
        $('input[name="' + id + '_multiplier_flag"]').val(0);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=reset_flag&token=' + $token + '&flag=multiplier_flag&id=' + id
        });

        $.each(elems, function () {
            $(this).val(1);
        });
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=flag&field=final_sale_price&id=' + id
        });

    }


});
$(document).on("dblclick", 'input[name*="_suggested_price"]', function (e) {

    if (table_status != 'Waiting for Aprv' || table_status != 'Approved') {
        id = $(this).parent().parent().attr('id');
        // $(this).css('background', '#A92677');
        $(this).focus();
        $(this).select();
        var elems = $('input[name="' + id + '_suggested_flag"]');
        $.each(elems, function () {
            $(this).val(1);
        });
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=flag&field=suggested&id=' + id
        });
    }
});
$(document).on("click", "button#to_all_cs_multiplier", function () {
    var main_fm, final_m, final_sp, final_m_flag, final_sp_flag, token;
    token = $('input[name="data[token]"]').val();
    var val = $('select[name="all_cs_select"]').val();
    elems = $('select[name*="_cs_select"]');
    $.each(elems, function () {
        $(this).val(val);
    });

    var val = $('input[name="all_cs_input"]').val();
    elems = $('input[name*="_cs_input"]');
    $.each(elems, function () {
        $(this).val(val);
    });

    calcs(pay_types);

    $elems = $('tr[pay-type]');


});

$(document).on("dblclick", 'textarea[name*="_partnum"]', function (e) {
    window.open(
        $(this).attr('data-uri'),
        '_blank' // <- This is what makes it open in a new window.
    );
});


$(document).on("click", "button#to_all_final_multiplier", function () {

    var main_fm, final_m, final_sp, final_m_flag, final_sp_flag, token;
    main_fm = $('input[name="all_final_multiplier"]').val();
    token = $('input[name="data[token]"]').val();

    fsp_editable = 1;
    $.ajax({
        type: 'POST',
        url: 'index.php?page=tables&action=edit_table_check',
        data: {
            'token': $("input[name='data[token]']").val()
        }
    });

    var row = $('tr[pay-type]');
    $.each(row, function () {
        var element = $(this);
        var id = element.attr('id');
        var pay_type = element.attr('pay-type');
        final_m = $('input[name="' + id + '_multiplier"]');
        final_sp = $('input[name="' + id + '_final_sale_price"]');
        // final_m.css('background', '#A92677');
        final_m_flag = $('input[name="' + id + '_multiplier_flag"]');
        //alert(id + ' DO '+final_m_flag.val());
        final_sp_flag = $('input[name="' + id + '_fsp_flag"]');

        final_m_flag.val(1);
        //alert(id + ' POSLE '+final_m_flag.val());
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=flag&field=multiplier&id=' + id
        });
        final_sp_flag.val(0);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=reset_flag&token=' + token + '&flag=final_sale_price_flag&id=' + id
        });
        final_m.val(main_fm);


        calcs(pay_types);
        recalc(pay_type, id);
    });


    /* $elems = $('tr[pay-type]');
     $.each($elems, function(){
     $elem = $(this);
     $id = $elem.attr('id');
     $pay_type = $elem.attr('pay-type');
     calcs($pay_type);
     recalc($pay_type, $id);


     });*/

});

//Копирование из Final Sale Price -> Suggested price
$(document).on("click", "button#copy_to_suggested", function () {
    elems = $('input[name*="_final_sale_price"]');
    $.each(elems, function () {
        elem = $(this);
        //elem.css('background', '#A92677');
        id = elem.parent().parent().attr('id');
        val = deformat(elem.val());
        var cs_multiplier_inp = $('input[name="' + id + '_cs_input"]');
        var cs_multiplier_sel = $('select[name="' + id + '_cs_select"]');
        var flag_fsp = $('input[name="' + id + '_fsp_flag"]');
        $token = $('input[name="data[token]"]').val();
        var final_multiplier = $('input[name="' + id + '_multiplier"]');
        //final_multiplier.css('background', 'white');
        //$('input[name="' + id + '_multiplier_flag"]').val(0);
        /*$.ajax({
         type: 'POST',
         url: 'index.php?page=domestic&action=reset_flag&token=' + $token + '&flag=multiplier_flag&id='+id
         });*/

        $.each(flag_fsp, function () {
            $(this).val(1);
        });
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=flag&field=final_sale_price&id=' + id
        });
        var fsp = $('input[name="' + id + '_final_sale_price"]');


        cs_multiplier_sel.val('null');

        cs_multiplier_inp.val(final_multiplier.val());
        target = $('input[name="' + id + '_suggested_price"]');
        target.val(val);

        // target.css('background', '#A92677');

        var target_flag = $('input[name="' + id + '_suggested_flag"]');
        target_flag.val(1);
        $.ajax({
            type: 'POST',
            url: 'index.php?page=domestic&action=flag&field=suggested&id=' + id
        });
    });
    calcs(pay_types);
});

//#######################################
//Считаем shipping
function sh(net_price) {

    if (isNaN(net_price)) {
        net_price = deformat(net_price);
    }


    $.tax();

    // alert(net_price);
    var shipp_to_freeport = $('#shipp_to_freeport');
    var shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
    var manuf_freeshipp = $('#freeshipping_mfg');
    var freeport = $('#freeport');
    var freeshipp_to_customer = $('#freeshipp_to_customer');
    var tax = $('input#tax_sum');
    var sh_to_customer, sh, value;

    var $tax = $('#tax_sum');
    var sumTax = ($('#useTaxInProfit').val() == '1') ? tax.val() : 0 ;
    //страховка
    if (parseFloat(net_price) < 1000) {
        value = 0;
    }
    else {
        value = parseFloat(net_price) * parseFloat(0.01);
    }

// если есть freeshipping от завода
    if (manuf_freeshipp.val() == 'Yes') {
        sh_to_customer = 0;
    }
    else {
        sh_to_customer = parseFloat(shipp_to_freeport_customer.val());
    }


//
    if (freeshipp_to_customer.val() == 'Yes') {

        if (freeport.val() == 'Yes') {
            if (manuf_freeshipp.val() == 'Yes') {
                value = value / 2;
            }
            sh = parseFloat(shipp_to_freeport.val()) + sh_to_customer + value * 2;
        }
        else {
            if (manuf_freeshipp.val() == 'Yes') {
                value = 0;
            }
            /*sh = parseFloat(sh_to_customer) + value + parseFloat(tax.val());*/

            // alert('tax: ' + parseFloat($tax.val()));

            sh = parseFloat(sh_to_customer) + parseFloat(value) + parseFloat(sumTax);
            // alert('sh: ' + sh);
        }

    }
    else {

        if (freeport.val() == 'Yes') {
            if (manuf_freeshipp.val() == 'Yes') {
                value = value / 2;
            }
            sh = parseFloat(shipp_to_freeport.val()) + value * 2;
        }
        else {
            if (manuf_freeshipp.val() == 'Yes') {
                value = 0;
            }
            // sh = value + parseFloat(tax.val());

            sh = value + parseFloat(sumTax);

        }
    }

    //alert("val: " + value);
    //alert("sh111: " + sh);
    //alert("sh_to_cust: " + sh_to_customer);

    return parseFloat(sh);
}

function total(pay_method) {
    var total;

    function total_uttc_net_price(pay_method) {

        var elems = $('input[pay-type="' + pay_method + '"][name*="_uttc_net_price"]');
        var total = 0;
        $.each(elems, function () {
            val = deformat($(this).val());
            par = $(this).parent().parent();
            id = par.attr('id');
            if (val === null || val == '') {
                val = 0;
            }
            total += parseFloat(val) * $('input[pay-type="' + pay_method + '"][name="' + id + '_qty"]').val();

        });
        $("#" + pay_method + "_total_uttc_net_price").val(priceFormat(total)).css('text-align', 'right');
    }

    function must_be(pay_method) {

        var elems = $('input[pay-type="' + pay_method + '"][name*="_min_sell_price"]');
        var total = 0;

        $.each(elems, function () {
            val = deformat($(this).val());
            par = $(this).parent().parent();
            shipping = $(this).attr('data-shipping');
            id = par.attr('id');
            var free = $('input[name="' + id + '_sell_price_freeshipping"]');
            var lower_sale_price = $('input[name="' + id + '_lower_sale_price"]');
            if (val === null || val == '') {
                val = 0;
            }
            if (isNaN(val)) {
                $('input[pay-type="' + pay_method + '"][name="' + id + '_must_be"]').val('N/A');
            } else {
                if(val != lower_sale_price.val() && free.val() == 1)
                {
                    //console.log("Конкурент: " + val);
                    //console.log("Доставка: " + shipping);

                    $('input[pay-type="' + pay_method + '"][name="' + id + '_must_be"]').val(priceFormat(must_be_discount(val - shipping)));
                }
                else
                {
                    //console.log("Конкурент: " + val);

                    $('input[pay-type="' + pay_method + '"][name="' + id + '_must_be"]').val(priceFormat(must_be_discount(val)));
                }
                //////console.log($('input[pay-type="' + pay_method + '"][name="' + id + '_must_be"]').val());
                /*if (free.val() == 1) {
                 if (lower_sale_price.val() == null || lower_sale_price.val() == 0) {

                 $('input[pay-type="' + pay_method + '"][name="' + id + '_must_be"]').val(priceFormat(must_be_discount(val)));
                 } else {
                 $('input[pay-type="' + pay_method + '"][name="' + id + '_must_be"]').val(priceFormat(must_be_discount(val)));
                 }

                 } else {
                 $('input[pay-type="' + pay_method + '"][name="' + id + '_must_be"]').val(priceFormat(must_be_discount(val)));
                 }*/

            }

        });

    }

    //Пересчет поля Suggested Price
    function suggested_price_f(pay_method) {
        var elems = $('input[pay-type="' + pay_method + '"][name*="_suggested_price"]');
        var total = 0;

        $.each(elems, function () {

            par = $(this).parent().parent();
            id = par.attr('id');
            cs_select = $('select[name*="' + id + '_cs_select"]');
            val = deformat($('input[pay-type="' + pay_method + '"][name="' + id + '_uttc_sell_price"]').val());
            var suggested_flag = $("input[pay-type='" + pay_method + "'][name='" + id + "_suggested_flag']").val();

//#############################################

            /*  if(pay_method == 'CC')
             {
             final_sale_price_res = (uttc_net_price.val() * multiplier.val()) / 0.965;
             }

             if(pay_method == 'ACH')
             {
             final_sale_price_res = (uttc_net_price.val() * multiplier.val());
             }
             if(pay_method == 'PAYPAL')
             {
             final_sale_price_res = (uttc_net_price.val() * multiplier.val()) / (1-0.029)+0.31;
             }
             if(pay_method == 'EBAY')
             {
             final_sale_price_res = (parseFloat(uttc_net_price.val()) + parseFloat(shipp_to_freeport.val()) + parseFloat(shipp_to_freeport_customer.val())) * multiplier.val();
             }*/
//##########################################

            if (cs_select.val() != 'null') {
                cs_input = $("input[pay-type='" + pay_method + "'][name*='" + id + "_cs_input']");

                var shipp_to_freeport = $('#shipp_to_freeport');
                var shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
                var cs_manual_val;
                if (cs_select.val() == 'np') {

                    if (cs_input.val() > 0) {
                        var net_price = $('input[pay-type="' + pay_method + '"][name*="' + id + '_uttc_net_price"]');

                        if (pay_method == 'CC') {
                            //TODO
                            if (suggested_flag == 1) {
                                //Если была нажата кнопка "Copy Final Sale Price to Suggested"
                                cs_manual_val = deformat(net_price.val()) * cs_input.val() / 0.97;

                            }
                            else {
                                cs_manual_val = (deformat(net_price.val()) * cs_input.val());
                            }

                        }

                        if (pay_method == 'ACH') {

                            if (flag_CC == 1) {

                                var sugg_price = $('input[pay-type="' + pay_method + '"][name="' + id + '_suggested_price"]');
                                var $pn = sugg_price.attr('pn');
                                var suggested_price_CC = $('input[pay-type="CC"][pn="' + $pn + '"][name*="_suggested_price"]');
                                console.log('============================================NP');
                                console.log(suggested_price_CC.val());
                                cs_manual_val = (deformat(suggested_price_CC.val()) * 0.99);
                                console.log(cs_manual_val);


                            }
                            else {

                                cs_manual_val = (deformat(net_price.val()) * cs_input.val());
                                console.log("SP : " + deformat(net_price.val()));
                                console.log("CS_INP : " + cs_input.val());
                                console.log("CS_SUM : " + cs_manual_val);


                            }
                        }

                        if (pay_method == 'PAYPAL') {
                            /* cs_manual_val = (net_price.val() * cs_input.val()) / (1-0.029)+0.31;*/


                            if (suggested_flag == 1) {
                                //Если была нажата кнопка "Copy Final Sale Price to Suggested"
                                cs_manual_val = (deformat(net_price.val()) * cs_input.val()) / (1 - 0.029) + 0.31;
                            }
                            else {
                                cs_manual_val = (deformat(net_price.val()) * cs_input.val());
                            }
                        }

                        if (pay_method == 'EBAY') {
                            if (suggested_flag == 1) {
                                //Если была нажата кнопка "Copy Final Sale Price to Suggested"
                                cs_manual_val = (deformat(net_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val())) * cs_input.val();
                            }
                            else {
                                cs_manual_val = (deformat(net_price.val()) * cs_input.val());
                            }
                        }

                    }
                }
                if (cs_select.val() == 'sp') {
                    if (cs_input.val() > 0) {
                        var sell_price = $('input[pay-type="' + pay_method + '"][name*="' + id + '_uttc_sell_price"]');
                        if (pay_method == 'CC') {
                            /*cs_manual_val = (sell_price.val() * cs_input.val()) / 0.965;*/
                            cs_manual_val = (deformat(sell_price.val()) * cs_input.val());
                        }

                        if (pay_method == 'ACH') {
                            if (flag_CC == 1) {

                                var sugg_price = $('input[pay-type="' + pay_method + '"][name*="' + id + '_suggested_price"]');
                                var $pn = sugg_price.attr('pn');
                                var suggested_price_CC = $('input[pay-type="CC"][pn="' + $pn + '"][name*="_suggested_price"]');

                                console.log('++++++++++++++++++++++++SP');
                                console.log(suggested_price_CC.val());
                                cs_manual_val = deformat(suggested_price_CC.val()) * 0.99;
                                console.log(cs_manual_val);

                            }
                            else {
                                /*cs_manual_val = (sell_price.val() * cs_input.val());*/
                                cs_manual_val = (deformat(sell_price.val()) * cs_input.val());
                            }

                        }

                        if (pay_method == 'PAYPAL') {
                            /*cs_manual_val = (sell_price.val() * cs_input.val()) / (1-0.029)+0.31;*/
                            cs_manual_val = (deformat(sell_price.val()) * cs_input.val());
                        }

                        if (pay_method == 'EBAY') {
                            /*cs_manual_val = (parseFloat(sell_price.val()) + parseFloat(shipp_to_freeport.val()) + parseFloat(shipp_to_freeport_customer.val())) * cs_input.val();*/
                            cs_manual_val = (deformat(sell_price.val()) * cs_input.val());
                        }
                    }
                }

                if (cs_select.val() == 'null') {
                    if (pay_method == 'CC') {
                        cs_manual_val = (deformat(uttc_net_price.val()) * multiplier.val()) / 0.97;
                    }

                    if (pay_method == 'ACH') {
                        cs_manual_val = (deformat(uttc_net_price.val()) * multiplier.val());
                    }
                    if (pay_method == 'PAYPAL') {
                        cs_manual_val = (deformat(uttc_net_price.val()) * multiplier.val()) / 1.339;
                    }
                    if (pay_method == 'EBAY') {
                        cs_manual_val = (deformat(uttc_net_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val())) * multiplier.val();
                    }
                }

                $('input[pay-type="' + pay_method + '"][name="' + id + '_suggested_price"]').val(priceFormat(rounding(cs_manual_val)));
            } else if (suggested_flag == 0) {

                if (pay_method == 'ACH') {

                    if (flag_CC == 1) {

                        var sugg_price = $('input[pay-type="' + pay_method + '"][name="' + id + '_suggested_price"]');
                        var $pn = sugg_price.attr('pn');
                        var suggested_price_CC = $('input[pay-type="CC"][pn="' + $pn + '"][name*="_suggested_price"]');
                        val = (deformat(suggested_price_CC.val()) * 0.99);
                    }
                }
                $('input[pay-type="' + pay_method + '"][name="' + id + '_suggested_price"]').val(priceFormat(rounding(val)));
            }


        });

    }

    function uttc_sell_price(pay_method) {

        var elems = $('input[pay-type="' + pay_method + '"][name*="_uttc_sell_price"]');
        var total = 0;
        $.each(elems, function () {
            val = deformat($(this).val());
            par = $(this).parent().parent();
            id = par.attr('id');
            if (val === null || val == '') {
                val = 0;
            }
            total += parseFloat(val) * $('input[pay-type="' + pay_method + '"][name="' + id + '_qty"]').val();
            // ////console.log('TOTAL: ' + total);
        });
        $("#" + pay_method + "_total_uttc_sell_price").val(priceFormat(rounding(total))).css('text-align', 'right');
    }

    function must_be_service(pay_method) {

        var tax_sum = $('input#tax_sum').val();
        var uttc_sell_price_total = $('input#tax_sum').val();


    }

    function suggested_price(pay_method) {

        var elems = $('input[pay-type="' + pay_method + '"][name*="_suggested_price"]');
        var total = 0;
        $.each(elems, function () {
            val = deformat($(this).val());
            par = $(this).parent().parent();
            id = par.attr('id');
            if (val === null || val == '') {
                val = 0;
            }
            total += parseFloat(val) * deformat($('input[pay-type="' + pay_method + '"][name="' + id + '_qty"]').val());

        });
        $("#" + pay_method + "_total_suggested_price").val(priceFormat(rounding(total))).css('text-align', 'right');


    }

    function app_suggested_price(pay_method) {

        var CC_total_suggested_price = $('#CC_total_suggested_price');
        var CC_approximate_profit = $('#CC_approximate_profit');
        var CC_total_uttc_net_price = $('#CC_total_uttc_net_price');
        var CC_total_suggested_price_final = $('#CC_total_suggested_price_final');

        var ACH_total_suggested_price = $('#ACH_total_suggested_price');
        var ACH_approximate_profit = $('#ACH_approximate_profit');
        var ACH_total_uttc_net_price = $('#ACH_total_uttc_net_price');
        var ACH_total_suggested_price_final = $('#ACH_total_suggested_price_final');


        var PAYPAL_total_suggested_price = $('#PAYPAL_total_suggested_price');
        var PAYPAL_approximate_profit = $('#PAYPAL_approximate_profit');
        var PAYPAL_total_uttc_net_price = $('#PAYPAL_total_uttc_net_price');
        var PAYPAL_total_suggested_price_final = $('#PAYPAL_total_suggested_price_final');

        var EBAY_total_suggested_price = $('#EBAY_total_suggested_price');
        var EBAY_approximate_profit = $('#EBAY_approximate_profit');
        var EBAY_total_uttc_net_price = $('#EBAY_total_uttc_net_price');
        var EBAY_total_suggested_price_final = $('#EBAY_total_suggested_price_final');

        var CHECK_total_suggested_price = $('#CHECK_total_suggested_price');
        var CHECK_approximate_profit = $('#CHECK_approximate_profit');
        var CHECK_total_uttc_net_price = $('#CHECK_total_uttc_net_price');
        var CHECK_total_suggested_price_final = $('#CHECK_total_suggested_price_final');

        var shipp_to_freeport = $('#shipp_to_freeport');
        var shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
        var freeport = $('#freeport');
        var value, value1, value2, value3;
        var sum;




///CC

        sum = deformat(CC_total_suggested_price.val()) - (deformat(CC_total_suggested_price.val()) * parseFloat(0.03)) - deformat(CC_total_uttc_net_price.val()) - sh(deformat(CC_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;
        console.log('CC_total_suggested_price.val() = ' + CC_total_suggested_price.val());
        console.log('CC_total_suggested_price.val() = ' + CC_total_suggested_price.val());
        console.log('CC_total_uttc_net_price.val() = ' + CC_total_uttc_net_price.val());
        console.log('sh(deformat(CC_total_uttc_net_price.val())) = ' + sh(deformat(CC_total_uttc_net_price.val())));
        console.log('handling = ' + handling);
        console.log('minOrderFee = ' + minOrderFee);
        console.log('handlingTariff = ' + handlingTariff);
        console.log('dropShipmentTariff = ' + dropShipmentTariff);
        console.log('sum = ' + sum);
        CC_total_suggested_price_final.val(priceFormat(sum));


///ACH

        sum = deformat(ACH_total_suggested_price.val()) - deformat(ACH_total_uttc_net_price.val()) - sh(deformat(ACH_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;

        console.log(ACH_total_suggested_price.val()); //16,289.21
        console.log(ACH_total_uttc_net_price.val()); // 13,910.40
        console.log(sh(deformat(ACH_total_uttc_net_price.val()))); 253.5632
        console.log(handling); // 0
        console.log(minOrderFee); //0
        console.log(handlingTariff); //0
        console.log(dropShipmentTariff); //0

        ACH_total_suggested_price_final.val(priceFormat(sum));


///PAYPAL
        sum = deformat(PAYPAL_total_suggested_price.val()) - deformat(PAYPAL_total_uttc_net_price.val()) - sh(deformat(PAYPAL_total_uttc_net_price.val())) - deformat(PAYPAL_total_suggested_price.val()) * parseFloat(0.029) - parseFloat(0.3) + handling - minOrderFee - handlingTariff - dropShipmentTariff;

        PAYPAL_total_suggested_price_final.val(priceFormat(sum));


///CHECK
        sum = deformat(CHECK_total_suggested_price.val()) - deformat(CHECK_total_uttc_net_price.val()) - sh(deformat(CHECK_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;

        CHECK_total_suggested_price_final.val(priceFormat(sum));

///EBAY


        sum = 0;
        value = 0;
        value2 = 0;
//TODO
        if (deformat(EBAY_total_suggested_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val()) < parseFloat(50.01)) {
            value = (deformat(EBAY_total_suggested_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val())) * parseFloat(0.11);

        }
        else {
            if (deformat(EBAY_total_suggested_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val()) < parseFloat(1000.01)) {
                value = (deformat(EBAY_total_suggested_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val()) - parseFloat(50)) * parseFloat(0.06) + parseFloat(5.5);

            } else {

                value = (deformat(EBAY_total_suggested_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val()) - parseFloat(1000)) * parseFloat(0.02) + parseFloat(62.5);

            }
        }

        //sum = deformat(EBAY_total_suggested_price.val()) - deformat(EBAY_total_uttc_net_price.val()) - (deformat(EBAY_total_suggested_price.val()) + sh(deformat(EBAY_total_uttc_net_price.val()))) * parseFloat(0.029) - parseFloat(0.3) - /*parseFloat(value)*/ - sh(deformat(EBAY_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;
        sum = deformat(EBAY_total_suggested_price.val()) - deformat(EBAY_total_uttc_net_price.val()) - (deformat(EBAY_total_suggested_price.val()) + sh(deformat(EBAY_total_uttc_net_price.val()))) * parseFloat(0.029) - parseFloat(0.3) - sh(deformat(EBAY_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;

        EBAY_total_suggested_price_final.val(priceFormat(sum));
    }

    function total_aproximate_profit() {


        var shipp_to_freeport = $('#shipp_to_freeport');
        var shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
        var freeport = $('#freeport');
        var freeshipp_to_customer = $('#freeshipp_to_customer');
        var tax_sum = $('input#tax_sum').val();
        var value, value2;
        var approximate_profit;
        var freep_to_custom;

        var CC_approximate_profit = $('#CC_approximate_profit');
        var CC_total_uttc_sell_price = $('#CC_total_uttc_sell_price');
        var CC_total_uttc_net_price = $('#CC_total_uttc_net_price');

        var ACH_approximate_profit = $('#ACH_approximate_profit');
        var ACH_total_uttc_sell_price = $('#ACH_total_uttc_sell_price');
        var ACH_total_uttc_net_price = $('#ACH_total_uttc_net_price');

        var PAYPAL_approximate_profit = $('#PAYPAL_approximate_profit');
        var PAYPAL_total_uttc_sell_price = $('#PAYPAL_total_uttc_sell_price');
        var PAYPAL_total_uttc_net_price = $('#PAYPAL_total_uttc_net_price');

        var EBAY_approximate_profit = $('#EBAY_approximate_profit');
        var EBAY_total_uttc_sell_price = $('#EBAY_total_uttc_sell_price');
        var EBAY_total_uttc_net_price = $('#EBAY_total_uttc_net_price');

        var CHECK_approximate_profit = $('#CHECK_approximate_profit');
        var CHECK_total_uttc_sell_price = $('#CHECK_total_uttc_sell_price');
        var CHECK_total_uttc_net_price = $('#CHECK_total_uttc_net_price');

        if (freeshipp_to_customer.val() == 'Yes') {
            freep_to_custom = deformat(shipp_to_freeport_customer.val());

        }
        else {
            freep_to_custom = 0;
        }
        //TODO
        function CC_app_profit() {
            //cc
            approximate_profit = 0;
            // ////console.log("CC_total_uttc_sell_price: " + deformat(CC_total_uttc_sell_price.val()));
            // ////console.log('CC_total_uttc_sell_price: ' + deformat(CC_total_uttc_sell_price.val()));
            // ////console.log('CC_total_uttc_net_price: ' + deformat(CC_total_uttc_net_price.val()));
            // ////console.log('sh: ' + sh(deformat(CC_total_uttc_net_price.val())));
            approximate_profit = deformat(CC_total_uttc_sell_price.val()) - (deformat(CC_total_uttc_sell_price.val()) * parseFloat(0.03)) - deformat(CC_total_uttc_net_price.val()) - sh(deformat(CC_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff ;

            CC_approximate_profit.val(priceFormat(approximate_profit));
        }

        //E - ACH_total_uttc_sell_price
        //D - ACH_total_uttc_net_price
        //K - shipp_to_freeport
        //L - shipp_to_freeport_customer

        function ACH_app_profit() {
            value = 0;
            approximate_profit = 0;
            approximate_profit = deformat(ACH_total_uttc_sell_price.val()) - deformat(ACH_total_uttc_net_price.val()) - sh(deformat(ACH_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff ;


            ACH_approximate_profit.val(priceFormat(approximate_profit));

        }

        function CHECK_app_profit() {
            value = 0;
            approximate_profit = 0;
            approximate_profit = deformat(CHECK_total_uttc_sell_price.val()) - deformat(CHECK_total_uttc_net_price.val()) - sh(deformat(CHECK_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff ;


            CHECK_approximate_profit.val(priceFormat(approximate_profit));

        }

        //E - ACH_total_uttc_sell_price
        //D - ACH_total_uttc_net_price
        //K - shipp_to_freeport
        //L - shipp_to_freeport_customer
        function PAYPAL_app_profit() {
            value = 0;
            approximate_profit = 0;


            approximate_profit = deformat(PAYPAL_total_uttc_sell_price.val()) - deformat(PAYPAL_total_uttc_net_price.val()) - sh(deformat(PAYPAL_total_uttc_net_price.val())) - deformat(PAYPAL_total_uttc_sell_price.val()) * parseFloat(0.029) - parseFloat(0.3) + handling - minOrderFee - handlingTariff - dropShipmentTariff ;

            PAYPAL_approximate_profit.val(priceFormat(approximate_profit));
        }


        function EBAY_app_profit() {
            value = 0;
            value2 = 0;
            approximate_profit = 0;

            if (deformat(EBAY_total_uttc_sell_price.val()) + deformat(shipp_to_freeport.val()) + parseFloat(freep_to_custom) < parseFloat(50.01)) {
                value = (deformat(EBAY_total_uttc_sell_price.val()) + deformat(shipp_to_freeport.val()) + parseFloat(freep_to_custom)) * parseFloat(0.11);
            }
            else {
                if (deformat(EBAY_total_uttc_sell_price.val()) + deformat(shipp_to_freeport.val()) + parseFloat(freep_to_custom) < parseFloat(1000.01)) {
                    value = (deformat(EBAY_total_uttc_sell_price.val()) + deformat(shipp_to_freeport.val()) + parseFloat(freep_to_custom) - parseFloat(50)) * parseFloat(0.06) + parseFloat(50) * parseFloat(0.11);
                }
                else {
                    value = (deformat(EBAY_total_uttc_sell_price.val()) + deformat(shipp_to_freeport.val()) + parseFloat(freep_to_custom) - parseFloat(1000)) * parseFloat(0.02) + parseFloat(950) * parseFloat(0.06) + parseFloat(50) * parseFloat(0.11);
                }
            }


            approximate_profit = deformat(EBAY_total_uttc_sell_price.val()) - deformat(EBAY_total_uttc_net_price.val()) - (deformat(EBAY_total_uttc_sell_price.val()) + sh(deformat(EBAY_total_uttc_net_price.val()))) * parseFloat(0.029) - parseFloat(0.3) - sh(deformat(EBAY_total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;


            EBAY_approximate_profit.val(priceFormat(approximate_profit));
        }


        CC_app_profit();
        ACH_app_profit();
        PAYPAL_app_profit();
        EBAY_app_profit();
        CHECK_app_profit();
    }

    function getTotalNetCostByOrder()
    {
        var CC_total_uttc_net_price = $('#CC_total_uttc_net_price').val();
        var ACH_total_uttc_net_price = $('#ACH_total_uttc_net_price').val();
        var PAYPAL_total_uttc_net_price = $('#PAYPAL_total_uttc_net_price').val();
        var EBAY_total_uttc_net_price = $('#EBAY_total_uttc_net_price').val();

        var result = 0;
        if (deformat(EBAY_total_uttc_net_price) > 0) {
            result = deformat(EBAY_total_uttc_net_price);
        } else if (deformat(ACH_total_uttc_net_price) > 0) {
            result = deformat(ACH_total_uttc_net_price);
        } else if (deformat(PAYPAL_total_uttc_net_price) > 0) {
            result = deformat(PAYPAL_total_uttc_net_price);
        } else {
            result = deformat(CC_total_uttc_net_price);
        }

        return result;
    }

    function approx_profit_calc(pay_method) {
        tr = $('tr[pay-type="' + pay_method + '"]');
        $.each(tr, function () {
            var id = $(this).attr('id');
            var brandName = $("input.brandtag[name='" + id + "_brand']").val().toLowerCase();
            brandName = brandName.replace(/\s|\.|\,|\(|\)|&|\"|\'|\`/g,'-');

            f(pay_method, id, brandName);
        });

        //var brandName = $("input.brandtag[name='" + id + "_brand']").val().toLowerCase().replace(/\s/g,'-');



        function f(pay_method, id, brandName) {

            var CC_total_uttc_net_price = $('#CC_total_uttc_net_price').val();
            var ACH_total_uttc_net_price = $('#ACH_total_uttc_net_price').val();
            var PAYPAL_total_uttc_net_price = $('#PAYPAL_total_uttc_net_price').val();
            var EBAY_total_uttc_net_price = $('#EBAY_total_uttc_net_price').val();

            var dropShipmentTariffDisplay = $("input[id='" + brandName + "_2dropShipmentTariffDisplay']").val();
            var handlingTariffDisplay = $("input[id='" + brandName + "_2handlingTariffDisplay']").val();
            var minOrderFeeDisplay = $('input[id='+brandName+'_2minOrderFeeDisplay]').val();

            var approx_profit = $("input[pay-type='" + pay_method + "'][name='" + id + "_approx_profit']");
            var suggested_price = $("input[pay-type='" + pay_method + "'][name='" + id + "_suggested_price']");
            var uttc_net_price = $("input[pay-type='" + pay_method + "'][name='" + id + "_uttc_net_price']");
            var qty = $("input[pay-type='" + pay_method + "'][name='" + id + "_qty']");
            var elems = $('input[pay-type="' + pay_method + '"][name*="_approx_profit"]');
            var sum;
            //var oldOtherFees = (parseFloat(dropShipmentTariff) + parseFloat(handlingTariff) + parseFloat(minOrderFee));
            var suggested_price_val = (deformat(suggested_price.val()) * qty.val());
            var suggested_price_val_without_qty = (deformat(suggested_price.val()));
            //var suggested_price_val = (deformat(suggested_price.val()));
            var uttc_net_price_val_without_qty = (deformat(uttc_net_price.val()));
           // var uttc_net_price_val = (deformat(uttc_net_price.val()) * qty.val());
           // var otherFees = oldOtherFees;

            var uttc_net_price_val = window.uttc_net_price_by_brand[pay_method][brandName];
            var uttc_net_price_val2 = deformat(uttc_net_price.val()) * qty.val();
            var suggested_price_val_total = window.suggested_price_by_brand[pay_method][brandName];
            console.log(brandName);
            console.log('brand name');


            if (dropShipmentTariffDisplay == undefined) {
                 dropShipmentTariffDisplay = '0';
            }
            else {
                dropShipmentTariffDisplay = dropShipmentTariffDisplay.replace("$", "");
            }

            if (handlingTariffDisplay == undefined) {
                 handlingTariffDisplay = '0';
            }
            else {
                handlingTariffDisplay = handlingTariffDisplay.replace("$", "");
            }

            if (minOrderFeeDisplay == undefined) {
                minOrderFeeDisplay = '0';
            }

            var oldOtherFees = (parseFloat(dropShipmentTariffDisplay) + parseFloat(handlingTariffDisplay) + parseFloat(minOrderFeeDisplay));


            var freep_to_custom = ($('#freeshipp_to_customer').val() == 'Yes') ? deformat($('#shipp_to_freeport_customer').val()) : 0;
            var tax_profit = ($('#useTaxInProfit').val() == '1') ? deformat($('#tax_sum').val()) : 0;


            var shippToFreeportCustomer = (uttc_net_price_val2 * freep_to_custom / getTotalNetCostByOrder());
            var sumTaxProfit = (uttc_net_price_val2 * tax_profit / getTotalNetCostByOrder());

            var otherFees = 0;
            if ((dropShipmentTariffDisplay != undefined && dropShipmentTariffDisplay.indexOf('%') > -1)) {
                otherFees += ((uttc_net_price_val * parseFloat(dropShipmentTariffDisplay) / 100) * uttc_net_price_val_without_qty / uttc_net_price_val) * qty.val() /*shippToFreeportCustomer +*/ /*sumTaxProfit*/;
            } else {
                otherFees += (parseFloat(dropShipmentTariffDisplay) * uttc_net_price_val_without_qty / uttc_net_price_val) * qty.val() /*shippToFreeportCustomer +*/ /*sumTaxProfit*/;
            }

            if ((handlingTariffDisplay != undefined && handlingTariffDisplay.indexOf('%') > -1)) {
                otherFees += ((uttc_net_price_val * parseFloat(handlingTariffDisplay) / 100) * uttc_net_price_val_without_qty / uttc_net_price_val) * qty.val()
            } else {
                otherFees += (parseFloat(handlingTariffDisplay) * uttc_net_price_val_without_qty / uttc_net_price_val) * qty.val()
            }

            if ((minOrderFeeDisplay != undefined && minOrderFeeDisplay.indexOf('%') > -1)) {
                otherFees += ((uttc_net_price_val * parseFloat(minOrderFeeDisplay) / 100) * uttc_net_price_val_without_qty / uttc_net_price_val) * qty.val()
            } else {
                otherFees += (parseFloat(minOrderFeeDisplay) * uttc_net_price_val_without_qty / uttc_net_price_val) * qty.val()
            }


            var handlingCorrect = $("[data-brandnamealghandling="+brandName+"] option").val();
            var isHandling = $("[data-brandnamealg="+brandName+"] input").val();
            var isChecked = ($("[data-brandnamealghandling="+brandName+"]").attr("disabled") == "disabled") ? 0 : 1;

            if (isHandling == undefined) {
                isHandling = 'off';
            }
            if (handlingCorrect == undefined || isHandling == 'off') {
                handlingCorrect = '0';
            }

            handlingCorrect = handlingCorrect * isChecked;
            newHandling =  (parseFloat(handlingCorrect) * parseFloat(suggested_price_val_without_qty) / parseFloat(suggested_price_val_total)) * qty.val();

            console.log('------------');
            console.log('handlingCorrect - ' + parseFloat(handlingCorrect));
            console.log('suggestPriceWithoutQty - ' + parseFloat(suggested_price_val_without_qty));
            console.log('suggestPriceTotal - ' + parseFloat(suggested_price_val_total));
            console.log('qty - ' + qty.val());
            console.log('newHandling');

            $.each(elems, function () {
                val = $(this).val();
                par = $(this).parent().parent();
                var qty1 = $('input[name='+id+'_qty]');
                id = par.attr('id');
                var sum = 0;
                var q1 = 0;
                var q2 = 0;
                if (pay_method === 'CC') {
                    let correctAmountShipping = sh(deformat(uttc_net_price_val2)) * uttc_net_price_val2 / deformat(CC_total_uttc_net_price);
                    q1 = deformat(suggested_price_val) - deformat(otherFees) - correctAmountShipping + newHandling - deformat(suggested_price_val) * parseFloat(0.030);
                    q2 = deformat(uttc_net_price_val2) ;
                    sum = ((q1 / q2) - parseFloat(1)) * parseFloat(100);
                    approx_profit.val((parseFloat(sum.toPrecision(4)))).css('text-align', 'right');
                    console.log('SC - ' + suggested_price_val);
                    console.log('NC - ' + uttc_net_price_val2);
                    console.log('ManufFee -' + otherFees);
                    console.log('OurFee - ' + newHandling);
                    console.log('BankFee - ' + (deformat(suggested_price_val) * parseFloat(0.030)));
                    console.log('AP - ' + (sum.toPrecision(4)));
                    console.log('q1 - ' + (q1));
                    console.log('q2 - ' + (q2));
                    console.log($("[data-brandnamealghandling="+brandName+"]"));
                    console.log($("[data-brandnamealghandling="+brandName+"]").attr("disabled") == "disabled");
                    console.log('hendCorect - ' + handlingCorrect);
                    console.log('CC');
                }

                if (pay_method === 'ACH') {
                    //sum = ((deformat(suggested_price.val()) - otherFees + parseFloat(newHandling)) / deformat(uttc_net_price.val()) - parseFloat(1)) * parseFloat(100);
                    //approx_profit.val((parseFloat(sum.toPrecision(4)))).css('text-align', 'right');

                    let correctAmountShipping = sh(deformat(uttc_net_price_val2)) * uttc_net_price_val2 / deformat(ACH_total_uttc_net_price);
                    q1 = deformat(suggested_price_val) - deformat(otherFees) - correctAmountShipping + newHandling ;
                    q2 = deformat(uttc_net_price_val2) ;
                    sum = ((q1 / q2) - parseFloat(1)) * parseFloat(100);
                    approx_profit.val((parseFloat(sum.toPrecision(4)))).css('text-align', 'right');
                }

                if (pay_method === 'PAYPAL') {
                    //sum = ((deformat(suggested_price.val()) - otherFees + parseFloat(newHandling) - deformat(suggested_price.val()) * parseFloat(0.029) - parseFloat(0.3)) / deformat(uttc_net_price.val()) - 1) * parseFloat(100);
                    //approx_profit.val((parseFloat(sum.toPrecision(4)))).css('text-align', 'right');

                    let correctAmountShipping = sh(deformat(uttc_net_price_val2)) * uttc_net_price_val2 / deformat(PAYPAL_total_uttc_net_price);
                    q1 = deformat(suggested_price_val) - deformat(otherFees) - correctAmountShipping + newHandling - deformat(suggested_price_val) * parseFloat(0.029) - parseFloat(0.3);
                    q2 = deformat(uttc_net_price_val2) ;
                    sum = ((q1 / q2) - parseFloat(1)) * parseFloat(100);
                    approx_profit.val((parseFloat(sum.toPrecision(4)))).css('text-align', 'right');
                }

                if (pay_method === 'EBAY') {
                    //sum = ((deformat(suggested_price.val()) - otherFees + parseFloat(newHandling)) / deformat(uttc_net_price.val()) - 1) * parseFloat(100);
                    //approx_profit.val((parseFloat(sum.toPrecision(4)))).css('text-align', 'right');

                    let correctAmountShipping = sh(deformat(uttc_net_price_val2)) * uttc_net_price_val2 / deformat(EBAY_total_uttc_net_price);
                    q1 = deformat(suggested_price_val) - deformat(otherFees) - correctAmountShipping + newHandling - (deformat(suggested_price_val) + sh(deformat(uttc_net_price_val2))) * deformat(0.029) - deformat(0.3);
                    q2 = deformat(uttc_net_price_val2) ;
                    sum = ((q1 / q2) - parseFloat(1)) * parseFloat(100);
                    approx_profit.val((parseFloat(sum.toPrecision(4)))).css('text-align', 'right');
                }

                if (pay_method === 'CHECK') {
                    sum = ((deformat(suggested_price.val()) - otherFees - sh(deformat(uttc_net_price_val2)) + parseFloat(newHandling)) / deformat(uttc_net_price.val()) - parseFloat(1)) * parseFloat(100);
                    approx_profit.val((parseFloat(sum.toPrecision(4)))).css('text-align', 'right');
                }
            });
        }

    }

    function approx_profit_total_calc(pay_method) {
        var total_approx_profit = $('#' + pay_method + '_total_approx_profit');
        var suggested_price_total = $('#' + pay_method + '_total_suggested_price');
        var total_suggested_price_final = $('#' + pay_method + '_total_suggested_price_final');
        var uttc_net_price_total = $('#' + pay_method + '_total_uttc_net_price');
        var approx_profit_total;

        var handlingFee = $('select[data-brandnamealghandling] option');

        var handlingFeeTotal = 0;
        handlingFee.each(function( index ) {
            var isChecked = $('input[name*="__checkbox"][data-id="0"]');
            if (isChecked && $(isChecked[0]).prop('checked')) {
                handlingFeeTotal += parseFloat($(this).val());
            }
        });

        var shipp_to_freeport = $('#shipp_to_freeport');
        var shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
        if ($('#freeshipp_to_customer').val() == 'Yes') {
            freep_to_custom = deformat(shipp_to_freeport_customer.val());

        }
        else {
            freep_to_custom = 0;
        }
        approx_profit_total = 0;

        var otherFees = (parseFloat(dropShipmentTariff) + parseFloat(handlingTariff) + parseFloat(minOrderFee) + parseFloat(4.75)); // TODO 4.75 => подпись

        var tax_profit = ($('#useTaxInProfit').val() == '1') ? deformat($('#tax_sum').val()) : 0;
        if (pay_method == 'CC') {
            otherFees -= 4.75;
            if ($('#freeshipp_to_customer').val() == 'Yes'){
                //approx_profit_total = ((deformat(suggested_price_total.val()) - (deformat(suggested_price_total.val()) * parseFloat(0.03)) - otherFees + parseFloat(handling) - deformat(shipp_to_freeport.val()) - deformat(freep_to_custom)) / deformat(uttc_net_price_total.val()) - parseFloat(1)) * parseFloat(100);
                //approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
                q1 = deformat(suggested_price_total.val()) - parseFloat(otherFees) - sh(deformat(uttc_net_price_total.val())) /*- tax_profit*/  + handlingFeeTotal - deformat(suggested_price_total.val()) * 0.03;
                approx_profit_total = ((q1 / deformat(uttc_net_price_total.val())) - parseFloat(1)) * parseFloat(100);
            }else{
                //approx_profit_total = ((deformat(suggested_price_total.val()) - (deformat(suggested_price_total.val()) * parseFloat(0.03)) - otherFees + parseFloat(handling)) / deformat(uttc_net_price_total.val()) - parseFloat(1)) * parseFloat(100);
                //approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
                q1 = deformat(suggested_price_total.val()) - parseFloat(otherFees) - sh(deformat(uttc_net_price_total.val())) /*- tax_profit*/ + handlingFeeTotal - deformat(suggested_price_total.val()) * 0.03;
                approx_profit_total = ((q1 / deformat(uttc_net_price_total.val())) - parseFloat(1)) * parseFloat(100);
            }
        }

        if (pay_method == 'ACH') {
            otherFeesAch = otherFees - 4.75;
            if ($('#freeshipp_to_customer').val() == 'Yes'){
                //approx_profit_total = ((deformat(suggested_price_total.val()) - otherFees + parseFloat(handling) - deformat(shipp_to_freeport.val()) - deformat(freep_to_custom)) / deformat(uttc_net_price_total.val()) - parseFloat(1)) * parseFloat(100);
                //approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
                q1 = deformat(suggested_price_total.val()) - parseFloat(otherFeesAch) - sh(deformat(uttc_net_price_total.val())) /*- tax_profit*/ + handlingFeeTotal;
                approx_profit_total = ((q1 / deformat(uttc_net_price_total.val())) - parseFloat(1)) * parseFloat(100);
            }else{
                //approx_profit_total = ((deformat(suggested_price_total.val()) - otherFees + parseFloat(handling)) / deformat(uttc_net_price_total.val()) - parseFloat(1)) * parseFloat(100);
                //approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
                q1 = deformat(suggested_price_total.val()) - parseFloat(otherFeesAch) - sh(deformat(uttc_net_price_total.val())) /*- tax_profit*/ + handlingFeeTotal;
                approx_profit_total = ((q1 / deformat(uttc_net_price_total.val())) - parseFloat(1)) * parseFloat(100);
            }
            console.log('mmmmm');
            console.log(otherFeesAch);

        }

        if (pay_method == 'PAYPAL') {
            if ($('#freeshipp_to_customer').val() == 'Yes'){
                //approx_profit_total = ((deformat(suggested_price_total.val()) - otherFees + parseFloat(handling) - deformat(suggested_price_total.val()) * parseFloat(0.029) - parseFloat(0.3) - deformat(shipp_to_freeport.val()) - deformat(freep_to_custom)) / deformat(uttc_net_price_total.val()) - 1) * parseFloat(100);
                //approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
                otherFees -= 4.75;
                q1 = deformat(suggested_price_total.val()) - parseFloat(otherFees) - sh(deformat(uttc_net_price_total.val())) /*- tax_profit*/ + handlingFeeTotal - deformat(suggested_price_total.val()) * parseFloat(0.029) - parseFloat(0.3);
                approx_profit_total = ((q1 / deformat(uttc_net_price_total.val())) - parseFloat(1)) * parseFloat(100);
            }else{
                //approx_profit_total = ((deformat(suggested_price_total.val()) - otherFees + parseFloat(handling) - deformat(suggested_price_total.val()) * parseFloat(0.029) - parseFloat(0.3)) / deformat(uttc_net_price_total.val()) - 1) * parseFloat(100);
                //approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
                otherFees -= 4.75;
                q1 = deformat(suggested_price_total.val()) - parseFloat(otherFees) - sh(deformat(uttc_net_price_total.val())) /*- tax_profit*/ + handlingFeeTotal - deformat(suggested_price_total.val()) * parseFloat(0.029) - parseFloat(0.3);
                approx_profit_total = ((q1 / deformat(uttc_net_price_total.val())) - parseFloat(1)) * parseFloat(100);
            }
        }

        if (pay_method == 'EBAY') {
            if ($('#freeshipp_to_customer').val() == 'Yes'){
                //approx_profit_total = ((deformat(suggested_price_total.val()) - otherFees + parseFloat(handling) - deformat(shipp_to_freeport.val()) - deformat(freep_to_custom)) / deformat(uttc_net_price_total.val()) - 1) * parseFloat(100);
                //approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
                otherFees -= 4.75;
                q1 = deformat(suggested_price_total.val()) - parseFloat(otherFees) - sh(deformat(uttc_net_price_total.val())) /*- tax_profit*/  + handlingFeeTotal - (deformat(suggested_price_total.val()) + sh(deformat(uttc_net_price_total.val()))) * deformat(0.029) - deformat(0.3);
                approx_profit_total = ((q1 / deformat(uttc_net_price_total.val())) - parseFloat(1)) * parseFloat(100);
            }else{
                //approx_profit_total = ((deformat(suggested_price_total.val()) - otherFees + parseFloat(handling)) / deformat(uttc_net_price_total.val()) - 1) * parseFloat(100);
                //approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
                otherFees -= 4.75;
                q1 = deformat(suggested_price_total.val()) - parseFloat(otherFees) - sh(deformat(uttc_net_price_total.val())) /*- tax_profit*/  + handlingFeeTotal - (deformat(suggested_price_total.val()) + sh(deformat(uttc_net_price_total.val()))) * deformat(0.029) - deformat(0.3);
                approx_profit_total = ((q1 / deformat(uttc_net_price_total.val())) - parseFloat(1)) * parseFloat(100);
            }
        }

        if (pay_method == 'CHECK') {
            if ($('#freeshipp_to_customer').val() == 'Yes'){
                // approx_profit_total = ((deformat(suggested_price_total.val()) - otherFees + parseFloat(handling) - deformat(shipp_to_freeport.val()) - deformat(freep_to_custom)) / deformat(uttc_net_price_total.val()) - parseFloat(1)) * parseFloat(100);
                approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
            }else{
                //approx_profit_total = ((deformat(suggested_price_total.val()) - otherFees + parseFloat(handling)) / deformat(uttc_net_price_total.val()) - parseFloat(1)) * parseFloat(100);
                approx_profit_total = ((deformat(total_suggested_price_final.val()) - parseFloat(handlingTariff)) * 100) / deformat(uttc_net_price_total.val());
            }
        }

        total_approx_profit.val(approx_profit_total.toFixed(2));

    }


    //Пересчет поля Final Multiplier
    function multiplier_f(pay_method) {
        tr = $('tr[pay-type="' + pay_method + '"]');

        $.each(tr, function () {
            var id = $(this).attr('id');

            f(pay_method, id);
        });


        function f(pay_method, id) {

            var shipp_to_freeport = $('#shipp_to_freeport');
            var shipp_to_freeport_customer = $('#shipp_to_freeport_customer');

            var final_multiplier = $("input[pay-type='" + pay_method + "'][name='" + id + "_final_1multiplier']");
            var multiplier = $("input[pay-type='" + pay_method + "'][name='" + id + "_multiplier']");
            var suggested_price = $("input[pay-type='" + pay_method + "'][name='" + id + "_suggested_price']");
            var uttc_net_price = $("input[pay-type='" + pay_method + "'][name='" + id + "_uttc_net_price']");
            var multiplier_flag = $("input[pay-type='" + pay_method + "'][name='" + id + "_multiplier_flag']").val();
            var final_sale_price_flag = $("input[pay-type='" + pay_method + "'][name='" + id + "_fsp_flag']").val();
            var final_sale_price = $("input[pay-type='" + pay_method + "'][name='" + id + "_final_sale_price']");
            var elems = $('input[pay-type="' + pay_method + '"][name*="_approx_profit"]');
            var sum;
            $.each(elems, function () {
                val = $(this).val();
                par = $(this).parent().parent();
                id = par.attr('id');
                var sum = 0;
                if (pay_method === 'CC') {
                    sum = (deformat(suggested_price.val()) - deformat(suggested_price.val()) * 0.03) / deformat(uttc_net_price.val());
                    final_multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');

                    if (multiplier_flag == 0) {
                        if (final_sale_price_flag == 1) {
                            res = (deformat(final_sale_price.val()) / deformat(uttc_net_price.val())) * 0.97;
                            multiplier.val(parseFloat(res.toFixed(2)));
                        }
                        else {
                            multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                        }
                    }
                }

                if (pay_method === 'ACH') {
                    sum = deformat(suggested_price.val()) / deformat(uttc_net_price.val());
                    final_multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                    if (multiplier_flag == 0) {
                        if (final_sale_price_flag == 1) {
                            res = deformat(final_sale_price.val()) / deformat(uttc_net_price.val());
                            multiplier.val(parseFloat(res.toFixed(2)));
                        }
                        else {
                            multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                        }

                    }
                }

                if (pay_method === 'CHECK') {
                    sum = deformat(suggested_price.val()) / deformat(uttc_net_price.val());
                    final_multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                    if (multiplier_flag == 0) {
                        if (final_sale_price_flag == 1) {
                            res = deformat(final_sale_price.val()) / deformat(uttc_net_price.val());
                            multiplier.val(parseFloat(res.toFixed(2)));
                        }
                        else {
                            multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                        }

                    }
                }

                if (pay_method === 'PAYPAL') {
                    sum = (deformat(suggested_price.val()) - deformat(suggested_price.val()) * 0.029 - 0.3) / deformat(uttc_net_price.val());
                    final_multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                    if (multiplier_flag == 0) {
                        if (final_sale_price_flag == 1) {
                            res = ((deformat(final_sale_price.val()) - 0.31) * 0.971) / deformat(uttc_net_price.val());
                            multiplier.val(parseFloat(res.toFixed(2)));
                        }
                        else {
                            multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                        }
                    }
                }

                if (pay_method === 'EBAY') {
                    sum = (deformat(suggested_price.val()) - deformat(shipp_to_freeport.val()) - deformat(shipp_to_freeport_customer.val())) / deformat(uttc_net_price.val());
                    final_multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                    if (multiplier_flag == 0) {
                        if (final_sale_price_flag == 1) {
                            res = (deformat(final_sale_price.val()) - deformat(shipp_to_freeport.val()) - deformat(shipp_to_freeport_customer.val())) / deformat(uttc_net_price.val());
                            multiplier.val(parseFloat(res.toFixed(2)));
                        }
                        else {
                            multiplier.val(parseFloat(sum.toFixed(2))).css('text-align', 'right');
                        }
                    }
                }


            });
        }


    }

    //Пересчет поля Final Sale Price
    function final_sale_price_f(pay_method) {
        tr = $('tr[pay-type="' + pay_method + '"]');

        $.each(tr, function () {
            var id = $(this).attr('id');

            f(pay_method, id);
        });


        function f(pay_method, id) {
            var final_sale_price_res;
            var uttc_sell_price = $("input[pay-type='" + pay_method + "'][name='" + id + "_uttc_sell_price']");
            var final_sale_price = $("input[pay-type='" + pay_method + "'][name='" + id + "_final_sale_price']");
            var multiplier = $("input[pay-type='" + pay_method + "'][name='" + id + "_multiplier']");
            var uttc_net_price = $("input[pay-type='" + pay_method + "'][name='" + id + "_uttc_net_price']");
            var final_sale_price_flag = $("input[name='" + id + "_fsp_flag']").val();
            var multiplier_flag = $("input[name='" + id + "_multiplier_flag']").val();
            var shipp_to_freeport = $('#shipp_to_freeport');
            var shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
            final_sale_price_res = 0;

            if (pay_method == 'CC') {
                if (multiplier_flag == 0 && final_sale_price_flag == 0) {
                    final_sale_price_res = deformat(uttc_sell_price.val());
                }
                else {
                    final_sale_price_res = (deformat(uttc_net_price.val()) * deformat(multiplier.val())) / 0.97;
                }
            }

            if (pay_method == 'ACH') {

                if (flag_CC == 1) {

                    var fsp = $('input[pay-type="' + pay_method + '"][name="' + id + '_final_sale_price"]');
                    var $pn = fsp.attr('pn');
                    //alert($pn);
                    var final_sale_price_CC = $('input[pay-type="CC"][pn="' + $pn + '"][name*="_final_sale_price"]');
                    //alert(final_sale_price_CC.val());

                    final_sale_price_res = deformat(final_sale_price_CC.val()) * 0.99;
                    //alert('res:' + final_sale_price_res);
                }
                else {
                    if (multiplier_flag == 0 && final_sale_price_flag == 0) {
                        final_sale_price_res = deformat(uttc_sell_price.val());
                    }
                    else {
                        final_sale_price_res = (deformat(uttc_net_price.val()) * deformat(multiplier.val()));
                    }
                }


            }
            if (pay_method == 'CHECK') {

                if (flag_CC == 1) {

                    var fsp = $('input[pay-type="' + pay_method + '"][name="' + id + '_final_sale_price"]');
                    var $pn = fsp.attr('pn');
                    //alert($pn);
                    var final_sale_price_CC = $('input[pay-type="CC"][pn="' + $pn + '"][name*="_final_sale_price"]');
                    //alert(final_sale_price_CC.val());

                    final_sale_price_res = deformat(final_sale_price_CC.val()) * 0.99;
                    //alert('res:' + final_sale_price_res);
                }
                else {
                    if (multiplier_flag == 0 && final_sale_price_flag == 0) {
                        final_sale_price_res = deformat(uttc_sell_price.val());
                    }
                    else {
                        final_sale_price_res = (deformat(uttc_net_price.val()) * deformat(multiplier.val()));
                    }
                }


            }
            if (pay_method == 'PAYPAL') {
                if (multiplier_flag == 0 && final_sale_price_flag == 0) {
                    final_sale_price_res = deformat(uttc_sell_price.val());
                }
                else {
                    final_sale_price_res = (deformat(uttc_net_price.val()) * multiplier.val()) / (1 - 0.029) + 0.31;
                }
            }
            if (pay_method == 'EBAY') {
                if (multiplier_flag == 0 && final_sale_price_flag == 0) {
                    final_sale_price_res = deformat(uttc_sell_price.val());
                }
                else {
                    final_sale_price_res = (deformat(uttc_net_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val())) * multiplier.val();
                }
            }

            if (final_sale_price_flag == 0) {
                final_sale_price.val(priceFormat(final_sale_price_res));
                // alert(final_sale_price.val());
            }


            recalc(pay_method, id);

        }

    }

    function app_total_final_sale_price() {
        tr = $('tr[pay-type="' + pay_method + '"]');

        $.each(tr, function () {
            var id = $(this).attr('id');

            f(pay_method, id);
        });
        function f(pay_method, id) {
            var app_total_final_sale_price = $('#' + pay_method + '_app_total_final_sale_price');
            var final_suggested_price = $('#' + pay_method + '_total_suggested_price_final');
            var approximate_profit = $('#' + pay_method + '_approximate_profit');
            var total_uttc_net_price = $('#' + pay_method + '_total_uttc_net_price');
            var total_final_sale_price = $('#' + pay_method + '_total_final_sale_price');

            var shipp_to_freeport = $('#shipp_to_freeport');
            var shipp_to_freeport_customer = $('#shipp_to_freeport_customer');
            var freeport = $('#freeport');
            var value, value1, value2, value3;
            var sum;
///CC
            if (pay_method == 'CC') {
                sum = deformat(total_final_sale_price.val()) - (deformat(total_final_sale_price.val()) * parseFloat(0.03)) - deformat(total_uttc_net_price.val()) - sh(deformat(total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;

            }

///ACH
            if (pay_method == 'ACH') {
                sum = deformat(total_final_sale_price.val()) - deformat(total_uttc_net_price.val()) - sh(deformat(total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;
            }
///CHECK
            if (pay_method == 'CHECK') {
                sum = deformat(total_final_sale_price.val()) - deformat(total_uttc_net_price.val()) - sh(deformat(total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;
            }

///PAYPAL
            if (pay_method == 'PAYPAL') {
                sum = deformat(total_final_sale_price.val()) - deformat(total_uttc_net_price.val()) - sh(deformat(total_uttc_net_price.val())) - deformat(total_final_sale_price.val()) * parseFloat(0.029) - parseFloat(0.3) + handling - minOrderFee - handlingTariff - dropShipmentTariff;
            }

///EBAY

            if (pay_method == 'EBAY') {
                sum = 0;
                value = 0;
                value2 = 0;

                if (deformat(total_final_sale_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val()) < parseFloat(50.01)) {
                    value = (deformat(total_final_sale_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val())) * parseFloat(0.11);

                }
                else {
                    if (deformat(total_final_sale_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val()) < parseFloat(1000.01)) {
                        value = (deformat(total_final_sale_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val()) - parseFloat(50)) * parseFloat(0.06) + parseFloat(5.5);

                    } else {

                        value = (deformat(total_final_sale_price.val()) + deformat(shipp_to_freeport.val()) + deformat(shipp_to_freeport_customer.val()) - parseFloat(1000)) * parseFloat(0.02) + parseFloat(62.5);

                    }
                }

                sum = deformat(total_final_sale_price.val()) - deformat(total_uttc_net_price.val()) - (deformat(total_final_sale_price.val()) + sh(deformat(total_uttc_net_price.val()))) * parseFloat(0.029) - parseFloat(0.3) - parseFloat(value) - sh(deformat(total_uttc_net_price.val())) + handling - minOrderFee - handlingTariff - dropShipmentTariff;
            }

            app_total_final_sale_price.val(priceFormat(sum));
        }

    }

    function total_final_sale_price(pay_method) {

        var elems = $('input[pay-type="' + pay_method + '"][name*="_final_sale_price"]');
        var total = 0;
        $.each(elems, function () {
            val = deformat($(this).val());
            par = $(this).parent().parent();
            id = par.attr('id');
            if (val === null || val == '') {
                val = 0;
            }

            total += parseFloat(val) * $('input[pay-type="' + pay_method + '"][name="' + id + '_qty"]').val();
        });
        $("#" + pay_method + "_total_final_sale_price").val(priceFormat(rounding(total))).css('text-align', 'right');


    }

    function net_price_data() {
        // ////console.log("Airy count: " + $('input[class="brandtag"][pay-type="CC"][value="Airy technology"]').length);
        // ////console.log("HazMat count: " + $('tr[brand="Bacharach"][haz-mat="H"][pay-type="CC"]').length);
        var hannaProductCCCountCheck = 0;
        var hannaProductACHCountCheck = 0;
        var hannaProductPAYPALCountCheck = 0;
        var hannaProductEBAYCountCheck = 0;
        var hannaCCRowCount = 0;
        var hannaACHRowCount = 0;
        var hannaPAYPALRowCount = 0;
        var hannaEBAYRowCount = 0;

        var infoMsg = new Array;
        var elem = $('input[name*="_qty"]');
        $.each(elem, function () {

            var net_p_c = 0;
            var id = $(this).parent().parent().attr('id');
            var brand = $(this).parent().parent().attr('brand');
            var part_number = $(this).parent().parent().attr('pn');
            var pay_t = $(this).parent().parent().attr('pay-type');
            var qty = $(this).val();
            var net_price = $('input[name="' + id + '_uttc_net_price"]');
            var product_status = $(this).parent().parent().attr('product-status');
            var box_qty = $(this).attr('box-qty');
            var manuf_box_qty = $(this).attr('manuf-box-qty');
            //#########################################################################################################
            //#########################################################################################################
            //#########################################################################################################
            //#########################################################################################################
            //#########################################################################################################
            //###########################                                                   ###########################
            //###########################     КОММЕНТАРИИ К БРЕНДАМ. УСЛОВИЯ ПО БРЕНДАМ     ###########################
            //###########################                                                   ###########################
            //#########################################################################################################
            //#########################################################################################################
            //#########################################################################################################
            //#########################################################################################################
            //#########################################################################################################
            var sum = qty * deformat(net_price.val());
            net_price.attr('data-value', sum);
            if(brand == '<strong>Master lock:</strong>') {
                var master_lock_msg = "• Есть понятие кратности упаковки, а так же цена может зависеть от количества. См. алгоритм по бренду. Скидка автоматически учитывается в Net Price.";
                infoMsg.push(master_lock_msg);
                var price_static = net_price.attr('static-price');

                var box_qty_price_change = $(this).attr('box-qty-price-change');


                if (price_static == 0) {
                    var sum = qty * deformat(net_price.val());
                    net_price.attr('data-value', sum);
                }
                // else if (parseInt(qty) < box_qty) {
                //     master_lock_msg = "• В таблицу добавленны товары, по которым кол-во меньше реккомендуемого. По таким товарам к Net Price добавлено 10%."
                //     infoMsg.push(master_lock_msg);
                //     var price_value = price_static * 1.1;
                //     net_price.val(rounding(price_value));
                //     net_price.attr('data-value', rounding(price_value * qty));
                //
                // }
                else {
                    net_price.val(rounding(price_static));
                    net_price.attr('data-value', rounding(price_static * qty));


                }
            }
            else if(brand == 'Dwyer instruments, inc.' /*|| brand == 'Love controls' || brand == 'Mercoid controls' || brand == 'Proximity' || brand == 'W.e.anderson'*/) {
                var discount = 1;
                var list_price = net_price.attr('list-price');


                dwyer_info = "<strong>" + brand + ":</strong><br/>• Скидка зависит от количества купленного товара. Все скидки учтены в Net Price.<br/>Disc for Qty 10-24 - 25%<br/>Disc for Qty 25-99 - 30%<br/>Disc for Qty 100+ - 35%<br/>На некоторые товары скидки нет!";
                infoMsg.push(dwyer_info);
                if (table_status !== 'Approved') {
                    if (list_price > 0) {
                        $.ajax({
                            url: 'index.php?page=conditions&action=Dwyer',
                            type: 'POST',
                            async: false,
                            data: {
                                qty: qty,
                                part_number: part_number
                            },
                            success: function (response) {

                                var data = jQuery.parseJSON(response);
                                sum = data.price;


                                console.log(data);

                                // for(i = 0; i<data.length; i++)
                                // {
                                //     var item = data[i];
                                //    // ////console.log(item);
                                //    // ////console.log("range: " + item.range.min  );
                                //     if(qty >= item.range.min && qty <= item.range.max)
                                //     {
                                //         discount = parseFloat(item.discount);
                                //         break;
                                //     }
                                // }


                            }
                        });
                        //sum = list_price - (list_price * discount);
                        // if(sum > 0) {
                        net_price.val(rounding(sum));
                        net_price.attr('data-value', rounding(sum * qty));
                        // }
                    }
                    //var dwyer_info = "<strong>"+brand+":</strong>";
                    // infoMsg.push(dwyer_info);

                }

            }else if(brand == 'Luxtel'){
                discount = 1;

                anderson_info = "<strong>" + brand + ":</strong><br/>• Скидка зависит от количества купленного товара. Скидка автоматически учитывается в Net Price.<br/>Скидка работает при следующих qty:<br/>- 5 - 9 pcs.<br/>- 10 - 24 pcs.<br/>- 25+ pcs.";
                infoMsg.push(anderson_info);
                if (table_status !== 'Approved') {
                    $.ajax({
                        url: 'index.php?page=conditions&action=Luxtel',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response) {
                            var data = jQuery.parseJSON(response);
                            sum = data.price;
                        },
                        error: function (error) {
                            console.log(error);
                            alert('Error set net price!');
                        }
                    });
                    net_price.val(rounding(sum));
                    net_price.attr('data-value', rounding(sum * qty));
                }
            }else if(brand == 'W.e.anderson') {
                discount = 1;
                list_price = net_price.attr('list-price');

                anderson_info = "<strong>" + brand + ":</strong><br/>• Скидка зависит от количества купленного товара. Все скидки учтены в Net Price.<br/>Disc for Qty 10-24 - 25%<br/>Disc for Qty 25-99 - 30%<br/>Disc for Qty 100+ - 35%<br/>На некоторые товары скидки нет!";
                infoMsg.push(anderson_info);
                if (table_status !== 'Approved') {
                    if (list_price > 0) {
                        $.ajax({
                            url: 'index.php?page=conditions&action=anderson',
                            type: 'POST',
                            async: false,
                            data: {
                                qty: qty,
                                part_number: part_number
                            },
                            success: function (response) {
                                var data = jQuery.parseJSON(response);
                                sum = data.price;
                            }
                        });
                        net_price.val(rounding(sum));
                        net_price.attr('data-value', rounding(sum * qty));
                    }
                }
            }else if(brand == 'Proximity') {
                discount = 1;
                list_price = net_price.attr('list-price');

                Prox_info = "<strong>" + brand + ":</strong><br/>• Скидка зависит от количества купленного товара. Все скидки учтены в Net Price.<br/>Disc for Qty 10-24 - 25%<br/>Disc for Qty 25-99 - 30%<br/>Disc for Qty 100+ - 35%<br/>На некоторые товары скидки нет!";
                infoMsg.push(Prox_info);
                if (table_status !== 'Approved') {
                    if (list_price > 0) {
                        $.ajax({
                            url: 'index.php?page=conditions&action=Proximity',
                            type: 'POST',
                            async: false,
                            data: {
                                qty: qty,
                                part_number: part_number
                            },
                            success: function (response) {
                                var data = jQuery.parseJSON(response);
                                sum = data.price;
                            }
                        });
                        net_price.val(rounding(sum));
                        net_price.attr('data-value', rounding(sum * qty));
                    }
                }
            }else if(brand == 'Mercoid controls') {
                discount = 1;
                list_price = net_price.attr('list-price');

                merc_info = "<strong>" + brand + ":</strong><br/>• Скидка зависит от количества купленного товара. Все скидки учтены в Net Price.<br/>Disc for Qty 10-24 - 25%<br/>Disc for Qty 25-99 - 30%<br/>Disc for Qty 100+ - 35%<br/>На некоторые товары скидки нет!";
                infoMsg.push(merc_info);
                if (table_status !== 'Approved') {
                    if (list_price > 0) {
                        $.ajax({
                            url: 'index.php?page=conditions&action=Mercoid',
                            type: 'POST',
                            async: false,
                            data: {
                                qty: qty,
                                part_number: part_number
                            },
                            success: function (response) {
                                var data = jQuery.parseJSON(response);
                                sum = data.price;
                            }
                        });
                        net_price.val(rounding(sum));
                        net_price.attr('data-value', rounding(sum * qty));
                    }
                }
            }else if(brand == 'Love controls'){
                discount = 1;
                list_price = net_price.attr('list-price');

                love_info = "<strong>" + brand + ":</strong><br/>• Скидка зависит от количества купленного товара. Все скидки учтены в Net Price.<br/>Disc for Qty 10-24 - 25%<br/>Disc for Qty 25-99 - 30%<br/>Disc for Qty 100+ - 35%<br/>На некоторые товары скидки нет!";
                infoMsg.push(love_info);
                if (table_status !== 'Approved') {
                    if (list_price > 0) {
                        $.ajax({
                            url: 'index.php?page=conditions&action=Love_controls',
                            type: 'POST',
                            async: false,
                            data: {
                                qty: qty,
                                part_number: part_number
                            },
                            success: function (response) {
                                var data = jQuery.parseJSON(response);
                                sum = data.price;
                            }
                        });
                        net_price.val(rounding(sum));
                        net_price.attr('data-value', rounding(sum * qty));
                    }
                }
            }else if(brand == 'Ideal')
            {
                if(box_qty < manuf_box_qty)
                {
                    var ideal_info = "<strong>Ideal:</strong>";
                    infoMsg.push(ideal_info);
                    ideal_info = "• Почти на все товары есть минимальный заказ и кратность упаковки. При несоблюдении условий, выскочит оповещение.";
                    infoMsg.push(ideal_info);

                }
                net_p_c++;

            }else if(brand == 'Vaisala'){
                var vaisala_msg = "<strong>Vaisala:</strong>";
                infoMsg.push(vaisala_msg);
                vaisala_msg = "• WARNING: составные парт номера, цены брать с прайса, обратите внимание на приписки под каждой таблицей при построении парт номеров с PDF прайса. Там часто указывается, что еще включает в себя даный товар и какие его особенности.<br/>• Все цены необходимо заносить вручную.";
                infoMsg.push(vaisala_msg);
                net_p_c++;
            }
            else if(brand == 'Airy technology'){
                if($('input[class="brandtag"][pay-type="CC"][value="Airy technology"]').length > 2){
                    var airytech_msg = "<strong>Airy Technology:</strong>";
                    infoMsg.push(airytech_msg);

                    airytech_msg = "• Когда заказ состоит из несколькиз заказов, то можно запросить дополнительную скидку";
                    infoMsg.push(airytech_msg);
                    net_p_c++;
                }
            }else if(brand == 'Bacharach'){
                if($('tr[brand="Bacharach"][haz-mat="H"][pay-type="CC"]').length > 0) {
                    var bacharach_msg = "<strong>Bacharach:</strong>";
                    infoMsg.push(bacharach_msg);
                    bacharach_msg = "• Один из товаров содержит Hazardous Materials, за это берут fee при доствке, в прайсе обозначены буквой H в колонке Haz Mat. В таблице Fee за Hazardous Materials не учтены.<br/>У таких товаров поле Part Number подсвеченно желтым цветом.";
                    infoMsg.push(bacharach_msg);

                }
                net_p_c++;
            }else if(brand == 'Sper scientific'){
                var sper_scientific_msg = "<strong>Sper Scientific:</strong>";
                infoMsg.push(sper_scientific_msg);
                sper_scientific_msg = "• Скидка зависит от количества купленного товара. Скидка автоматически учитывается в Net Price.<br/>Скидка работает при следующих qty:<br/>- 1 - 9 pcs.<br/>- 10 - 99 pcs.<br/>- 100+ pcs.";
                infoMsg.push(sper_scientific_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=SperScientific',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function( response, data )
                        {
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    ////console.log(net_price.val());
                                }
                            }
                        }
                    });
                }
            } else if (brand == 'Anti-seize technology') {
                var anti_seize_technology_msg = "<strong>Anti-Seize Technology:</strong>";
                infoMsg.push(anti_seize_technology_msg);
                anti_seize_technology_msg = "• Скидка зависит от количества купленного товара. Скидка автоматически учитывается в Net Price.<br/>Цена зависит от кол-ва кейсов:<br/>- Less 1 case<br/>- 1 - 5 cases<br/>- 6 - 19 cases<br/>- 20 - 49 cases<br/>- 50+ cases";
                infoMsg.push(anti_seize_technology_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=AntiSeizeTechnology',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number,
                            manuf_box_qty: manuf_box_qty
                        },
                        success: function (response, data) {
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                }
                            }
                        }
                    });

                }
            }else if (brand == 'Scilogex') {
                var scilogex_msg = "<strong>Scilogex:</strong>";
                infoMsg.push(scilogex_msg);
                scilogex_msg = "• Скидка зависит от количества купленного товара, на некоторые товары скидки нет. Скидка автоматически учитывается в Net Price.<br/>Цена зависит от кол-ва:<br/>- 1 - 4 pcs.<br/>- 5 - 9 pcs.<br/>- 10 - 24 pcs.<br/>- 25 - 99 pcs.<br/>- 100+ pcs.";
                infoMsg.push(scilogex_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Scilogex',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            var json = $.parseJSON(response);

                            if(json !== null){
                                if (json.price !== null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price*qty));
                                }
                            }
                        }
                    });

                }
            }else if (brand == 'Tpi') {
                var pnArray = ['A725', 'A727', 'A728'];
                //////console.log($.inArray(part_number, pnArray));
                if($.inArray(part_number, pnArray) > -1)
                {
                    var tpi_msg = "<strong>TPI:</strong>";
                    infoMsg.push(tpi_msg);
                    tpi_msg = "• WARNING: A725/A727/A728 products - cal gas or tracer gas is not shipped from TPI due to hazmat. It is shipped directly from the supplier. These cannot be returned. These products cannot be exported.";
                    infoMsg.push(tpi_msg);
                }
                net_p_c++;


            }else if (brand == 'Dixon valve') {
                var in_package = $(this).attr('buy-in-package');
                var inPackageArrayYes = ['Yes', 'Y'];
                var inPackageArrayCall = ['call'];
                var partNum_field = $('textarea[name="' + id + '_partnum"]');
                if ($.inArray(in_package, inPackageArrayYes) > -1)
                {
                    dixon_msg ="<strong>Dixon Valve:</strong>";
                    infoMsg.push(dixon_msg);

                    partNum_field.css('background-color', 'yellow');

                    dixon_msg = "• Некоторые товары могут продаваться только упаковками! У таких товаров поле Part Number подсвечено желтым цветом. Для того чтоб узнать кратность упаковки, наведите на поле Qty (Кол-во), в всплывающем окне будет показана кратность.";
                    infoMsg.push(dixon_msg);
                }

                if($.inArray(in_package, inPackageArrayCall) > -1)
                {
                    dixon_msg ="<strong>Dixon Valve:</strong>";
                    infoMsg.push(dixon_msg);
                    partNum_field.css('background-color', 'rgb(0, 162, 210)');
                    partNum_field.css('border', '2px solid red');

                    dixon_msg ="• В таблице добавленны товары, по которым нужно уточнять у завода минимальное кол-во и кратность упаковки. У таких товаров поля Part Number подсвеченны синим цветом в красной рамочке.";
                    infoMsg.push(dixon_msg);
                }
                net_p_c++;

            }else if (brand == 'Brady people id') {
                var bradyPeopleId_msg = "<strong>Brady People ID:</strong>";
                infoMsg.push(bradyPeopleId_msg);
                bradyPeopleId_msg = "• Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Скидка автоматически учитывается в Net Price.<br/>На каждый товар свой кол-ный рубеж.";
                infoMsg.push(bradyPeopleId_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=BradyPeopleId',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            ////console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Tasco') {
                var tasco_msg = "<strong>Tasco:</strong>";
                infoMsg.push(tasco_msg);
                tasco_msg = "•  Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Скидка автоматически учитывается в Net Price.<br/>Цена зависит от кол-ва:<br/>- 1+ pcs.<br/>- 3+ pcs.<br/>- 10+ pcs.<br/>- 100+ pcs.";
                infoMsg.push(tasco_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Tasco',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            ////console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Comark') {
                var comark_msg = "<strong>Comark:</strong>";
                infoMsg.push(comark_msg);
                comark_msg = "•  Кол-во должно соответствовать кратности, указанной в всплывающем окне, при наведении курсора на поле Qty.";
                infoMsg.push(comark_msg);
                net_p_c++;

            }else if (brand == 'Lignomat') {
                var lignomat_msg = "<strong>Lignomat:</strong>";
                infoMsg.push(lignomat_msg);
                lignomat_msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- 1 pc.<br/>- 2 - 9 pcs.<br/>- 10 - 19 pcs.<br/>- 20+ pcs.";
                infoMsg.push(lignomat_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Lignomat',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            ////console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                }
                            }
                        }
                    });

                }
            } else if (brand == 'Meriam') {
                if ($('input[class="brandtag"][value="Meriam"]').length >= 2) {
                    var meriam_msg = "<strong>Meriam:</strong>";
                    infoMsg.push(meriam_msg);
                    meriam_msg = "•  Когда запрос состоит из нескольких товаров, то можно запросить дополнительную скидку.";
                    infoMsg.push(meriam_msg);
                }
                net_p_c++;
            }else if (brand == 'Accutrak') {
                var accutrak_msg = "<strong>AccuTrak:</strong>";
                infoMsg.push(accutrak_msg);
                accutrak_msg = "•    Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- 1 - 4 pc.<br/>- 5+ pcs.";
                infoMsg.push(accutrak_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Accutrak',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            ////console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));

                                }
                            }


                        }
                    });
                }

            }else if (brand == 'Adam equipment') {
                var adamEquip_msg = "<strong>Adam Equipment:</strong>";
                infoMsg.push(adamEquip_msg);
                adamEquip_msg = "•    Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1<br/> - Qty 2+<br/> - Qty 5+<br/> - Qty 6+<br/> - Qty 8+<br/> - Qty 10+<br/> - Qty 12+<br/> - Qty 24+";
                infoMsg.push(adamEquip_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=AdamEquipment',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            ////console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    ////console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Irrometer') {
                var adamEquip_msg = "<strong>Irrometer:</strong>";
                infoMsg.push(adamEquip_msg);
                adamEquip_msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 4+<br/> - Qty 6+<br/> - Qty 10+<br/> - Qty 24+<br/> - Qty 25+<br/> - Qty 36+";
                infoMsg.push(adamEquip_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Irrometer',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            ////console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    ////console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'General tools') {
                var generaltools_msg = "<strong>General tools:</strong>";
                infoMsg.push(generaltools_msg);
                generaltools_msg = "•   Qty должен быть не менее чем Manuf min units (Если Manuf min units больше 1, то при наведении на поле Qty, отображается в сплывающем окне)";
                infoMsg.push(generaltools_msg);
                net_p_c++;

            }else if (brand == 'Schonstedt') {
                var schonstedt_msg = "<strong>Schonstedt:</strong>";
                infoMsg.push(schonstedt_msg);
                schonstedt_msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 10+<br/> - Qty 50+";
                infoMsg.push(schonstedt_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Schonstedt',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            ////console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    ////console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Palmer wahl') {
                var palmer_msg = "<strong>Palmer Wahl:</strong>";
                infoMsg.push(palmer_msg);
                palmer_msg = "• Наша скидка зависит от количества товаров в заказе. Все условия прописаны в прайсе. Автоматически скидка не считается.";
                infoMsg.push(palmer_msg);
                net_p_c++;

            }else if (brand == 'Kaltman') {
                var schonstedt_msg = "<strong>Kaltman:</strong>";
                infoMsg.push(schonstedt_msg);
                schonstedt_msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 2+<br/> - Qty 4+";
                infoMsg.push(schonstedt_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Kaltman',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //  //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //  //console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Fieldpiece') {
                var schonstedt_msg = "<strong>Fieldpiece:</strong>";
                infoMsg.push(schonstedt_msg);
                schonstedt_msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 2+";
                infoMsg.push(schonstedt_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Fieldpiece',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Apg') {
                var schonstedt_msg = "<strong>APG:</strong>";
                infoMsg.push(schonstedt_msg);
                schonstedt_msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Кол-во, у товаров может быть разным.";
                infoMsg.push(schonstedt_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=APG',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Powersight') {
                var schonstedt_msg = "<strong>PowerSight:</strong>";
                infoMsg.push(schonstedt_msg);
                schonstedt_msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 25+<br/> - Qty 50+<br/> - Qty 100+";
                infoMsg.push(schonstedt_msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=PowerSight',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });

                }
            }
            else if (brand == 'Fjm security products') {
                var msg = "<strong>FJM Security Products:</strong>";
                infoMsg.push(msg);
                msg = "•   На некоторые товары есть мин. заказ. Его можно узнать, если навести на подсвеченное поле Qty.";
                infoMsg.push(msg);
                msg = "•   В зависимости от кол-ва, в Net Price автоматически учитывается размер скидки.";
                infoMsg.push(msg);
                  if(table_status !== 'Approved'){
                $.ajax({
                    url: 'index.php?page=conditions&action=FjmSecurityProducts',
                    type: 'POST',
                    async: false,
                    data: {
                        qty: qty,
                        part_number: part_number,
                        net_price: $(net_price).val()
                    },
                    success: function (response, data) {
                        //console.log(response);
                        var json = $.parseJSON(response);
                        var freeport_select = $('#freeport');
                        //  //console.log(freeport_select.val());

                        if(freeport_select.val() == 'Yes')
                        {
                            msg = "•   Если мы заказываем товар to our facility (на фрипорт), минимальное количество будет 1 коробка ('Inner box Qty'). Если мы дропаем товар с завода на адрес клиента, тогда минимальное кол-во будет 'min order (ea)' с прайса на 2015.";
                            infoMsg.push(msg);
                        }
						//json.case_qty != null && json.case_qty > 0
                        if(parseInt(qty) >= parseInt(json.case_qty) && json.case_qty != null && json.case_qty > 0 && json.case_cost_each != null && json.case_cost_each > 0)
                        {
                            net_price.val(priceFormat(json.case_cost_each));
                            net_price.attr('data-value', rounding(json.case_cost_each*qty));
                            //console.log(json.case_cost_each);
                        } else {
                            net_price.val(priceFormat(json.standart_price));
                            net_price.attr('data-value', rounding(json.standart_price*qty));
                            //console.log(json.standart_price);
                        }

                    }
                });
}

            }else if (brand == 'Zebra') {
                var msg = "<strong>Zebra:</strong>";
                infoMsg.push(msg);
                msg = "•   Its always possible to send a request to Jared Zumbiel jzumbiel@bluestarinc.com to get a better price!!!";
                infoMsg.push(msg);
                net_p_c++;
                $.ajax({
                    url: 'index.php?page=conditions&action=Zebra',
                    type: 'POST',
                    async: false,
                    data: {
                        part_number: part_number,
                        qty: qty
                    },
                    success: function (response, data) {

                        //  console.log(response);
                        var json = $.parseJSON(response);
                        console.log(json);

                        if(json.response !== null){


                            msg = "<strong>Zebra " + part_number + "</strong> - Запросите Quote у завода, на сайте цена не правильная т.к. товар Custom Made";
                            infoMsg.push(msg);

                        }

                    }
                });


            }else if (brand == 'Brecknell') {
                if(qty >= 5 ){
                    var msg = "<strong>Brecknell:</strong>";
                    infoMsg.push(msg);
                    msg = "•  При больших количествах можно запрашивать балк дискаунт у завода.";
                    infoMsg.push(msg);
                    net_p_c++;
                }

            }else if (brand == 'Cole-parmer') {

                var msg = "<strong>Cole-Parmer:</strong>";
                infoMsg.push(msg);
                msg = "•  Все цены запрашивайте у завода.";
                infoMsg.push(msg);
                net_p_c++;

            }else if (brand == 'Hurricone') {
                var msg = "<strong>Hurricone:</strong>";
                infoMsg.push(msg);
                msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 31+<br/> - Qty 61+";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Hurricone',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Wheeleez') {
                var msg = "<strong>Wheeleez:</strong>";
                infoMsg.push(msg);
                msg = "•   Скидка на товары зависит от суммы заказа по бренду. Если сумма больше $660, то в Net Price будет учитываться скидка, по каждому товару бренда. На некоторые товары скидка может не распространяться. Всё это автоматически учитывается системой.";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Wheeleez',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number,
                            token: token,
                            table: 'domestic_data',
                            pay_type: pay_t
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }


                        }
                    });
                }

            }else if (brand == 'Ted') {
                var msg = "<strong>TED :</strong>";
                infoMsg.push(msg);
                msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1<br/> - Qty 2+";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=TED ',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Scissor lok') {
                var msg = "<strong>Scissor Lok :</strong>";
                infoMsg.push(msg);
                msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 50+<br/> - Qty 250+<br/> - Qty 1000+<br/>";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=ScissorLok',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });

                }
            }else if (brand == 'Se international') {
                var msg = "<strong>SE International:</strong>";
                infoMsg.push(msg);
                msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 10+<br/> - Qty 21+<br/> - Qty 100+<br/>";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=SEInternational',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Soeks') {
                var msg = "<strong>Soeks:</strong>";
                infoMsg.push(msg);
                msg = "• Минимальный заказ по партномеру 20 ед.";
                infoMsg.push(msg);
                msg = "•   Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 50+<br/> - Qty 100+";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Soeks',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'A-d weighing') {
                var msg = "<strong>A-D Weighing:</strong>";
                infoMsg.push(msg);
                msg = "• Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 20+<br/> - Qty 30+<br/> - Qty 40+ - нужно звонить на завод.";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=ADWeighing',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                                if (json.price === null) {
                                    net_price.val(priceFormat(0));
                                    net_price.attr('data-value', rounding(0 * qty));
                                }
                            }
                        }
                    });

                }
            }else if (brand == 'Lw scientific') {
                var sum = qty * deformat(net_price.val());
                net_price.attr('data-value', sum);
                var msg = "<strong>LW Scientific:</strong>";
                infoMsg.push(msg);
                msg = "• Скидка зависит от количества купленного товара. На некоторые товары скидки нет. Все скидки учтены в Net Price.<br/>Цена зависит от кол-ва:<br/>- Qty 1+<br/> - Qty 6+<br/> - Qty 12+.<br/>Если товара нет в прйсе 2015, то нужно звонить на завод уточнять цену.";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=LWScientific',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }
                        }
                    });
                }

            }else if (brand == 'Cooper-atkins') {
                var sum = qty * deformat(net_price.val());
                net_price.attr('data-value', sum);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=CooperAtkins',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);
                            //console.log('resp: ' + json.echo);
                            if(json !== null) {
                                if (json.echo === 1) {
                                    var msg = "<strong>Cooper-Atkins:</strong>";
                                    infoMsg.push(msg);
                                    msg = "• Запросите Bulk discount.";
                                    infoMsg.push(msg);
                                }
                            }

                        }
                    });
                }

            }else if (brand == 'Uniweld') {
                var sum = qty * deformat(net_price.val());
                net_price.attr('data-value', sum);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Uniweld',
                        type: 'POST',
                        async: false,
                        data: {
                            brand: brand,
                            token: token,
                            type: 'domestic',
                            pay_type: pay_t
                        },
                        success: function (response, data) {
                            //console.log(response);
                            var json = $.parseJSON(response);

                            if(json !== null) {
                                if (json.echo == 1) {
                                    var msg = "<strong>Uniweld:</strong>";
                                    infoMsg.push(msg);
                                    msg = "• Запросите Freight Prepaid, Bulk - дискаунт.";
                                    infoMsg.push(msg);
                                }
                            }

                        }
                    });
                }
            }else if (brand == 'Cherne') {
                var sum = qty * deformat(net_price.val());
                net_price.attr('data-value', sum);
                if (box_qty < manuf_box_qty) {
                    var msg = "<strong>Cherne:</strong>";
                    infoMsg.push(msg);
                    msg = "• На некоторые товары есть минимальный заказ и кратность упаковки. При несоблюдении условий, выскочит оповещение.";
                    infoMsg.push(msg);

                    net_p_c++;
                }

            }else if (brand == 'Aven') {
                var sum = qty * deformat(net_price.val());
                net_price.attr('data-value', sum);
                if (box_qty < manuf_box_qty) {
                    var msg = "<strong>Aven:</strong>";
                    infoMsg.push(msg);
                    msg = "•  На некоторые товары есть минимальный заказ и кратность упаковки. При несоблюдении условий, выскочит оповещение.";
                    infoMsg.push(msg);

                    net_p_c++;
                }

            }else if (brand == 'Bel-art products') {
                var msg = "<strong>Bel-Art Products:</strong>";
                infoMsg.push(msg);
                msg = "•    Скидка зависит от количества купленного товара.";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=BelArtProducts',
                        type: 'POST',
                        async: false,
                        data: {
                            qty: qty,
                            part_number: part_number
                        },
                        success: function (response, data) {

                            //console.log(response);
                            var json = $.parseJSON(response);
                            if(json !== null) {
                                if (json.price != null && json.price > 0) {
                                    net_price.val(priceFormat(json.price));
                                    net_price.attr('data-value', rounding(json.price * qty));
                                    //console.log(json.price);
                                }
                            }


                        }
                    });
                }

            }else if (brand == 'Ample scientific') {

                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=AmpleScientific',
                        type: 'POST',
                        async: false,
                        data: {
                            brand: brand,
                            token: token,
                            type: 'domestic',
                            pay_type: pay_t,
                            productCount: $('input[class="brandtag"][value="Ample scientific"][pay-type="'+pay_t+'"]').length
                        },
                        success: function (response, data) {

                            //console.log(response);
                            var json = $.parseJSON(response);
                            if(json !== null) {
                                if (json.echo === 1) {
                                    var msg = "<strong>Ample Scientific:</strong>";
                                    infoMsg.push(msg);
                                    msg = "• Можно позвонить на завод и договориться о лучшей цене.";
                                    infoMsg.push(msg);
                                }
                            }


                        }
                    });
                }

            }else if (brand == 'Xeltek') {

                var msg = "<strong>XELTEK:</strong>";
                infoMsg.push(msg);
                msg = "• В зависимости от суммы заказа, автоматически считается Net Price.<br/>Total order:<br/>менее $1000 - disc. 30%<br/>$1000 - 3000 - disc. 35%<br/>более $3000 - disc. 40%. <br/> При заказе от $1000 - уточняйте у завода shipping discount";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=XELTEK',
                        type: 'POST',
                        async: false,
                        data: {
                            brand: brand,
                            token: token,
                            type: 'domestic',
                            pay_type: pay_t,
                            part_number: part_number,
                            qty: qty

                        },
                        success: function (response, data) {

                            //console.log(response);
                            var json = $.parseJSON(response);
                            net_price.val(priceFormat(json.price));
                            net_price.attr('data-value', rounding(json.price*qty));

                            if (json.echo === 1) {

                                msg = "• Уточните shipping discount у завода";
                                infoMsg.push(msg);
                            }


                        }
                    });

                }
            }else if (brand == 'Deltatrak') {
                var msg = "<strong>Deltatrak:</strong>";
                infoMsg.push(msg);
                msg = "•  Товары можно покупать как кейсами, так и поштучно. При покупке кейса, наша скидка лучше, чем при покупке поштучно. Лучшая скидка распространяется только на полные кейсы. Например, в кейсе 24 шт., а мы заказываем 25. В этом случае, лучшая скидка будет для 24 шт., а для оставшейся 1 - хуже, т.к. им нужно распечатывать другой кейс и брать из него одну штуку. В Таблице в Net Cost отображается худшая цена, но при подсчетах все учитывается правильно. При наведении на Net Cost можно увидеть формулу, по которой подсчитывается total";
                infoMsg.push(msg);

                if(table_status !== 'Approved'){
                    if(parseInt(product_status) !== 0){
                        $.ajax({
                            url: 'index.php?page=conditions&action=Deltatrak',
                            type: 'POST',
                            async: false,
                            data: {
                                qty: qty,
                                part_number: part_number
                            },
                            success: function (response, data) {

                                var json = $.parseJSON(response);

                                if (json.flagBoxQty == false) {
                                    //$('#create_offer').hide();
                                    msg = "• Qty для партномера "+part_number+" должно быть больше/равно "+json.boxQty+" и кратным "+json.boxQty;
                                    infoMsg.push(msg);
                                }

                                net_price.val(priceFormat(json.netPrice));
                                net_price.attr('data-value', rounding(json.total));
                                //console.log(response);


                                net_price.attr('hover', json.hover);



                            }
                        });
                    }
                }
            }else if (brand == 'Mahr federal') {
                var sum = qty * deformat(net_price.val());
                net_price.attr('data-value', sum);
                if (box_qty < manuf_box_qty) {
                    var msg = "<strong>Mahr Federal:</strong>";
                    infoMsg.push(msg);
                    msg = "•       На некоторые товары есть минимальный заказ и кратность упаковки. При несоблюдении условий, выскочит оповещение.";
                    infoMsg.push(msg);

                    net_p_c++;
                }
            }else if (brand == 'Hanna instruments') {
                var payTypesChecked = 0;
                var msg = "<strong>Hanna Instruments:</strong>";
                infoMsg.push(msg);
                msg = "• В зависимости от суммы заказа, автоматически считается Net Price.<br/>Если сумма заказа по бренду больше $200 то discount 25% от list price, евсли сумма меньше $200 то discount 20% от list price.";
                infoMsg.push(msg);
                var hannaProductsCountInTable = $('input[class="brandtag"][value="Hanna instruments"]').length;
                var hannaPCount = $('input[class="brandtag"][value="Hanna instruments"][pay-type="CC"]').length;
                if (pay_t == 'CC' && $('input[type="checkbox"]#CC').is(":checked")) {
                    if(hannaCCRowCount < hannaPCount)
                        hannaCCRowCount++;
                    if (qty >= 3) {
                        hannaProductCCCountCheck++;
                    }

                    if(hannaProductsCountInTable == hannaCCRowCount && hannaProductCCCountCheck == 0)
                    {
                        msg = "• У одного из партномеров по бренду Hanna Instruments, QTY должен быть не менее 3х.";
                        infoMsg.push(msg);
                    }
                }
                if (pay_t == 'ACH' && $('input[type="checkbox"]#ACH').is(":checked")) {
                    if(hannaACHRowCount < hannaPCount)
                        hannaACHRowCount++;
                    if (qty >= 3) {
                        hannaProductACHCountCheck++;
                    }

                    if(hannaProductsCountInTable == hannaACHRowCount && hannaProductACHCountCheck == 0)
                    {
                        msg = "• У одного из партномеров по бренду Hanna Instruments, QTY должен быть не менее 3х.";
                        infoMsg.push(msg);
                    }
                }
                if (pay_t == 'PAYPAL' && $('input[type="checkbox"]#PAYPAL').is(":checked")) {
                    if(hannaPAYPALRowCount < hannaPCount)
                        hannaPAYPALRowCount++;
                    if (qty >= 3) {
                        hannaProductPAYPALCountCheck++;
                    }

                    if(hannaProductsCountInTable == hannaPAYPALRowCount && hannaProductPAYPALCountCheck == 0)
                    {
                        msg = "• У одного из партномеров по бренду Hanna Instruments, QTY должен быть не менее 3х.";
                        infoMsg.push(msg);
                    }
                }
                if (pay_t == 'EBAY' && $('input[type="checkbox"]#EBAY').is(":checked")) {
                    if(hannaEBAYRowCount < hannaPCount)
                        hannaEBAYRowCount++;
                    if (qty >= 3) {
                        hannaProductEBAYCountCheck++;
                    }
                }

                console.log("---------------------");
                console.log("CC: " + hannaProductCCCountCheck);
                console.log("ACH: " + hannaProductACHCountCheck);
                console.log("PAYPAL: " + hannaProductPAYPALCountCheck);
                console.log("EBAY: " + hannaProductEBAYCountCheck);
                console.log("---------------------");
                console.log("---------------------");
                console.log("RowCC: " + hannaCCRowCount);
                console.log("RowACH: " + hannaACHRowCount);
                console.log("RowPAYPAL: " + hannaPAYPALRowCount);
                console.log("RowEBAY: " + hannaEBAYRowCount);
                console.log("---------------------");

                if(hannaCCRowCount > 0)
                {
                    if(hannaProductCCCountCheck == 0){
                        msg = "• У одного из партномеров по бренду Hanna Instruments, QTY должен быть не менее 3х.";
                        infoMsg.push(msg);
                    }
                }
                if(hannaACHRowCount > 0)
                {
                    if(hannaProductACHCountCheck == 0){
                        msg = "• У одного из партномеров по бренду Hanna Instruments, QTY должен быть не менее 3х.";
                        infoMsg.push(msg);
                    }
                }
                if(hannaPAYPALRowCount > 0)
                {
                    if(hannaProductPAYPALCountCheck == 0){
                        msg = "• У одного из партномеров по бренду Hanna Instruments, QTY должен быть не менее 3х.";
                        infoMsg.push(msg);
                    }
                }
                if(hannaEBAYRowCount > 0)
                {
                    if(hannaProductEBAYCountCheck == 0){
                        msg = "• У одного из партномеров по бренду Hanna Instruments, QTY должен быть не менее 3х.";
                        infoMsg.push(msg);
                    }
                }

                //    var hannaProductsCountInTable = $('input[class="brandtag"][value="Hanna instruments"][pay-type="CC"]').length * payTypesChecked;




                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=HannaInstruments',
                        type: 'POST',
                        async: false,
                        data: {
                            brand: brand,
                            token: token,
                            type: 'domestic',
                            pay_type: pay_t,
                            part_number: part_number,
                            qty: qty

                        },
                        success: function (response, data) {

                            //  //console.log(response);
                            var json = $.parseJSON(response);
                            net_price.val(priceFormat(json.price));
                            net_price.attr('data-value', rounding(json.price*qty));
                            if(json.price === null || json.price === 0)
                            {
                                msg = "• Добавлен товар, который мы не можем продавать.";
                                infoMsg.push(msg);
                            }


                        }
                    });
                }

            }else if (brand == 'Industrial test systems') {

                var msg = "<strong>Industrial Test Systems:</strong>";
                infoMsg.push(msg);
                msg = "•     Скидка зависит от количества купленного товара. Скидка автоматически учитывается в Net Price.";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=IndustrialTestSystems',
                        type: 'POST',
                        async: false,
                        data: {
                            part_number: part_number,
                            qty: qty,
                            net_price: $(net_price).val()
                        },
                        success: function (response, data) {

                            //  console.log(response);
                            var json = $.parseJSON(response);
                            console.log(json);
                            var price = (json.price) ? json.price : 0;
                            net_price.val(priceFormat(price));
                            net_price.attr('data-value', rounding(price*qty));



                        }
                    });
                }

            }else if (brand == 'Airwolf 3d') {

                var msg = "<strong>Airwolf 3d:</strong>";
                infoMsg.push(msg);
                msg = "•      Скидка зависит от количества купленного товара. Скидка автоматически учитывается в Net Price.";
                infoMsg.push(msg);
                if(table_status !== 'Approved'){
                    $.ajax({
                        url: 'index.php?page=conditions&action=Airwolf3d',
                        type: 'POST',
                        async: false,
                        data: {
                            part_number: part_number,
                            qty: qty
                        },
                        success: function (response, data) {

                            //  console.log(response);
                            var json = $.parseJSON(response);
                            console.log(json);
                            net_price.val(priceFormat(json.price));
                            net_price.attr('data-value', rounding(json.price*qty));
                        }
                    });
                }

            }else if (brand == 'Hid global - fargo') {



                $.ajax({
                    url: 'index.php?page=conditions&action=HIDGlobal',
                    type: 'POST',
                    async: false,
                    data: {
                        part_number: part_number,
                        qty: qty
                    },
                    success: function (response, data) {

                        //  console.log(response);
                        var json = $.parseJSON(response);
                        console.log(json);

                        if(json.response !== null){


                            msg = "<strong>Hid global - fargo " + part_number + "</strong> - Запросите Quote у завода, на сайте цена не правильная т.к. товар Custom Made";
                            infoMsg.push(msg);

                        }

                    }
                });


            }else {
                var sum = qty * deformat(net_price.val());
                net_price.attr('data-value', sum);
            }
            ////console.log('net_p_c: ' + net_p_c);
            if(net_p_c > 0)
            {
                var sum = qty * deformat(net_price.val());
                net_price.attr('data-value', sum);
            }
        });

        var uniqueNames = [];
        $.each(infoMsg, function (i, el) {
            if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
        });

        /*fixed start zazimko*/
        var infoBox = document.getElementById("infoError");
        if(infoBox != null) {
            infoBox.innerHTML = "";
            var ul = document.createElement("ul");
            infoBox.appendChild(ul);
            for (var i = 0; i < uniqueNames.length; i++) {
                $('#infoError').append("<li>"+uniqueNames[i]+"</li>");
                /*var t = document.createTextNode(uniqueNames[i]);
                 var li = document.createElement("li");
                 li.appendChild(t);
                 ul.appendChild(li);*/
                //console.log(infoMsg[i]);
            }
        }
        /*fixed end zazimko*/
    }

    function suggested_price_data() {

        var elem = $('input[name*="_qty"]');
        $.each(elem, function () {
            var id = $(this).parent().parent().attr('id');
            var qty = $(this).val();
            var suggested_price = $('input[name="' + id + '_suggested_price"]');

            var sum = qty * deformat(suggested_price.val());
            suggested_price.attr('data-value', sum);

            //////console.log('sum : ' + sum);
            //////console.log('data-value : ' + suggested_price.attr('data-value'));

        });
    }

    if ($('input#shipp_to_freeport').val() == '') {
        $('input#shipp_to_freeport').val(0);
    }
    if ($('input#shipp_to_freeport_customer').val() == '') {
        $('input#shipp_to_freeport_customer').val(0);
    }
    suggested_price_f(pay_method);
    total_uttc_net_price(pay_method);
    must_be(pay_method);
    uttc_sell_price(pay_method);
    suggested_price(pay_method);
    app_suggested_price(pay_method);
    approx_profit_calc(pay_method);

    total_aproximate_profit();
    approx_profit_total_calc(pay_method);
    multiplier_f(pay_method);
    final_sale_price_f(pay_method);

    total_final_sale_price(pay_method);
    app_total_final_sale_price();
    net_price_data();
    suggested_price_data();
    //TODO:



}

function tax_rounding(value)
{
    if (parseFloat(value) != 0 && value != '') {


        return parseFloat(value).toFixed(3);

    } else {

        r = parseFloat(value).toFixed(3);

        if (!isNaN(r)) {

            return r;
        } else {

            return value;
        }

    }
}

function rounding(value) {

    if (parseFloat(value) != 0 && value != '') {

        if (parseFloat(value) > 0.01 && parseFloat(value) <= 100) {

            return parseFloat(value).toFixed(2);
        } else if (parseFloat(value) > 100. && parseFloat(value) <= 500) {

            return parseFloat(value).toFixed(2);
        } else if (parseFloat(value) > 500) {

            return parseFloat(value).toFixed(2);
        }
    } else {

        r = parseFloat(value).toFixed(2);

        if (!isNaN(r)) {

            return r;
        } else {

            return value;
        }

    }

}
function rounding_must_be(value) {
    var price = 0;

    if (parseFloat(value) != 0 && value != '') {

        // ////console.log('val: ' + value);
        if (parseFloat(value) < 10) {
            price = parseFloat(value).toPrecision(3);
            //     ////console.log('<10: ' + value);
        } else {
            price = parseFloat(value).toFixed(3);
            //price = Math.ceil(parseFloat(value)).toFixed(2);
        }

        if (parseFloat(value) >= 500) {
            //   ////console.log('>= 500: ' + value);
            price = Math.ceil(parseFloat(value)).toFixed(0);
        } else {
            if (parseFloat(value) >= 100) {
                //      ////console.log('>= 100: ' + value);
                price = parseFloat(value).toFixed(2);
            }
        }
        //  ////console.log('return ' + price);
        return price;

    } else {
        r = parseFloat(value).toPrecision(2);
        //alert(r);
        if (!isNaN(r)) {
            return r;
        } else {
            return value;
        }
    }
}
function must_be_discount(price) {
    var res;
    if (price > 0 && price < 300) {
        res = price - (price / 100) * 2;

    } else if (price >= 300 && price <= 5000) {
        res = price - (price / 100);
    } else if (price > 5000 && price <= 30000) {
        res = price - (price / 100) * 0.5;
    }

    //  ////console.log('must_be_disc: ' + res);
    return rounding_must_be(res);
}
//#######################################
function discount(price) {
    var res;
    if (price > 0 && price <= 299.99) {
        res = price - (price / 100) * 2;

    } else if (price >= 300 && price <= 4999.99) {
        res = price - (price / 100);
    } else if (price >= 5000 && price <= 30000) {
        res = price - (price / 100) * 0.5;
    }


    return rounding(res);
}
//#######################################
function calcs(pay_meth) {

    var len = pay_meth.length;
    for (var i = 0; i < len; i++) {
        total(pay_meth[i]);
    }
    $.save_profits();
}

//#######################################

//#######################################
//#######################################
//#######################################
//#######################################
//#######################################
//#######################################
//#######################################
//#######################################
//#######################################
function recalc($pay_type, id) {

    /*var handling_fee = $('#0_handling_fee').val();
     var handling_fee_val;
     if(!handling_fee || handling_fee == ""){
     handling_fee_val = 0;
     }*/

    _description = $("textarea[pay-type='" + $pay_type + "'][name='" + id + "_description']");
    _qty = $("input[pay-type='" + $pay_type + "'][name='" + id + "_qty']");
    _uttc_net_price = $("input[pay-type='" + $pay_type + "'][name='" + id + "_uttc_net_price']");
    _uttc_net_price_data_value = _uttc_net_price.attr('data-value');
    _uttc_net_price_total = $("input[name='" + $pay_type + "_total_uttc_net_price']");
    _uttc_sell_price = $("input[pay-type='" + $pay_type + "'][name='" + id + "_uttc_sell_price']");
    _uttc_sell_price_data_value = _uttc_sell_price.attr('data-value');
    _uttc_sell_price_total = $("input[name='" + $pay_type + "_total_uttc_sell_price']");
    _must_be = $("input[pay-type='" + $pay_type + "'][name='" + id + "_must_be']");
    _min_sell_price = $("input[pay-type='" + $pay_type + "'][name='" + id + "_min_sell_price']");
    _msp_link = $("input[pay-type='" + $pay_type + "'][name='" + id + "_msp_link']");
    _status = $("input[pay-type='" + $pay_type + "'][name='" + id + "_status']");
    _suggested_price = $("input[pay-type='" + $pay_type + "'][name='" + id + "_suggested_price']");
    _suggested_price_data_value = _suggested_price.attr('data-value');
    _suggested_price_total = $("input[name='" + $pay_type + "_total_suggested_price']");
    _suggested_price_total_app = $("input[name='" + $pay_type + "_total_suggested_price_final']");
    _approx_profit = $("input[pay-type='" + $pay_type + "'][name='" + id + "_approx_profit']");
    _approx_profit_total = $("input[name='" + $pay_type + "_total_approx_profit']");
    _multiplier = $("input[pay-type='" + $pay_type + "'][name='" + id + "_multiplier']");
    _final_sale_price = $("input[pay-type='" + $pay_type + "'][name='" + id + "_final_sale_price']");
    _final_sale_price_total = $("input[name='" + $pay_type + "_total_final_sale_price']");
    _final_sale_price_total_app = $("input[name='" + $pay_type + "_app_total_final_sale_price']");
    _final_multiplier = $("input[pay-type='" + $pay_type + "'][name='" + id + "_final_1multiplier']");
    _approximate_profit = $("input[name='" + $pay_type + "_approximate_profit']");
    _custom_flag = $("select[pay-type='" + $pay_type + "'][name='" + id + "_cs_select']");
    _custom_val = $("input[pay-type='" + $pay_type + "'][name='" + id + "_cs_input']");

    //////console.log('approx: ' + deformat(_approx_profit.val()));
    $.ajax({
        async: false,
        type: 'POST',
        url: 'index.php?page=domestic&action=update&type=' + $pay_type,
        data: {
            'id': id,
            'description': _description.val(),
            'qty': _qty.val(),
            'uttc_net_price': deformat(_uttc_net_price.val()),
            'uttc_net_price_data_value': _uttc_net_price_data_value,
            'uttc_net_price_total': deformat(_uttc_net_price_total.val()),
            'uttc_sell_price': deformat(_uttc_sell_price.val()),
            'uttc_sell_price_data_value': _uttc_sell_price_data_value,
            'uttc_sell_price_total': deformat(_uttc_sell_price_total.val()),
            'must_be': deformat(_must_be.val()),
            'min_sell_price': deformat(_min_sell_price.val()),
            'msp_link': _msp_link.val(),
            'status': _status.val(),
            'suggested_price': deformat(_suggested_price.val()),
            'suggested_price_data_value': _suggested_price_data_value,
            'suggested_price_total': deformat(_suggested_price_total.val()),
            'suggested_price_total_app': deformat(_suggested_price_total_app.val()),
            'approx_profit': deformat(_approx_profit.val()),
            'approx_profit_total': deformat(_approx_profit_total.val()),
            'multiplier': _multiplier.val(),
            'final_sale_price': deformat(_final_sale_price.val()),
            'final_sale_price_total': deformat(_final_sale_price_total.val()),
            'final_sale_price_total_app': deformat(_final_sale_price_total_app.val()),
            'final_multiplier': _final_multiplier.val(),
            'approximate_profit': deformat(_approximate_profit.val()),
            'custom_flag': _custom_flag.val(),
            'custom_value': _custom_val.val(),
            //'handling_fee': handling_fee_val,
        },
        success: function(data) {
            console.log('Запрос по смене количества успешно отправлен!' + data);
            console.log('CHECK--------------------------------' + $pay_type);
            var link = document.getElementById('msp-link-' + id);
            if (link != null) {
                link.href =  _msp_link.val();
            }

        },
        error: function (xhr, str) {
            console.log('Возникла ошибка при смене количества: ' + xhr.responseCode);
            alert('Возникла ошибка при смене количества: ' + xhr.responseCode);
        }
    });


}

function getValueInArrayObjects(array)
{
    var result = [];

    for (var i = 0; i < array.length;i++) {
        result.push(array[i].value);
    }

    return result;
}

function getSalesTaxForZip(data)
{
    var zip = $('#zip').val();
    var ccTotalSuggestedPrice = $('#CC_total_suggested_price').val().replace("$","");
    var shipping = $('#shipp_to_freeport_customer').val().replace("$","");
    var handlingFee = getValueInArrayObjects($('select[id$="_handling_fee"]'));
    var freeshippToCustomer = $('#freeshipp_to_customer').val();
    if (freeshippToCustomer == 'Yes') {
        shipping = 0;
    }

    $.ajax({
        type: "POST",
        url: '/test/TaxJar/getSalesTaxForZip',
        data: {'ownerAction': 'table', 'zip': zip, 'ccTotalSuggestedPrice': ccTotalSuggestedPrice, 'shippingCost': shipping, 'handlingFee': handlingFee},
        async: false,
        success: function(response){
            var res = JSON.parse(response);

            if (res.status == '200') {
                $('input[id="1tax_sum1"]').val(res.data.amount_to_collect);
                $('input[id="tax_percent_info"]').val(res.data.rate * 100);
                console.log(res.data);
                $('#header').html($('#header').html()+"<p style='z-index: 100' class='flash-message success'>"+res.msg+"</p>");
            } else {
                console.log(res.data);
                $('#header').html($('#header').html()+"<p style='z-index: 100' class='flash-message bad'>"+res.msg+"</p>");
            }
            console.log(res);
            console.log(res.msg);
        },
        error: function (error) {
            console.log(error);
        }
    });
}